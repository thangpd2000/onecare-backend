sudo mkdir -p /var/www/deployments/onecare-backend-$1
cd /var/www/deployments/onecare-backend-$1
sudo rm -rf node_modules dist
sudo tar -xf ~/build-backend.tar.gz -C .
sudo cp .env.$1 .env
pm2 restart onecare-backend-$1 || pm2 start dist/main.js -i 1 --name onecare-backend-$1
rm ~/build-backend.tar.gz
