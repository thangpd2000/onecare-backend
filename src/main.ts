import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as express from 'express';
import { join } from 'path';
import { envConfig } from './helper/env.helper';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from '@helper/error.helper';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const options = new DocumentBuilder()
    .setTitle('App One care')
    .setDescription('Mô tả API App One care')
    .setVersion('0.1')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-doc', app, document);
  app.useGlobalFilters(new HttpExceptionFilter());
  app.use('/assets', express.static(join(__dirname, '..', 'assets')));
  app.use('/statics', express.static(join(__dirname, '..', 'assets')));
  await app.listen(process.env.PORT|| 3000);
}
bootstrap();
