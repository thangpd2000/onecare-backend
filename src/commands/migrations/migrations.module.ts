import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Migration } from './migrations.command';
import { MigrationSchema } from './migrations.schema';
import { GroupSchema } from '@modules/permissions/schemas/group.schema';
import { PermissionSchema } from '@modules/permissions/schemas/permission.schema';
import {
  DeliveryAddressSchema,
  UserSchema,
} from '@modules/users/schemas/user.schema';
import { NotificationSchema } from '@modules/notifications/schemas/notification.schema';
import { CitySchema } from '@modules/address/schemas/city.schema';
import { DistrictSchema } from '@modules/address/schemas/district.schema';
import { WardSchema } from '@modules/address/schemas/ward.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Migration', schema: MigrationSchema }]),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Group', schema: GroupSchema }]),
    MongooseModule.forFeature([
      { name: 'Permission', schema: PermissionSchema },
    ]),
    MongooseModule.forFeature([
      { name: 'DeliveryAddress', schema: DeliveryAddressSchema },
    ]),
    MongooseModule.forFeature([
      { name: 'Notification', schema: NotificationSchema },
    ]),
    MongooseModule.forFeature([{ name: 'City', schema: CitySchema }]),
    MongooseModule.forFeature([{ name: 'District', schema: DistrictSchema }]),
    MongooseModule.forFeature([{ name: 'Ward', schema: WardSchema }]),
  ],
  providers: [Migration],
})
export class MigrationModule {}
