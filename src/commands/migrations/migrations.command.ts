import { Command, Positional } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import Commands from '../base';
import { Migration as MigrationInterface } from './migrations.interface';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import requireDir from 'require-dir';
import { Group } from '@modules/permissions/interfaces/group.interface';
import { Permission } from '@modules/permissions/interfaces/permission.interface';
import {
  DeliveryAddress,
  User,
} from '@modules/users/interfaces/user.interface';
import { Notification } from '@modules/notifications/interfaces/notification.interface'
import { District } from '@modules/address/interfaces/district.interface';
import { City } from '@modules/address/interfaces/city.interface';
import { Ward } from '@modules/address/interfaces/ward.interface';

export interface MigrationStep {
  name: string;
  up(migration: Migration): Promise<any>;
}

interface Migrations {
  [key: string]: MigrationStep;
}

@Injectable()
export class Migration {
  constructor(
    @InjectModel('Migration')
    private readonly migrationModel: Model<MigrationInterface>,
    @InjectModel('User')
    public readonly userModel: Model<User>,
    @InjectModel('Group')
    public readonly groupModel: Model<Group>,
    @InjectModel('Permission')
    public readonly permissionModel: Model<Permission>,
    @InjectModel('DeliveryAddress')
    public readonly deliveryAddressModel: Model<DeliveryAddress>,
    @InjectModel('City')
    public readonly cityModel: Model<City>,
    @InjectModel('District')
    public readonly DistrictModel: Model<District>,
    @InjectModel('Ward')
    public readonly wardModel: Model<Ward>,
    @InjectModel('Notification')
    public readonly notificationModel: Model<Notification>,
  ) {}

  run = async (migrationName?: string) => {
    console.log(
      '!!!!!!!!!!!!!!!!!!!!!!!!! STARTED MIGRATE !!!!!!!!!!!!!!!!!!!!!!!!!',
    );

    // eslint-disable-next-line @typescript-eslint/no-var-requires
    // const modules = require('require-dir-all')('./');
    const modules: Migrations = requireDir('./steps');
    const keys = [];
    for (const key in modules) {
      if (modules.hasOwnProperty(key)) {
        keys.push(key);
      }
    }

    // check script need to run
    let ranKeys = [];
    ranKeys = await this.migrationModel
      .find({
        key: {
          $in: keys,
        },
      })
      .distinct('key');

    const needToRunKeys = migrationName
      ? [migrationName]
      : keys.filter(k => ranKeys.indexOf(k) === -1);

    if (migrationName && !modules[migrationName]) {
      console.log('Migration key not exist');
      return;
    }

    for (const key of needToRunKeys) {
      console.log(`MIGRATE ${modules[key].name}: ${key} STARTED`);
      await modules[key].up(this);
      if (key !== '0-before-all') {
        await this.migrationModel.create({
          key,
          createdAt: new Date(),
        });
      }
      console.log(`MIGRATE ${modules[key].name}: ${key} COMPLETED`);
    }

    console.log(
      '!!!!!!!!!!!!!!!!!!!!!!!!! MIGRATE DONE !!!!!!!!!!!!!!!!!!!!!!!!!',
    );
  };

  @Command({
    command: 'database:migrate:one <migrationName>',
    describe: 'migrate database',
    autoExit: false,
  })
  async migrateOne(
    @Positional({
      name: 'migrationName',
      describe: 'name of specific migratioan to run',
      type: 'string',
    })
    migrationName?: string,
  ) {
    Commands.run(() => {
      this.run(migrationName)
        .then(() => {
          process.exit(0);
        })
        .catch(error => {
          console.error(error);
          console.error(
            '!!!!!!!!!!!!!!!!!!!!!!!!! MIGRATE DONE WITH ERROR !!!!!!!!!!!!!!!!!!!!!!!!!',
          );
          process.exit(0);
        });
    });
  }

  @Command({
    command: 'database:migrate:all',
    describe: 'migrate database',
    autoExit: false,
  })
  async migrateAll() {
    Commands.run(() => {
      this.run()
        .then(() => {
          process.exit(0);
        })
        .catch(error => {
          console.error(error);
          console.error(
            '!!!!!!!!!!!!!!!!!!!!!!!!! MIGRATE DONE WITH ERROR !!!!!!!!!!!!!!!!!!!!!!!!!',
          );
          process.exit(0);
        });
    });
  }
}
