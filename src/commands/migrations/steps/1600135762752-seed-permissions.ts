import { PermissionType } from '@modules/permissions/interfaces/permission.entity';
import { Migration } from '../migrations.command';

export const name = 'seed-permissions';
export const up = async (migration: Migration) => {
  const countPermissions = await migration.permissionModel.countDocuments();

  if (!countPermissions) {
    await migration.permissionModel.insertMany([
      {
        type: PermissionType.PRODUCT,
        read: true,
        create: true,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.ORDER,
        read: true,
        create: true,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.PROMOTION,
        read: true,
        create: true,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.NEWS,
        read: true,
        create: true,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.TERMS_OF_DELIVERY,
        read: true,
        create: false,
        edit: true,
        delete: false,
      },
      {
        type: PermissionType.CONTACT,
        read: true,
        create: false,
        edit: true,
        delete: false,
      },
      {
        type: PermissionType.BANK_ACCOUNT,
        read: true,
        create: false,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.FAQ,
        read: true,
        create: false,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.MESSAGE,
        read: true,
        create: true,
        edit: false,
        delete: false,
      },
      {
        type: PermissionType.NOTIFICATION,
        read: true,
        create: false,
        edit: true,
        delete: true,
      },
      {
        type: PermissionType.CUSTOMERS,
        read: true,
        create: false,
        edit: true,
        delete: true,
      },
    ]);
  }
};
