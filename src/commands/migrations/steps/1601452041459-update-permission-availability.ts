import { PermissionType } from "@modules/permissions/interfaces/permission.entity";
import { Migration } from "../migrations.command";

export const name = 'update-permission';
export const up = async (migration: Migration) => {
  await migration.permissionModel.bulkWrite([
    {
      updateOne: {
        filter: { type: PermissionType.BANK_ACCOUNT },
        update: {
          $set: { read: true, create: true, edit: true, delete: true },
        },
      },
    },
    {
      updateOne: {
        filter: { type: PermissionType.FAQ },
        update: {
          $set: { read: true, create: true, edit: true, delete: true },
        },
      },
    },
    {
      updateOne: {
        filter: { type: PermissionType.NOTIFICATION },
        update: {
          $set: { read: true, create: true, edit: true, delete: true },
        },
      },
    },
    {
      updateOne: {
        filter: { type: PermissionType.CUSTOMERS },
        update: {
          $set: { read: true, create: false, edit: true, delete: false },
        },
      },
    },
  ]);
};
