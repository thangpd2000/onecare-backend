import { Migration } from '../migrations.command';
import { asyncForEach } from '@helper/function.helper';
import data from '../data.json';
import { from } from 'rxjs';
export const name = 'insert-city-district';
export const up = async (migration: Migration) => {
  type City = {
    name: string;
    slug: string;
    type: string;
    name_with_type: string;
    code: string;
  };

  type District = {
    name: string;
    type: string;
    slug: string;
    name_with_type: string;
    path: string;
    path_with_type: string;
    code: string;
    parent_code: string;
  };
  const cities = Object.entries(data);
  await asyncForEach(cities, async ([, city]: [string, City]) => {
    const cityDoc = await migration.cityModel.create({ name: city.name });
    const districts = Object.entries(city['quan-huyen']);

    const districtQuery = districts.map(([, district]: [string, District]) => ({
      name: district.name,
      cityId: cityDoc._id,
    }));

    await migration.DistrictModel.insertMany(districtQuery);
  });
};
