import { Permission } from '@modules/permissions/interfaces/permission.interface';
import { Migration } from '../migrations.command';

export const name = 'update-permission-in-group';
export const up = async (migration: Migration) => {
  const [groups, permissions] = await Promise.all([
    migration.groupModel.find(),
    migration.permissionModel.find(),
  ]);
  for (const group of groups) {
    let permission = [];
    for (const per of group.permission) {
      const permissionCollection: any = permissions.find(
        (p: Permission) => p._id.toString() === per._id.toString(),
      );
      if (per.read == -1 && permissionCollection.read !== false) {
        per.read = 0;
      }
      if (per.create == -1 && permissionCollection.create !== false) {
        per.create = 0;
      }
      if (per.edit == -1 && permissionCollection.edit !== false) {
        per.edit = 0;
      }
      if (per.delete == -1 && permissionCollection.delete !== false) {
        per.delete = 0;
      }

      if (
        (per.read == 1 || per.read == 0) &&
        permissionCollection.read !== true
      ) {
        per.read = -1;
      }
      if (
        (per.create == 1 || per.create == 0) &&
        permissionCollection.create !== true
      ) {
        per.create = -1;
      }
      if (
        (per.edit == 1 || per.edit == 0) &&
        permissionCollection.edit !== true
      ) {
        per.edit = -1;
      }
      if (
        (per.delete == 1 || per.delete == 0) &&
        permissionCollection.delete !== true
      ) {
        per.delete = -1;
      }
      permission.push(per);
    }
    await migration.groupModel.findByIdAndUpdate(group._id, { permission });
  }
};
