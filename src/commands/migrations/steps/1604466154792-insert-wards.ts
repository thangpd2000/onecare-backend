import { Migration } from '../migrations.command';
import { asyncForEach } from '@helper/function.helper';
import data from '../data.json';
import { City as ICity } from '../../../modules/address/interfaces/city.interface';
import { District as IDistrict } from '../../../modules/address/interfaces/district.interface';
import { from } from 'rxjs';
export const name = 'insert-wards';
export const up = async (migration: Migration) => {
  type City = {
    name: string;
    slug: string;
    type: string;
    name_with_type: string;
    code: string;
  };

  type District = {
    name: string;
    type: string;
    slug: string;
    name_with_type: string;
    path: string;
    path_with_type: string;
    code: string;
    parent_code: string;
  };

  type Ward = {
    name: string;
    type: string;
    slug: string;
    name_with_type: string;
    path: string;
    path_with_type: string;
    code: string;
    parent_code: string;
  };
  const cities = Object.entries(data);
  const cityObjects = await migration.cityModel.find();
  await asyncForEach(cities, async ([, city]: [string, City]) => {
    const cityDoc = await cityObjects.find((c: ICity) => c.name === city.name);

    const districtObjects = await migration.DistrictModel.find({
      cityId: cityDoc._id.toString(),
    });

    const districts = Object.entries(city['quan-huyen']);

    await asyncForEach(districts, async ([, district]: [string, District]) => {
      const districtDoc = districtObjects.find(
        (d: IDistrict) => d.name === district.name,
      );

      const wards = Object.entries(district['xa-phuong']);

      const wardQuery = wards.map(([, ward]: [string, Ward]) => ({
        name: ward.name,
        districtId: districtDoc._id,
      }));

      await migration.wardModel.insertMany(wardQuery);
    });
  });
};
