import { PermissionType } from '@modules/permissions/interfaces/permission.entity';
import { Migration } from '../migrations.command';
const newPermissions = [
  {
    type: PermissionType.STORE,
    read: true,
    create: true,
    edit: true,
    delete: true,
  },
  {
    type: PermissionType.CATEGORYPRODUCT,
    read: true,
    create: true,
    edit: true,
    delete: true,
  },
  {
    type: PermissionType.CONFIGURATIONS,
    read: true,
    create: true,
    edit: true,
    delete: true,
  },
  {
    type: PermissionType.RANK,
    read: true,
    create: true,
    edit: true,
    delete: true,
  },
  {
    type: PermissionType.CATEGOTYSYSTEM,
    read: true,
    create: true,
    edit: true,
    delete: true,
  },
  {
    type: PermissionType.STATISTICS,
    read: true,
    create: false,
    edit: false,
    delete: false,
  },
];
export const name = 'insert-permissionnew';
export const up = async (migration: Migration) => {
  for (let permission of newPermissions) {
    const countPermission = await migration.permissionModel.countDocuments({
      type: permission.type,
    });
    if (!countPermission) {
      migration.permissionModel.insertMany(permission);
    }
  }
};
