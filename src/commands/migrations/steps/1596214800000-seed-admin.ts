import { Migration } from '../migrations.command';
import { envConfig } from '@helper/env.helper';
import { Type } from '@modules/users/interfaces/user.entity';

export const name = 'seed-permissions';
export const up = async (migration: Migration) => {
  const countAdmins = await migration.userModel.countDocuments({ type: Type.ADMIN });

  if (!countAdmins) {
    const admin = new migration.userModel({
      name: "Admin",
      username: envConfig.ADMIN_USERNAME,
      password: envConfig.ADMIN_PASSWORD,
      type: Type.ADMIN,
    });
    await admin.save();
  }
};
