import { asyncForEach } from '@helper/function.helper';
import { Group } from "@modules/permissions/interfaces/group.interface";
import { Permission } from "@modules/permissions/interfaces/permission.interface";
import { Migration } from "../migrations.command";

export const name = 'update-permission-type-in-group';
export const up = async (migration: Migration) => {
  const [groups, permissions] = await Promise.all([
    migration.groupModel.find(),
    migration.permissionModel.find(),
  ]);

  await asyncForEach(groups, async (group: Group) => {
    const groupPermission = group.permission.map((p: Permission) => ({
      ...p,
      type: permissions
        .find(permission => permission._id.toString() === p._id.toString())
        .get('type'),
    }));

    group.set({ permission: groupPermission });
    await group.save();
  });
};
