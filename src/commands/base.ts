async function run(func: () => void) {
  await func();
}

export default {
  run,
};
