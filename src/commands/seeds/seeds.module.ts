import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CitySchema } from '@modules/address/schemas/city.schema';
import { DistrictSchema } from '@modules/address/schemas/district.schema';
import { CityDistrictSeed } from '@modules/address/seeds/cities-districts';
import { WardSeed } from '@modules/address/seeds/wards';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'City', schema: CitySchema }]),
    MongooseModule.forFeature([{ name: 'District', schema: DistrictSchema }]),
  ],
  providers: [CityDistrictSeed, WardSeed],
})
export class SeedModule {}