import { Test, TestingModule } from '@nestjs/testing';
import { CrontabController } from './crontab.controller';

describe('CrontabController', () => {
  let controller: CrontabController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CrontabController],
    }).compile();

    controller = module.get<CrontabController>(CrontabController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
