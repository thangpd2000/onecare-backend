import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CronJob }  from 'cron';
import momentTz from 'moment-timezone';
import { User } from '@modules/users/interfaces/user.interface';
import { Model } from 'mongoose';
import { CronExpression, DateFormat, TimeZone, Actor } from '@modules/base.interface';
import { TitleNotification } from '@modules/notifications/interfaces/notification.entity';
import { createNotificationToTag } from '@modules/notifications/utils/onesignal';
import { Notification } from '@modules/notifications/interfaces/notification.interface';
import { TemplateTitleNotification, TypeNoti } from '@modules/base.interface';
import { getContentNotification } from './../../../utils/helper';
import { sendMessageTelegram } from '@utils/helper';
import { PayloadData } from '@modules/notifications/interfaces/notification.entity';
@Injectable()
export class CrontabService {
  constructor(
    @InjectModel('User')
    private readonly userModel: Model<User>,
    @InjectModel('Notification')
    private readonly notificationModel: Model<Notification>,
  ) {}
  onModuleInit() {

    const _self = this;
    const job = new CronJob(CronExpression.EVERY_START_DAY, async function() { 
      const currentDateVN = momentTz(new Date())
        .tz(TimeZone.VN_HOCHIMINH)
        .format(DateFormat.YYYYMMDD);
      const currentDay = new Date(currentDateVN);
      const dataUsers = await _self.userModel.aggregate([
          { 
            $match: {
              $expr: {
                $and: [
                  { $eq: [{ $dayOfMonth: '$dateOfBirth' }, { $dayOfMonth: currentDay }] },
                  { $eq: [{ $month: '$dateOfBirth' }, { $month: currentDay }] },
                ],
              },
            }
          }
        ]);
      const prmInsertNoti = [];
      const prmPushNoti = [];
      const dataTelegram = {
        user: [],
        timeTrigger: new Date().getTime()
      };
      if (dataUsers.length > 0) {
        for (const itemUser of dataUsers) {
          const bodyNoti = {
            title: TitleNotification.HAPPY_BIRTHDAY,
            content: getContentNotification(TemplateTitleNotification.HAPPY_BIRTHDAY, {}),
          };
          const id = itemUser._id;
          dataTelegram.user.push(id);
          const fromUser = [id];
          const payload: PayloadData = {
            userId: id,
            actor: Actor.CRON_JOB,
            type: TypeNoti.HAPPY_BIRTHDAY,
          }
          const dataInsertNoti = {
            title: bodyNoti.title,
            content: bodyNoti.content,
            image: itemUser.avatar || '',
            payload,
            fromUser,
            status: true,
            createdAt: new Date(),
          };
          prmInsertNoti.push( _self.notificationModel.create(dataInsertNoti));
          prmInsertNoti.push(
            _self.userModel.findByIdAndUpdate(id, {
              $inc: { unreadNotification: 1 },
            }),
          );
          prmPushNoti.push(createNotificationToTag(bodyNoti.title, bodyNoti.content, fromUser, {
            ...payload,
            title: bodyNoti.title,
            content: bodyNoti.content,
          }));
        }
      }
      if (prmInsertNoti.length) {
        await Promise.all(prmInsertNoti);
        await Promise.all(prmPushNoti);
      }
      sendMessageTelegram('HAPPY BIRTHDAY', dataTelegram);
    }, null, true, TimeZone.VN_HOCHIMINH);
    job.start();
  }
}
 
