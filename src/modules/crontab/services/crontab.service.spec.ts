import { Test, TestingModule } from '@nestjs/testing';
import { CrontabService } from './crontab.service';

describe('CrontabService', () => {
  let service: CrontabService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CrontabService],
    }).compile();

    service = module.get<CrontabService>(CrontabService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
