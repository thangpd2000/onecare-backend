import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { CrontabService } from '@modules/crontab/services/crontab.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from '@modules/users/schemas/user.schema';
import { NotificationSchema } from '@modules/notifications/schemas/notification.schema';

@Module({
    imports: [
        ScheduleModule.forRoot(),
        MongooseModule.forFeature([
            { name: 'User', schema: UserSchema },
            { name: 'Notification', schema: NotificationSchema },

        ])
    ],
    exports: [CrontabService],
    providers: [CrontabService],
})
export class CrontabModule {}
