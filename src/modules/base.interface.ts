import { Request } from 'express';
import { User } from './users/interfaces/user.interface';

export interface UpdatedResponse {
  ok: number;
  n: number;
  nModified: number;
}
export interface UserRequest extends Request {
  user?: User;
  userId?: string;
  token?: string;
}
export interface MyRequest extends Request {
  user?: User;
}

export enum DefaultGeneralConfig {
  estimatedDeliveryTime = 0,
  quantityHotsale = 1,
  poinConversionRate = 0,
  contentPayment = '',
}
export enum DefaultConditionDelivery {
  deliveryCondition = 0,
  description=''

}

export enum StatusCodes {
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
  NOT_HAVE_ACCESS = 'NOT_HAVE_ACCESS',
  VALIDATION_ERROR = 'VALIDATION_ERROR',
  INVALID_TOKEN = 'INVALID_TOKEN',
  USER_ALREADY_EXISTS = 'USER_ALREADY_EXISTS',
  TEL_ALREADY_EXISTS = 'TEL_ALREADY_EXISTS',
  USER_DO_NOT_EXISTS = 'USER_DO_NOT_EXISTS',
  PASSWORD_INCORRECT = 'PASSWORD_INCORRECT',
  PASSWORD_CAN_NOT_BE_BLANK = 'PASSWORD_CAN_NOT_BE_BLANK',
  PASSWORD_MUST_BE_BETWEEN_6_CHARACTER = 'PASSWORD_MUST_BE_BETWEEN_6_CHARACTER',
  NAME_CAN_NOT_BE_BLANK = 'NAME_CAN_NOT_BE_BLANK',
  USERNAME_CAN_NOT_BE_BLANK = 'USERNAME_CAN_NOT_BE_BLANK',
  TYPE_CAN_NOT_BE_BLANK = 'TYPE_CAN_NOT_BE_BLANK',
  CONVERSATION_ID_CAN_NOT_BE_BLANK = 'CONVERSATION_ID_CAN_NOT_BE_BLANK',
  FRIEND_ID_CAN_NOT_BE_BLANK = 'FRIEND_ID_CAN_NOT_BE_BLANK',
  LIMIT_CAN_NOT_BE_BLANK = 'LIMIT_CAN_NOT_BE_BLANK',
  ID_CAN_NOT_BE_BLANK = 'ID_CAN_NOT_BE_BLANK',
  WIDTH_CAN_NOT_BE_BLANK = 'WIDTH_CAN_NOT_BE_BLANK',
  HEIGHT_CAN_NOT_BE_BLANK = 'HEIGHT_CAN_NOT_BE_BLANK',
  FILE_CAN_NOT_BE_BLANK = 'FILE_CAN_NOT_BE_BLANK',
  RECORD_NOT_FOUND = 'RECORD_NOT_FOUND',
  UNIT_ALREADY_EXISTS = 'UNIT_ALREADY_EXISTS',
  STORE_ALREADY_EXISTS = 'STORE_ALREADY_EXISTS',
  STORE_DO_NOT_EXISTS = 'STORE_DO_NOT_EXISTS',
  TOKEN_ALREADY_EXISTS = 'TOKEN_ALREADY_EXISTS',
  PERMISSION_ALREADY_EXISTS = 'PERMISSION_ALREADY_EXISTS',
  PERMISSION_MALFORMED = 'PERMISSION_MALFORMED',
  UNKNOWN_PERMISSION = 'UNKNOWN_PERMISSION',
  FILE_NOT_ALLOW_MIME_TYPE = 'FILE_NOT_ALLOW_MIME_TYPE',
  RECORD_ALREADY_EXISTS = 'RECORD_ALREADY_EXISTS',
  POINT_NOT_ENOUGH_TO_USE = 'POINT_NOT_ENOUGH_TO_USE',
  PERMISSION_DO_NOT_EXISTS = 'PERMISSION_DO_NOT_EXISTS',
  USER_ALREADY_EXISTS_IN_GROUP = 'USER_ALREADY_EXISTS_IN_GROUP',
  STORE_HAVE_NO_PRODUCTS = 'STORE_HAVE_NO_PRODUCTS',
  CAN_NOT_GREATER_THAN_MAX_UPLOADED_FILE = 'CAN_NOT_GREATER_THAN_MAX_UPLOADED_FILE',
  MIN_ONE = 'MIN_ONE', //ít nhất 1
  MIN_MAIN_ONE = 'MIN_MAIN_ONE',
  NOTIFICATION_DO_NOT_EXISTS = 'NOTIFICATION_DO_NOT_EXISTS',
  CART_INVALID = 'CART_INVALID',
  PRODUCT_IS_OUT_OF_STOCK = 'PRODUCT_IS_OUT_OF_STOCK',
  PRODUCT_IS_OUT_OF_PPROMO = 'PRODUCT_IS_OUT_OF_PPROMO',
  BANK_ALREADY_EXISTS = 'BANK_ALREADY_EXISTS',
  BANK_DO_NOT_EXISTS = 'BANK_DO_NOT_EXISTS',
  CATEGORY_DO_NOT_EXIST = 'CATEGORY_DO_NOT_EXIST',
  PRODUCT_DO_NOT_EXIST = 'PRODUCT_DO_NOT_EXIST',
  USER_HAS_BEEN_BLOCKED = 'USER_HAS_BEEN_BLOCKED',
  CATEGORY_ALREADY_PRODUCT = 'CATEGORY_ALREADY_PRODUCT',
  CODE_ALREADY_EXISTS = 'CODE_ALREADY_EXISTS',
  PROMOTION_NAME_ALREADY_EXIST = 'PROMOTION_NAME_ALREADY_EXIST',
  DIFF_PREFERENTIAL_AMOUNT = 'DIFF_PREFERENTIAL_AMOUNT',
  PRODUCT_NOT_EXIST = 'PRODUCT_NOT_EXIST',
  DUPLICATE_PRODUCT = 'DUPLICATE_PRODUCT',
  QUANTITY_INVALID = 'QUANTITY_INVALID',
  NOT_RIGHT = 'NOT_RIGHT',
  RANKING_NOT_AVAILABLE = 'RANKING_NOT_AVAILABLE',
  DELIVERY_ADDRESS_DO_NOT_EXISTS = 'DELIVERY_ADDRESS_DO_NOT_EXISTS',
  PRODUCT_UNAVAILABLE = 'PRODUCT_UNAVAILABLE',
  BODY_CONFIGURATION_IS_EMPTY = 'BODY_CONFIGURATION_IS_EMPTY',
  QUESTION_DO_NOT_EXISTS = 'QUESTION_DO_NOT_EXISTS',
  NEWS_ALREADY_EXISTS = 'NEW_ALREADY_EXISTS',
  NEWS_DO_NOT_EXISTS = 'NEW_DO_NOT_EXISTS',
  PRODUCT_DO_NOT_EXISTS = 'PRODUCT_DO_NOT_EXISTS',
  CANNOT_CREATE_CONVERSATION = 'CANNOT_CREATE_CONVERSATION',
  COMMUNICATION_DO_NOT_EXISTS = 'COMMUNICATION_DO_NOT_EXISTS',
  FILE_IS_NOT_FORMULATED = 'FILE_IS_NOT_FORMULATED',
  CONVERSATION_NOT_EXIST = 'CONVERSATION_NOT_EXIST',
  TOKEN_NOT_RIGHT = 'TOKEN_NOT_RIGHT',
  TOKEN_NOT_EXIST = 'TOKEN_NOT_EXIST',
  EMAIL_ALREADY_EXISTS="EMAIL_ALREADY_EXISTS",
  EXERCISEREGIME_DO_NOT_EXISTS='EXERCISEREGIME_DO_NOT_EXISTS'
}
export interface BaseResponse {
  status: StatusCodes;
}

export interface BooleanResponse {
  status: StatusCodes;
  data: boolean;
}

export enum BaseStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export enum BaseStatusVi {
  ACTIVE = 'HOẠT ĐỘNG',
  INACTIVE = 'KHÔNG HOẠT ĐỘNG',
}

export class BaseInput {
  id: string;
}

export class DateRange {
  startDate: any;
  endDate: any;
}

export enum DateFormat {
  DDMMYYYY = 'DD-MM-YYYY',
  YYYYMMDD = 'YYYY-MM-DD',
  MMYYYY = 'MM-YYYY',
  YYYY = 'YYYY',
}

export enum CronExpression {
  EVERY_START_DAY = '0 0 0 * * *',
}

export enum BaseBoolean {
  YES = 'YES',
  NO = 'NO',
}

export enum TimeZone {
  VN_HOCHIMINH = 'Asia/Ho_Chi_Minh',
}

export enum PermissionDetails {
  READ = 'read',
  EDIT = 'edit',
  CREATE = 'create',
  DELETE = 'delete',
}

export enum PermissionTypes {
  FAQ = 'FAQ',
  NEWS = 'NEWS',
  ORDER = 'ORDER',
  PRODUCT = 'PRODUCT',
  CONTACT = 'CONTACT',
  MESSAGE = 'MESSAGE',
  PROMOTION = 'PROMOTION',
  CUSTOMERS = 'CUSTOMERS',
  NOTIFICATION = 'NOTIFICATION',
  BANK_ACCOUNT = 'BANK_ACCOUNT',
  TERMS_OF_DELIVERY = 'TERMS_OF_DELIVERY',
}

export enum TemplateTitleNotification {
  ORDER_CONFRIRMED = 'Đơn hàng của bạn đã được xác nhận',
  ORDER_PAID = 'Xác nhận thanh toán thành công',
  ORDER_DELIVED = 'Giao hàng thành công',
  ORDER_CANCELED_BY_CMS = 'Đơn bị từ chối',
  PROMOTION = 'Khuyến mại',
  HAPPY_BIRTHDAY = 'Chúc mừng sinh nhật',
  CHANGE_POIN_WHEN_PAY_IN_STORE = 'Áp dụng điểm khi mua hàng',
  MESSAGE_CMS = 'Bạn nhận được tin nhắn mới',
}

export enum LengthContentNoti {
  MIN = 0,
  MAX = 500,
}
export interface PayloadNoti {
  codeOrder?: string;
  point?: number;
  hotline?: number;
  userId?: string;
  actor?: string;
  orderId?: string;
  type?: TypeNoti;
}

export enum TypeNoti {
  HAPPY_BIRTHDAY = 'Happy Birthday',
  ORDER_CANCEL = 'CANCEL',
  ORDER_RECEIVE = 'RECEIVE',
  ORDER_RECEIVED = 'RECEIVED',
  GENERAL = 'General',
  PROMOTION = 'Promotion',
  USE_POINT = 'USE POINT',
  MESSAGE_CMS = 'MESSAGE CMS',
}

export enum TelegramConfig {
  TOKEN = '1338857563:AAEcINF0PXgJp297XI0FKzn04maMPZnzmaE',
  CHAT_ID = '890396618',
}

export enum Actor {
  CMS = 'CMS',
  MOBILE = 'MOBILE',
  CRON_JOB = 'CRON JOB',
  SYSTEM = 'SYSTEM', // Thong bao tu tao tren cms
}

export enum RoomSocket {
  SUPPORT = 'SUPPORT',
}

export enum Contacts {
  HOTLINE = 19000285,
}

export enum TimeToLiveMessage {
  FIFTEEN_DAY = 1296000,
  ONE_HOUT = 3600,
  ONE_MINUTE = 60,
}

export enum EventSocket {
  CLIENT_JOIN_ROOM_WHEN_LOGIN = 'client-join-room-when-login',
  PERMISSION_CHANGED = 'PERMISSION_CHANGED',
  ERROR = 'err'
}
/**
 * + Cập nhật trạng thái đơn hàng :
Đơn đã xác nhận:
- Title:   Đơn hàng của bạn đã được xác nhận
- Content: Đơn hàng <Mã đơn hàng> đã được hệ thống xác nhận. 

Đơn đã thanh toán
- Title:   Xác nhận thanh toán thành công
- Content: Bạn vừa thanh toán thành công cho đơn hàng <Mã đơn hàng>

Đơn hàng đã giao
- Title:   Giao hàng thành công
- Content: Đơn hàng <mã đơn hàng> đã được giao hàng thành công tới địa điểm nhận

Đơn hàng bị hủy bởi CMS
- Title:   Đơn bị từ chối
- Content (đang confirm thêm với khách): Đơn hàng <mã đơn hàng> của bạn đã bị từ chối. Vui lòng liên hệ Dailymart theo số hotline cửa hàng……. hoặc chat trực tiếp trên App để được hỗ trợ.

+ Chương trình khuyến mại:
- Title:   Khuyến mại
- Content: <Nội dung trường Mô tả của khuyến mại trên CMS>

+ Chúc mừng sinh nhật:
- Title:   Chúc mừng sinh nhật
- Content: Đội ngũ Dailymart gửi lời chúc mừng sinh nhật tốt đẹp nhất tới quý khách. Hy vọng quý khách sẽ tiếp tục đồng hành và có những trải nghiệm thú vị cùng Dailymart trong thời gian tới!

+ Thông báo trừ điểm khi điểm được sử dụng lúc khách hàng mua trực tiếp tại cửa hàng:
- Title:   Áp dụng điểm khi mua hàng
- Content: Bạn vừa sử dụng <số điểm đã bị trừ> điểm vào đơn hàng mua trực tiếp tại cửa hàng 

+ Thông báo chung từ hệ thống: được hiển thị với tiêu đề và nội dung được tạo từ CMS


 */
