export const getFriendKey = (userIds: string[]): string => {
  return userIds.sort((a, b) => a.toString().localeCompare(b.toString())).join('-');
};

export const getDirectConversationKey = (userId: string, otherUserId: string): string => {
  return getFriendKey([userId, otherUserId]);
};
