import { isEmpty } from '@helper/function.helper';
import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { UserError } from '@helper/error.helper';
import { ConversationStatusCode } from '../conversation.types';

@Injectable()
export class ValidateConversationInput implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const { conversationId, type } = request.body;
    if (isEmpty(type)) {
      throw new UserError(ConversationStatusCode.TYPE_CAN_NOT_BE_BLANK);
    }

    if (isEmpty(conversationId)) {
      throw new UserError(
        ConversationStatusCode.CONVERSATION_ID_CAN_NOT_BE_BLANK,
      );
    }

    return true;
  }
}

export class ValidateDirectParams implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const { friendId } = request.query;
    if (isEmpty(friendId)) {
      throw new UserError(ConversationStatusCode.FRIEND_ID_CAN_NOT_BE_BLANK);
    }

    return true;
  }
}

@Injectable()
export class ValidateListConversationParams implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const { limit } = request.query;

    if (isEmpty(limit)) {
      throw new HttpException(
        { status: ConversationStatusCode.LIMIT_CAN_NOT_BE_BLANK },
        HttpStatus.BAD_REQUEST,
      );
    }

    return true;
  }
}
