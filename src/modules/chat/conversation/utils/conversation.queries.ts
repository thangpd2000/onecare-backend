import { Types } from 'mongoose';
import { LookupUsers, LookupMessage } from '@helper/queries.helper';
import { ListConversationParams } from '../conversation.types';
import mongoose from 'mongoose';
export const ListConversationQuery = (
  params: ListConversationParams,
): any[] => {
  const { limit, after, userId } = params;
  const $limit = parseInt(limit);
  const ObjectId = mongoose.Types.ObjectId;
  const $match: any = {
    userIds:ObjectId(params.userId),

    // deletedUserIds: { $nin: [userId] }
  };
  if (after) {
    $match._id = { $gt: new Types.ObjectId(after) };
  }
  const lookupUsers = LookupUsers('$userIds', 'users');
  const lookupMessage = LookupMessage();
  const mainQuery = [
    { $addFields :  {  firstElem :   { $arrayElemAt: [ "$userIds", 0 ] } }  } ,
    { $addFields :  {  secondElem :   { $arrayElemAt: [ "$userIds", 1 ] } }  } ,
    { ...lookupUsers },
    { ...lookupMessage },

    { $match },
    { $limit },
    {$sort:{updatedAt:-1}}
    
  ];
  return mainQuery;
};

export const ConversationQuery = (conversationId: string): any[] => {
  const $match: any = {
    _id: new Types.ObjectId(conversationId),
  };
  const lookupUsers = LookupUsers('$userIds', 'users');
  const lookupMessage = LookupMessage();
  const mainQuery = [{ $match }, { ...lookupUsers }, { ...lookupMessage }];
  return mainQuery;
};
