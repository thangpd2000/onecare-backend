import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

/**
 * First: type
 */
export enum ConversationType {
  DIRECT = 'DIRECT',
  GROUP = 'GROUP',
  CHANEL = 'CHANEL',
}

/**
 * Second: interface
 */
export class Conversation extends Document {
  @ApiProperty({
    description: "Id conversation",
    required: true
  })
  readonly _id: string;

  @ApiProperty({
    description: "Loại conversation: DIRECT | GROUP",
    required: true
  })
  readonly type: ConversationType;

  @ApiProperty({
    description: "Id Ảnh conversation",
    required: false
  })
  readonly avatarId?: string;

  @ApiProperty({
    description: "Id người dùng trong conversation",
    required: true
  })
  readonly userIds: string[];

  @ApiProperty({
    description: "Id người dùng đã xóa conversation",
    required: true
  })
  readonly deletedUserIds: string[];

  @ApiProperty({
    description: "Id người dùng đã tắt thông báo conversation",
    required: true
  })
  readonly userIdsHiddenNotitifcation: string[];

  @ApiProperty({
    description: "Id người tạo conversation",
    required: true
  })
  readonly createdBy: string;
  @ApiProperty({
    description: "Key định danh",
    required: true
  })
  key: string;
  @ApiProperty({
    description: "Thời gian tạo",
    required: true
  })
  readonly createdAt: Date;

  @ApiProperty({
    description: "Thời gian sửa",
    required: true
  })
  readonly updatedAt: Date;

  @ApiProperty({
    description: "Thời gian xóa",
    required: false,
    default: null
  })
  readonly deletedAt: Date;
}

/**
 * Three: response code
 */
export enum ConversationStatusCode {
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
  CONVERSATION_ID_CAN_NOT_BE_BLANK = 'CONVERSATION_ID_CAN_NOT_BE_BLANK',
  ID_CAN_NOT_BE_BLANK = 'ID_CAN_NOT_BE_BLANK',
  TYPE_CAN_NOT_BE_BLANK = 'TYPE_CAN_NOT_BE_BLANK',
  FRIEND_ID_CAN_NOT_BE_BLANK = 'FRIEND_ID_CAN_NOT_BE_BLANK',
  LIMIT_CAN_NOT_BE_BLANK = 'LIMIT_CAN_NOT_BE_BLANK',
  TYPE_INVALID = 'TYPE_INVALID',
  CONVERSATION_DO_NOT_EXISTS = 'CONVERSATION_DO_NOT_EXISTS',
}

/**
 * Four: params, query input
*/
export class ConversationInput {
  @ApiProperty({
    description: "Key định danh",
    required: true
  })
  key: string;

  @ApiProperty({
    description: "Kiểu conversation",
    required: true,
    enum: ConversationType
  })
  type: ConversationType;

  @ApiProperty({
    description: "Người dùng",
    required: true
  })
  userIds?: string[];
}

export class DirectParams {
  @ApiProperty({ description: 'Id bạn chat', required: true })
  friendId: string;
}

export class ListConversationParams {
  @ApiProperty({ description: 'Số item trả về', required: true })
  limit: string;
  @ApiProperty({ description: 'Lấy sau _id', required: false })
  after?: string;
  @ApiProperty({ description: 'UserId', required: true })
  userId: string;
}

export class ConversationParams {
  @ApiProperty({ description: 'Id nhóm chat', required: true })
  id: string;
}

export class UpdateConversationInput {
  lastMessageId?: string;
  deletedUserIds?: string[];
  userIdsHiddenNotitifcation?: string[];
  avatarId?: string;
  name?: string;
}

export class ConversationResponse {
  @ApiProperty({ description: 'Status', required: true, enum: ConversationStatusCode })
  status: ConversationStatusCode;
  @ApiProperty({ description: 'Data Nhóm chat', required: true })
  data: Conversation;
}

export class ListConversationResponse {
  @ApiProperty({ description: 'Status', required: true, enum: ConversationStatusCode })
  status: ConversationStatusCode;
  @ApiProperty({ description: 'Data danh sách Nhóm chat', required: true })
  data: Conversation[];
}
