import mongoose from 'mongoose';
import { ConversationType } from './conversation.types';

export const ConversationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      default: '',
    },
    key: String,
    avatarId: mongoose.Types.ObjectId,
    userIds: [mongoose.Types.ObjectId],
    lastMessageId: mongoose.Types.ObjectId,
    deletedUserIds: {
      type: [mongoose.Types.ObjectId],
      default: [],
    },
    userIdsHiddenNotitifcation: {
      type: [mongoose.Types.ObjectId],
      default: [],
    },
    createdBy: mongoose.Types.ObjectId,
    type: {
      type: ConversationType,
      default: ConversationType.DIRECT,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  },
);
