import { Controller, Get, UseGuards, Query, Req, Param } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import {
  ValidateDirectParams,
  ValidateListConversationParams,
} from './utils/conversation.validation';
import {
  DirectParams,
  ListConversationParams,
  ConversationParams,
  ConversationResponse,
  ListConversationResponse,
  ConversationType,
  ConversationStatusCode,
} from './conversation.types';
import { UserRequest } from '@modules/base.interface';
import { getDirectConversationKey } from './utils/conversation.utils';
import { ConversationService } from './conversation.service';
import { Roles } from '@modules/users/interfaces/user.entity';
import {
  ApiOperation,
  ApiOkResponse,
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { User } from '@modules/users/interfaces/user.interface';
@ApiTags('chat')
@Controller('chat/conversations')
export class ConversationController {
  constructor(
    private readonly conversationService: ConversationService,
    private readonly i18n: I18nService,
  ) {}

  // tạo nhóm chat 1-1
  @Get('direct-conversation')
  @ApiOperation({ summary: 'API Tạo nhóm chat 1-1' })
  @ApiOkResponse({ type: ConversationResponse })
  @ApiBearerAuth()
  @Roles('CUSTOMERS')
  @UseGuards(ValidateDirectParams)
  async getDirectConversation(
    @Query() query: DirectParams,
    @Req() req: UserRequest,
  ): Promise<ConversationResponse> {
    const { friendId } = query;
    const { _id: userId } = req.user;
    const key = getDirectConversationKey(userId, friendId);
    const existConversation = await this.conversationService.getExistConversation(
      {
        key,
        type: ConversationType.DIRECT,
      },
    );

    if (existConversation) {
      return {
        status: ConversationStatusCode.SUCCESS,
        data: existConversation,
      };
    }
    const conversation = await this.conversationService.create({
      key,
      userIds: [userId, friendId],
      type: ConversationType.DIRECT,
    });
    return {
      status: ConversationStatusCode.SUCCESS,
      data: conversation,
    };
  }

  // tạo nhóm chat 1-1
  @Get('check-direct-conversation')
  @ApiOperation({ summary: 'Check Tạo nhóm chat 1-1' })
  @ApiOkResponse({ type: ConversationResponse })
  @ApiBearerAuth()
  @Roles('CUSTOMERS')
  @UseGuards(ValidateDirectParams)
  async CheckDirectConversation(
    @Query() query: DirectParams,
    @Req() req: UserRequest,
  ): Promise<ConversationResponse> {
    const { friendId } = query;
    const { _id: userId } = req.user;
    const key = getDirectConversationKey(userId, friendId);
    const existConversation = await this.conversationService.getExistConversation(
      {
        key,
        type: ConversationType.DIRECT,
      },
    );
    return {
      status: ConversationStatusCode.SUCCESS,
      data: existConversation,
    };
  }
  // lấy danh sách nhóm chat
  @Get()
  @ApiOperation({ summary: 'API Lấy danh sách nhóm chat' })
  @ApiOkResponse({ type: ListConversationResponse })
  @ApiBearerAuth()
  @Roles('CUSTOMERS', 'STAFF')
  @UseGuards(ValidateListConversationParams)
  async listConversation(
    @Query() params: ListConversationParams,
    @Req() req: Request & { user: User },
  ): Promise<ListConversationResponse> {
    const conversations = await this.conversationService.listConversation(
      params,
      req.user,
    );
    return {
      status: ConversationStatusCode.SUCCESS,
      data: conversations,
    };
  }

  // lấy chi tiết nhóm chat
  @Get(':id')
  @ApiOperation({ summary: 'API Lấy chi tiết nhóm chat' })
  @ApiOkResponse({ type: ConversationResponse })
  @ApiBearerAuth()
  @Roles('CUSTOMERS')
  async conversation(
    @Param() params: ConversationParams,
  ): Promise<ConversationResponse> {
    const conversation = await this.conversationService.findById(params.id);
    return {
      status: conversation
        ? ConversationStatusCode.SUCCESS
        : ConversationStatusCode.CONVERSATION_DO_NOT_EXISTS,
      data: conversation || null,
    };
  }
}
