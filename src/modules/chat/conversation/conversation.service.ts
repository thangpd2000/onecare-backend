import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  ConversationInput,
  ListConversationParams,
  UpdateConversationInput,
  Conversation,
  ConversationType,
} from './conversation.types';
import {
  ListConversationQuery,
  ConversationQuery,
} from './utils/conversation.queries';
import { UpdatedResponse } from '@modules/base.interface';
import { User } from '@modules/users/interfaces/user.interface';
// import mongoose from 'mongoose';
@Injectable()
export class ConversationService {
  constructor(
    @InjectModel('Conversation')
    private readonly ConversationModel: Model<Conversation>,
  ) {}

  async create(body: ConversationInput): Promise<Conversation> {
    const createdData = new this.ConversationModel(body);
    return await createdData.save();
  }

  async listConversation(
    params: ListConversationParams,
    user: User,
  ): Promise<Conversation[]> {
    params.userId = user._id;
    // const ObjectId = mongoose.Types.ObjectId;
    const mainQuery = ListConversationQuery(params);
    return await this.ConversationModel.aggregate(
      mainQuery,
      // [
      //   { $unwind : "$userIds" } ,
      //   {$match :{
      //     userIds: ObjectId(params.userId)

      //   }},

      // ]
    ).exec();
  }

  async getExistConversation(params: {
    type: ConversationType;
    key: string;
  }): Promise<Conversation> {
    return await this.ConversationModel.findOne(params).lean()
    .exec() as Conversation;
  }

  async update(
    _id: string,
    input: UpdateConversationInput,
  ): Promise<UpdatedResponse> {
    const $set: any = input;
    return await this.ConversationModel.updateOne(
      { _id },
      { $set },
      { new: true },
    )
  }

  async findById(conversationId: string): Promise<Conversation> {
    const mainQuery = ConversationQuery(conversationId);
    const res = await this.ConversationModel.aggregate(mainQuery).exec();
    return res[0];
  }
  async findOne(conversationId: string): Promise<Conversation> {
    const res = await this.ConversationModel.findOne({ _id: conversationId });
    return res;
  }
}
