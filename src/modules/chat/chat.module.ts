import { RequireAuth } from '@modules/users/utils/user.guard';
import { Module } from '@nestjs/common';
import { ConversationModule } from './conversation/conversation.module';
import { MessageModule } from './message/message.module';
import { ChatGateway } from './socket/chat.socket';

@Module({
  imports: [ConversationModule, MessageModule ] ,
  exports: [ChatGateway],
  providers: [ ChatGateway, RequireAuth]
})
export class ChatModule {}
