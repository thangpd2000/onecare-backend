import { isEmpty } from '@helper/function.helper';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { UserError } from '../../../../helper/error.helper';
import { MessageType, MessageStatusCode } from '../message.types';

@Injectable()
export class ValidateMessageInput implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const { conversationId, type, text, mediaIds } = request.body;
    if (isEmpty(type)) {
      throw new UserError(MessageStatusCode.TYPE_CAN_NOT_BE_BLANK);
    }

    if (!Object.values(MessageType).includes(type)) {
      throw new UserError(MessageStatusCode.TYPE_INVALID);
    }
   
    if (isEmpty(conversationId)) {
      throw new UserError(MessageStatusCode.CONVERSATION_ID_CAN_NOT_BE_BLANK);
    }

    if (type === MessageType.TEXT && (!text || text.trim() === '')) {
      throw new UserError(MessageStatusCode.TEXT_CAN_NOT_BE_BLANK);
    }

    if (type === MessageType.PHOTO && (!mediaIds || mediaIds.length === 0)) {
      throw new UserError(MessageStatusCode.MEDIA_IDS_CAN_NOT_BE_BLANK);
    }

    return true;
  }
}

@Injectable()
export class ValidateMessageParams implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const { conversationId, limit } = request.query;
    if (isEmpty(conversationId)) {
      throw new UserError(MessageStatusCode.CONVERSATION_ID_CAN_NOT_BE_BLANK);
    }

    if (isEmpty(limit)) {
      throw new UserError(MessageStatusCode.LIMIT_CAN_NOT_BE_BLANK);
    }

    return true;
  }
}
