import { Types } from 'mongoose';
import {
  LookupUsers,
  LookupUser,
  LookupMedias,
} from '@helper/queries.helper';
import { HistoriesParams, MessageParams } from '../message.types';

export const historiesMessageQuery = (params: HistoriesParams): any[] => {
  const { conversationId, limit, after } = params;
  const $limit = parseInt(limit);
  const $match: any = {
    conversationId: new Types.ObjectId(conversationId),
  };
  if (after) {
    $match._id = { $gt: new Types.ObjectId(after) };
  }
  const lookupUsers = LookupUsers('$seenUserIds', 'seenUsers');
  const lookupUser = LookupUser();
  const lookupMeidas = LookupMedias();
  const mainQuery = [
    { $match },
    { ...lookupUsers },
    { ...lookupUser },
    { ...lookupMeidas },
    { $sort : { createdAt : 1} },
    { $limit },
  ];
  return mainQuery;
};

export const messageQuery = ({ messageId }: MessageParams): any[] => {
  const lookupMeidas = LookupMedias();
  const mainQuery = [{ _id: messageId }, { ...lookupMeidas }];
  return mainQuery;
};
