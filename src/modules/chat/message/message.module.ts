import { ConversationModule } from '@modules/chat/conversation/conversation.module';
import { MediaSchema } from '@modules/media/schemas/media.schema';
import { UserSchema } from '@modules/users/schemas/user.schema';
import { RequireAuth } from '@modules/users/utils/user.guard';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConversationSchema } from '../conversation/conversation.schema';
import { ChatGateway } from '../socket/chat.socket';
import { MessageController } from './message.controller';
import { MessageSchema } from './message.schema';
import { MessageService } from './message.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Message', schema: MessageSchema },
      { name: 'Conversation' , schema: ConversationSchema},
      { name: 'User', schema: UserSchema },
      { name: 'Media', schema: MediaSchema }
    ]),
    ConversationModule
  ],
  controllers: [MessageController],
  exports: [ChatGateway],
  providers: [MessageService, ChatGateway, RequireAuth]
})

export class MessageModule {}
