import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

/**
 * First: type
 */
export enum MessageType {
  TEXT = 'TEXT',
  PHOTO = 'PHOTO',
  VIDEO = 'VIDEO',
  VOICE = 'VOICE',
}

/**
 * Second: interface
 */
export class Message extends Document {
  @ApiProperty({
    description: "Id tin nhắn",
    required: true
  })
  readonly _id: string;

  @ApiProperty({
    description: "Nội dung tin nhắn văn bản",
    required: false
  })
  readonly text?: string;

  @ApiProperty({
    description: "Loại tin nhắn",
    required: true,
    enum: MessageType
  })
  readonly type: MessageType;

  @ApiProperty({
    description: "Id tin nhắn hình ảnh",
    required: true
  })
  readonly mediaIds?: string[];

  @ApiProperty({
    description: "Id người xem tin nhắn",
    required: true
  })
  readonly seenUserIds: string[];

  @ApiProperty({
    description: "Id người xóa tin nhắn",
    required: true
  })
  readonly deletedUserIds: string[];

  @ApiProperty({
    description: "Id người tạo tin nhắn",
    required: true
  })
  readonly createdBy: string;

  @ApiProperty({
    description: "Id nhóm chat",
    required: true
  })
  readonly conversationId: string;

  @ApiProperty({
    description: "Thời gian tạo tin nhắn",
    required: true
  })
  readonly createdAt: Date;

  @ApiProperty({
    description: "Thời gian sửa tin nhắn",
    required: true
  })
  readonly updatedAt: Date;

  @ApiProperty({
    description: "Thời gian xóa tin nhắn",
    required: true,
    default: null
  })
  readonly deletedAt: Date;
}

/**
 * Three: response code
 */
export enum MessageStatusCode {
  SUCCESS = 'SUCCESS',
  FAILURE = 'FAILURE',
  CONVERSATION_ID_CAN_NOT_BE_BLANK = 'CONVERSATION_ID_CAN_NOT_BE_BLANK',
  ID_CAN_NOT_BE_BLANK = 'ID_CAN_NOT_BE_BLANK',
  TYPE_CAN_NOT_BE_BLANK = 'TYPE_CAN_NOT_BE_BLANK',
  FRIEND_ID_CAN_NOT_BE_BLANK = 'FRIEND_ID_CAN_NOT_BE_BLANK',
  LIMIT_CAN_NOT_BE_BLANK = 'LIMIT_CAN_NOT_BE_BLANK',
  TYPE_INVALID = 'TYPE_INVALID',
  TEXT_CAN_NOT_BE_BLANK = 'TEXT_CAN_NOT_BE_BLANK',
  MEDIA_IDS_CAN_NOT_BE_BLANK = 'MEDIA_IDS_CAN_NOT_BE_BLANK',
}

/**
 * Four: params, query input
*/
export class MessageInput {
  @ApiProperty({
    description: "Nội dung tin nhắn",
    required: false
  })
  text?: string;

  @ApiProperty({
    description: "Loại tin nhắn",
    required: true,
    enum: MessageType
  })
  type: MessageType;

  @ApiProperty({
    description: "Id tin nhắn hình ảnh",
    required: false
  })
  mediaIds?: string[];

  @ApiProperty({
    description: "Id nhóm chat",
    required: true
  })
  conversationId: string;

  @ApiProperty({
    description: "Id người tạo tin nhắn",
    required: false
  })
  createdBy: string;

  @ApiProperty({
    description: "Id người xem tin nhắn",
    required: false
  })
  seenUserIds: string[];
}

export class HistoriesParams {
  @ApiProperty({
    description: "Id nhóm chat",
    required: true
  })
  conversationId: string;

  @ApiProperty({
    description: "Id người tạo tin nhắn",
    required: false
  })
  userId?: string;

  @ApiProperty({
    description: "Số record trả về",
    required: true
  })
  limit: string;

  @ApiProperty({
    description: "Id tin nhắn trước",
    required: false
  })
  after?: string;
}

export class MessageParams {
  @ApiProperty({
    description: "Id tin nhắn",
    required: true
  })
  messageId: string;
}

export class HistoriesResponse {
  @ApiProperty({
    description: "Status code",
    required: true,
    enum: MessageStatusCode
  })
  status: MessageStatusCode;

  @ApiProperty({
    description: "Danh sách tin nhắn đã gửi",
    required: true
  })
  data: Message[];
}

export class CreateMessageResponse {
  @ApiProperty({
    description: "Status code",
    required: true,
    enum: MessageStatusCode
  })
  status: MessageStatusCode;

  @ApiProperty({
    description: "Tin nhắn đã gửi",
    required: true
  })
  data: Message;
}
