import OnesignalHelper from '@helper/onesignal.helper';
import { RoomSocket, UserRequest } from '@modules/base.interface';
import { Roles } from '@modules/users/interfaces/user.entity';
import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import {
  CreateMessageResponse,
  HistoriesParams,
  HistoriesResponse,
  MessageInput,
  MessageStatusCode,
  MessageType,
  Message
} from './message.types';
import { MessageService } from './message.service';
import {
  ValidateMessageInput,
  ValidateMessageParams,
} from './utils/message.validation';
import { ConversationService } from '../conversation/conversation.service';
import { ApiOperation, ApiOkResponse, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { ChatGateway } from '../socket/chat.socket';

@ApiTags('chat')
@Controller('chat/message')
export class MessageController {
  constructor(
    private readonly messageService: MessageService,
    private readonly conversationService: ConversationService,
    private readonly i18n: I18nService,
    private readonly chatGatewayService: ChatGateway,
  ) {}

  // lấy lịch sử chat
  @Get()
  @ApiOperation({ summary: 'API lấy lịch sử chat'})
  @ApiBearerAuth()
  @ApiOkResponse({ type: HistoriesResponse })
  @Roles('CUSTOMERS')
  @UseGuards( ValidateMessageParams)

  async conversationMessages(
    @Query() query: HistoriesParams,
  ): Promise<HistoriesResponse> {
    const messsages = await this.messageService.histories(query);
    return {
      status: MessageStatusCode.SUCCESS,
      data: messsages,
    };
  }

  // tạo tin nhắn
  @Post()
  @ApiOperation({ summary: 'API tạo tin nhắn'})
  @ApiOkResponse({ type: CreateMessageResponse })
  @ApiBearerAuth()
  @Roles('CUSTOMERS')
  @UseGuards( ValidateMessageInput)
  async createMessage(
    @Body() body: MessageInput,
    @Req() req: UserRequest,
  ): Promise<CreateMessageResponse> {
    const { _id: userId } = req.user;
    let messsage = await this.messageService.create({
      ...body,
      createdBy: userId,
      seenUserIds: [userId],
    });
    await this.conversationService.update(body.conversationId, {
      lastMessageId: messsage._id,
    });
    if (body.type === MessageType.PHOTO) {
      messsage = await this.messageService.detail({ messageId: messsage._id });
    }
    const conversation=  await this.conversationService.findOne(body.conversationId)
    const room =  `${RoomSocket.SUPPORT}:${conversation.key}`;
    // this.chatGatewayService.sendAppToApp(room, messsage);
    // todo check hệ thống có bắn thông báo
    // OnesignalHelper.sendToAlls({
    //   title: 'New message',
    //   content: 'ok'
    // })

    return {
      status: MessageStatusCode.SUCCESS,
      data: messsage,
    };
  }
}
