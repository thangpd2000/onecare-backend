import mongoose from 'mongoose';
import { MessageType } from './message.types';

export const MessageSchema = new mongoose.Schema(
  {
    text: String,
    conversationId: mongoose.Types.ObjectId,
    mediaIds: [mongoose.Types.ObjectId],
    seenUserIds: {
      type: [mongoose.Types.ObjectId],
      default: [],
    },
    deletedUserIds: {
      type: [mongoose.Types.ObjectId],
      default: [],
    },
    createdBy: mongoose.Types.ObjectId,
    type: {
      type: MessageType,
      default: MessageType.TEXT,
    },
  },
  {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  },
);
