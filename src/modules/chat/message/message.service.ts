import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Message, MessageInput, HistoriesParams, MessageParams } from './message.types';
import { historiesMessageQuery, messageQuery } from './utils/message.queries';

@Injectable()
export class MessageService {
  constructor(@InjectModel('Message') private readonly messageModel: Model<Message>) { }

  async create(body: MessageInput): Promise<Message> {
    const createdData = new this.messageModel(body);
    return await createdData.save();
  }

  async histories(params: HistoriesParams): Promise<Message[]> {
    const mainQuery: any[] = historiesMessageQuery(params);

    return await this.messageModel.aggregate(mainQuery).exec();
  }

  async detail(params: MessageParams): Promise<Message> {
    const mainQuery: any[] = messageQuery(params);

    return await this.messageModel.aggregate(mainQuery).exec();
  }

}
