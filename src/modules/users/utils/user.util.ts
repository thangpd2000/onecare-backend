import * as bcrypt from 'bcrypt-nodejs';

export default {
  /**
   * Hash password
   * @param originPassword
   * @returns {Promise}
   */
  hash(originPassword) {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) reject(err);
        bcrypt.hash(originPassword, salt, null, (err, hash: string) => {
          if (err) {
            console.log('BcryptService - hash method: ', err);
            reject(err);
          } else resolve(hash);
        });
      });
    });
  },

  compare(origin, hash) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(origin, hash, (err, result) => {
        if (err) {
          console.log('BcryptService - compare method: ', err);
          reject(err);
        } else resolve(result);
      });
    });
  },
};

export const updatePointRanking = async (
  userModel,
  pointRankingModel,
  userId,
) => {
  const user = await userModel.findOne({ _id: userId });
  const pointRankings = await pointRankingModel
    .find()
    .sort({ targetPoint: -1 });
  let pointRanking;
  for (const point of pointRankings) {
    if (user.point >= point.targetPoint) {
      pointRanking = point._id;
      break;
    }
  }
  if (pointRanking) {
    await userModel.findByIdAndUpdate(userId, {
      pointRankingId: pointRanking._id,
    });
  }
};
