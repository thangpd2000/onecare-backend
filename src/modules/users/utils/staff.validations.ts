import { LoginCmsInput } from '../interfaces/auth.entity';
import {
  CreateStaffInput,
  DetailStaffInput,
  ListStaffInput,
  UpdateStaffInput,
} from '../interfaces/staff.entity';
import yup from './user.yup';
export const validateCreateStaff = (data: CreateStaffInput) => {
  const basedRules: any = {
    username: yup
      .string()
      .min(6)
      .required(),
    email: yup
      .string()
      .email()
      .required(),
    name: yup
      .string()
      .required()
      .min(2)
      .max(100),
    phoneNumber: yup
      .string()
      .max(10)
      .matches(/^$|^[0][0-9]+$/, 'Số điện thoại không đúng định dạng'),
    dateOfBirth: yup.string(),
    password: yup
      .string()
      .required()
      .min(6),
    status: yup.boolean().required(),
    type: yup
      .string()
      .typeValidate()
      .required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateUpdateStaff = (data: UpdateStaffInput) => {
  const basedRules: any = {
    _id: yup.string().required(),
    email: yup
      .string()
      .email()
      .required(),
    name: yup
      .string()
      .required()
      .min(2)
      .max(100),
    phoneNumber: yup
      .string()
      .max(10)
      .matches(/^$|^[0][0-9]+$/, 'Số điện thoại không đúng định dạng'),
    dateOfBirth: yup.string(),
    password: yup.string().min(6),
    status: yup.boolean().required(),
    type: yup
      .string()
      .typeValidate()
      .required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateDetailStaff = (data: DetailStaffInput) => {
  const basedRules: any = {
    id: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateListStaff = (data: ListStaffInput) => {
  const basedRules: any = {
    limit: yup.string().required(),
    page: yup.string().required(),
    search: yup.string(),
    status: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
export const validateLoginCms = (data: LoginCmsInput) => {
  const basedRules: any = {
    username: yup.string().required(),
    password: yup.string().required(),
  };
};
export const validateDeleteStaff = (data: DetailStaffInput) => {
  const schema = yup.object().shape({ id: yup.string().required() });
  schema.validateSync(data);
};
