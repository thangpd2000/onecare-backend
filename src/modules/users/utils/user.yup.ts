import * as yup from 'yup';
import { StringSchema } from 'yup';
import { GenderType, Type } from '../interfaces/user.entity';
declare module 'yup' {
  interface StringSchema {
    genderValidate(): StringSchema;
  }
  interface StringSchema {
    typeValidate(): StringSchema;
  }
}
function genderValidate(this: StringSchema) {
  return this.test('genderValidate', '', function(gender: string | undefined) {
    if (!gender) return true;
    const { path, createError } = this;
    if (!(gender in GenderType)) {
      return createError({
        path,
        message: 'gender wrong format',
      });
    }
    return true;
  });
}
yup.addMethod(yup.string, 'genderValidate', genderValidate);

function typeValidate(this: StringSchema) {
  return this.test('typeValidate', '', function(type: string | undefined) {
    const { path, createError } = this;
    if (!(type in Type)) {
      return createError({
        path,
        message: 'type wrong format',
      });
    }
    return true;
  });
}
yup.addMethod(yup.string, 'genderValidate', genderValidate);
yup.addMethod(yup.string, 'typeValidate', typeValidate);
export default yup;
