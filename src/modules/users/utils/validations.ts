import yup from './user.yup';
import { ForgotPasswordInput, LoginInput, RefreshTokenInput } from '../interfaces/auth.entity';
import {
  RegisterUserInput,
  CheckPhoneInput,
  DetailUserInput,
  // UpdateUserInput,
  // GenderType,
  UpdateUserAppInput,
  UploadAvatarAppInput,
  ListUserInput,
  UpdateInfoUserInput,
  ChangePassword,
  UpdateDeliveryAddressInput,
} from '../interfaces/user.entity';
import userUtil from '../utils/user.util';
// import { User } from '../interfaces/user.interface';
import { BaseInput, StatusCodes } from '@modules/base.interface';
import { UserErrors } from '@helper/error.helper';

export const validateLoginInput = (data: LoginInput) => {
  const schema = yup.object().shape({
    email: yup
      .string()
      .required(),
    password: yup
      .string()
      .min(6)
      .max(2000)
      .required(),
  });

  schema.validateSync(data);
};

export const userValidations = (data: RegisterUserInput) => {
  const basedRules: any = {
    phoneNumber: yup
      .string()
      .required()
      .matches(/^[0][0-9]+$/, 'Số điện thoại không đúng định dạng'),
    email: yup
      .string()
      .required()
      .email(),
    name: yup
      .string()
      .required()
      .min(2)
      .max(100),
    password: yup
      .string()
      .required()
      .min(6),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
export const ForgotValidations = (data: ForgotPasswordInput) => {
  const basedRules: any = {
    email: yup
      .string()
      .required()
      .email(),
    verifyToken:yup.string().required(),
    password: yup
      .string()
      .required()
      .min(6),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
export const phoneNumberValidations = (data: CheckPhoneInput) => {
  const basedRules: any = {
    phoneNumber: yup
      .string()
      .required()
      .matches(/^[0][0-9]+$/, 'Số điện thoại không đúng định dạng'),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const detailUserValidation = (data: DetailUserInput) => {
  const basedRules: any = {
    id: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const tokenValidation = (data: RefreshTokenInput) => {
  const basedRules: any = {
    refreshToken: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const updateUserValidation = (data: UpdateUserAppInput) => {
  const basedRules: any = {
    _id: yup.string().required(),
    name: yup
      .string()
      .min(2)
      .max(100)
      .required(),
    email: yup.string().required(),
    location: yup.string(),
    password: yup.string(),
    dateOfBirth: yup.string(),
    gender: yup.string().genderValidate(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const uploadAvatarValidation = (data: UploadAvatarAppInput) => {
  const basedRules: any = {
    _id: yup.string().required(),
    avatar: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const listUserValidation = (data: ListUserInput) => {
  const schema = yup.object().shape({
    search: yup.string(),
    limit: yup.number().integer(),
    page: yup.number().integer(),
    active: yup.boolean(),
    status: yup.string(),
  });

  schema.validateSync(data);
};

export const validateUpdateInfoUserInput = (data: UpdateInfoUserInput) => {
  const schema = yup.object().shape({
    _id: yup.string().required(),
    name: yup.string(),
    email: yup.string(),
    dateOfBirth: yup.string(),
    gender: yup.string().genderValidate(),
    location: yup.string(),
    avatar: yup.string(),
    status: yup.string(),
    pointsUsed: yup.number().min(0),
  });
  schema.validateSync(data);
};
export const changePasswordValidation = async (
  data: ChangePassword,
  oldPassword: string,
) => {
  const basedRules: any = {
    password: yup.string().required(),
    newPassword: yup
      .string()
      .required()
      .min(6),
    passwordConfirmed: yup
      .string()
      .oneOf([yup.ref('newPassword'), null], 'Passwords must match')
      .required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
  const checkPassword = await userUtil.compare(data.password, oldPassword);
  if (!checkPassword) {
    UserErrors(StatusCodes.PASSWORD_INCORRECT);
  }
};

export const addDeliveryAddressValidation = (
  data: UpdateDeliveryAddressInput,
) => {
  const basedRules: any = {
    receiverName: yup
      .string()
      .required()
      .min(2)
      .max(100),
    receiverPhone: yup
      .string()
      .required()
      .min(10)
      .max(12),
    detail: yup.string(),
    cityId: yup.string(),
    districtId: yup.string(),
    wardId: yup.string(),
    lat: yup.number(),
    lon: yup.number(),
    isDefault: yup.boolean().required(),
  };
  if (data._id) {
    basedRules._id = yup.string().required();
  }
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateDeliveryAddress = (data: BaseInput) => {
  const schema = yup.object().shape({
    id: yup.string().required(),
  });
  schema.validateSync(data);
};
