import {
  CanActivate,
  ExecutionContext,
  Injectable,
  HttpStatus,
} from '@nestjs/common';
import { decodeToken } from '@utils/jwt';
import { UserService } from '../services/user.service';
import { StatusCodes } from '@modules/base.interface';
import { UserErrors } from '@helper/error.helper';
import { Type } from '../interfaces/user.entity';
import { StaffService } from '../services/staff.service';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RequireAuth implements CanActivate {
  constructor(
    private reflector: Reflector,
    private readonly userService: UserService,
    private readonly staffService: StaffService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const req = context.switchToHttp().getRequest();
    if (!req.headers || !req.headers.authorization) {
      UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
    }
    await this.staffService.checkExistToken(req.headers.authorization);
    const decoded = await this.validateToken(req.headers.authorization);
    if (!decoded || !decoded.userId) {
      UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
    }

    const user = await this.userService.findById(decoded.userId);
    if (user) {
      req.user = user;
    }
    if (user.type === Type.ADMIN) {
      return true;
    }
    return true;
  }
  async validateToken(auth: string) {
    try {
      if (auth.split(' ')[0] !== 'Bearer') {
        UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
      }
      const token = auth.split(' ')[1];
      const decoded = await decodeToken(token);
      return decoded;
    } catch (error) {
      console.log('validateToken error: ', error);
      UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
    }
  }
}
@Injectable()
export class GetUser implements CanActivate {
  constructor(private readonly userService: UserService) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const auth = req.headers.authorization;
    if (auth) {
      const token = auth.split(' ')[1];
      if (token && token != 'null' && token != null) {
        const decoded = await decodeToken(token);
        if (decoded && decoded.userId) {
          const user = await this.userService.findById(decoded.userId);
          if (user) {
            req.user = user;
          }
        }
      }
    }
    return true;
  }

  async validateToken(auth: string) {
    try {
      if (auth.split(' ')[0] !== 'Bearer') {
        UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
      }
      const token = auth.split(' ')[1];
      const decoded = await decodeToken(token);
      return decoded;
    } catch (error) {
      console.log('validateToken error: ', error);
      UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
    }
  }
}
