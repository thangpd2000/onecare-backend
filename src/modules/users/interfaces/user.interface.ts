import { District } from '../../address/interfaces/district.interface';
import { City } from '../../address/interfaces/city.interface';
import { Document } from 'mongoose';
import { GenderType, OnlineStatus, Type } from './user.entity';
import { Ward } from '@modules/address/interfaces/ward.interface';
import { BaseStatus } from '@modules/base.interface';


export interface User extends Document {
  readonly _id: string;
  readonly name: string;
  readonly phoneNumber: string;
  readonly username: string;
  readonly email: string;
  password: string;
  readonly avatar: string;
  readonly type: Type;
  readonly status: BaseStatus;
  readonly groups: string;
  readonly age: number;
  readonly online: OnlineStatus;
  readonly gender: GenderType;
  readonly lastOnlineTime: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
  verifyToken:string;
  unreadNotification: number;
  readonly conversationId: string;
}

export interface UserRecord {
  readonly _id: string;
  readonly name: string;
  readonly phoneNumber: string;
  readonly username: string;
  readonly email: string;
  password: string;
  readonly avatar: string;
  readonly type: Type;
  readonly status: BaseStatus;
  readonly groups: string;
  readonly age: number;
  readonly online: OnlineStatus;
  readonly gender: GenderType;
  readonly lastOnlineTime: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
  unreadNotification: number;
  verifyToken:string;
  readonly conversationId: string;
}

export interface DeliveryAddress extends Document {
  readonly _id: string;
  readonly receiverName: string;
  readonly receiverPhone: string;
  readonly receiverAddress: string;
  readonly cityId: string;
  readonly city: City;
  readonly districtId: string;
  readonly district: District;
  readonly wardId: string;
  readonly ward: Ward;
  readonly detail: string;
  readonly isDefault: boolean;
  readonly createdBy: string;
  readonly creator?: User;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
}