import {
  User,
  DeliveryAddress,
  UserRecord,

} from './user.interface';
import { StatusCodes, BaseResponse } from '@modules/base.interface';
import { Token } from './token.interface';
import { SetMetadata } from '@nestjs/common';

export enum OnlineStatus {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}
export enum Type {
  STAFF = 'STAFF',
  CUSTOMERS = 'CUSTOMERS',
  ADMIN = 'ADMIN',
}

export const Roles = (...roles: string[]) => SetMetadata('roles', roles);
export class UserInput {
  name: string;
  phoneNumber?: string;
  username?: string;
  password: string;
  age?: number;
  gender?: GenderType;
  avatarId?: string;
  deviceId?: string;
  country?: Country;
  status?: OnlineStatus;
  online?: OnlineStatus;
  type:Type
}

export class RegisterUserInput {
  phoneNumber: string;
  name: string;
  email: string;
  password: string;
  verifyToken:string
  isVerified: boolean
  type:Type
}
export class RegisterUserResponse {
  user: User;
  status: StatusCodes;
}

export class UpdateUserInput {
  name?: string;
  phoneNumber?: string;
  username?: string;
  password?: string;
  age?: number;
  gender?: GenderType;
  avatarId?: string;
  deviceId?: string;
  country?: Country;
  status?: OnlineStatus;
  online?: OnlineStatus;
  lastOnlineTime?: Date;
}

export class CheckUserNameResponse {
  user: User;
  status: StatusCodes;
}

export class CheckPhoneResponse {
  phoneNumber: string;
  status: StatusCodes;
}

export enum GenderType {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHER = 'OTHER',
}

export class UpdateUserResponse {
  status: StatusCodes;
  user?: User;
}

export class CheckUserNameInput {
  username: string;
}
export class CheckPhoneInput {
  phoneNumber: string;
}

export class Country {
  _id: string;
  name: string;
}

export class Language {
  _id: string;
  name: string;
}

export class FetchCountryLanguageResponse {
  status: StatusCodes;
  countries: Country[];
  languages: Language[];
}

export class MatchingUserResponse {
  status: StatusCodes;
  data: User[];
}

export class LogOutInput {
  id: string;
}

export class LogOutResponse {
  status: StatusCodes;
  token: Token;
}

export class DetailUserInput {
  id: string;
}

export class DetailUserResponse {
  status: StatusCodes;
  user: UserRecord;
}
export class EmailVerifiedParam {
  token: string;
}
export class EmailVerifiedResponse {
  status: StatusCodes;
}
export class UpdateUserAppInput {
  _id: string;
  name?: string;
  email?: string;
  location?: string;
  password?: string;
  dateOfBirth?: string;
  gender?: GenderType;
}

export class UploadAvatarAppInput {
  _id: string;
  avatar: string;
}

export class UpdateUserAppInputId {
  id: string;
}

export class UpdateUserAppResponse {
  status: StatusCodes;
  user?: User;
}
export class ListUserInput {
  limit: string;
  page: string;
  search: string;
  status: string;
  rank: string;
}

export class ListUserResponse {
  status?: StatusCodes;
  data: UserRecord[];
  total: number;
}

export class UpdateInfoUserInputId {
  id: string;
}

export class UpdateInfoUserInput {
  _id: string;
  name: string;
  email: string;
  dateOfBirth: string;
  gender: GenderType;
  location: string;
  avatar: string;
  status: string;
}

export class UpdateInfoUserResponse {
  status?: StatusCodes;
  data: User;
}
/**
 *
 *
 * @export
 * @class ChangePassword
 */
export class ChangePassword {
  password: string;
  newPassword: string;
  passwordConfirmed: string;
}

export class UpdateDeliveryAddressInput {
  _id?: string;
  receiverName: string;
  receiverPhone: string;
  cityId: string;
  districtId: string;
  wardId: string;
  lat: string;
  lon: string;
  detail: string;
  isDefault: boolean;
}

export class DeliveryAddressResponse {
  status: StatusCodes;
  data: DeliveryAddress;
}

export class ListDeliveryAddressResponse {
  status: StatusCodes;
  data: DeliveryAddress[];
}

export class BaseResponseUser implements BaseResponse {
  status: StatusCodes;
}

export class RankUserResponse {
  status?: StatusCodes;
  data: User;
  total: number;
}
export class ListDeliveryAddress {
  isDefault: boolean;
  limit: string;
  page: string;
}
