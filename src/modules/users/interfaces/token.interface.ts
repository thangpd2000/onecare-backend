import { Document } from 'mongoose';
import { GenderType, OnlineStatus } from './user.entity';

export interface Token extends Document {
  readonly _id: string;
  readonly token: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}