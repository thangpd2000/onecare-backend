import { StatusCodes } from '@modules/base.interface';
import { User } from './user.interface';

export class CreateStaffInput {
  username: string;
  email: string;
  name: string;
  phoneNumber: string;
  dateOfBirth: string;
  password: string;
  status: boolean;
  type: string;
}

export class UpdateStaffInput {
  _id: string;
  email: string;
  name: string;
  phoneNumber: string;
  dateOfBirth: string;
  password: string;
  status: boolean;
  type: string;
}

export class StaffResponse {
  status: StatusCodes;
  data: User;
}
export class UpdateStaffInputId {
  id: string;
}

export class DetailStaffInput {
  id: string;
}

export class ListStaffInput {
  limit: string;
  page: string;
  search: string;
  status: string;
}
export class ListStaffResponse {
  status?: StatusCodes;
  data: User[];
  total: number;
}
export class DeleteStaffResponse {
  status: StatusCodes;
  data: boolean;
}
