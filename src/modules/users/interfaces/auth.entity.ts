import { User } from './user.interface';
import { StatusCodes } from '@modules/base.interface';

export class LoginInput {
  email: string;
  password: string;
}
export class ForgotPasswordInput {
  email: string;
  password: string;
  verifyToken: string;
}
export class EmailForgotPasswordInput {
  email: string;

}
export class LoginCmsInput {
  username: string;
  password: string;
}

export class AuthResponse {
  user: User;
  token: string;
  refreshToken: string;
  status?: StatusCodes;
  expiresAt?: string;
}

export class IDTokenPayload {
  token: string;
  refreshToken: string;
  user: User;
  expiresAt: string;
}
export class RefreshTokenInput {
  refreshToken: string;
}
