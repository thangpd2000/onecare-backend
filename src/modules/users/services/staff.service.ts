import { UserErrors } from '@helper/error.helper';
import { BaseStatus, StatusCodes } from '@modules/base.interface';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  CreateStaffInput,
  DetailStaffInput,
  ListStaffInput,
  ListStaffResponse,
  UpdateStaffInput,
} from '../interfaces/staff.entity';
import { User } from '../interfaces/user.interface';
import {
  validateCreateStaff,
  validateDeleteStaff,
  validateDetailStaff,
  validateListStaff,
  validateLoginCms,
  validateUpdateStaff,
} from '../utils/staff.validations';
import { omit } from 'lodash';
import { UserService } from './user.service';
import { Type } from '../interfaces/user.entity';
import { AuthResponse, LoginCmsInput } from '../interfaces/auth.entity';
import userUtil from '../utils/user.util';
import { generateUserToken } from '@utils/jwt';
import { JWTAuthTokenType } from '@utils/jwt.entity';
import ms from 'ms';
import { Token } from '../interfaces/token.interface';
import { ExerciseRegineService } from '@modules/exercise-regime/services/exercise-regime.sevices';
@Injectable()
export class StaffService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    @InjectModel('Token') private readonly tokenModel: Model<Token>,
    private readonly userService: UserService,
    private readonly exerciseRegineService: ExerciseRegineService,
  ) {}
  async loginStaff(data: LoginCmsInput): Promise<AuthResponse> {
    validateLoginCms(data);
    console.log('data', data);
    const user = await this.userModel
      .findOne({
        username: data.username,
        deletedAt: null,
        status:BaseStatus.ACTIVE
      })
      .exec();
    if (!user) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS, "Người dùng không tồn tại hoặc chưa được kích hoạt");
    }
    const checkPassword = await userUtil.compare(data.password, user.password);
    if (!checkPassword) {
      UserErrors(StatusCodes.PASSWORD_INCORRECT,"Mật khẩu không đúng");
    }
    return {
      token: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.ID_TOKEN,
      }),
      refreshToken: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.REFRESH_TOKEN,
      }),
      user,
      expiresAt: new Date(
        new Date().getTime() + ms(process.env.JWT_ID_TOKEN_EXPIRES),
      ).toISOString(),
    };
  }
  async checkExistToken(data: string): Promise<any> {
    const token = await this.tokenModel.exists({ token: data });
    if (token) {
      UserErrors(StatusCodes.INVALID_TOKEN, HttpStatus.FORBIDDEN);
    }
  }
  async logout(header: any): Promise<Token> {
    if (header.authorization) {
      return await this.tokenModel.create({ token: header.authorization });
    }
  }
  async createStaff(data: CreateStaffInput): Promise<User> {
    validateCreateStaff(data);
    const isDuplicatedName = await this.userModel.exists({
      username: data.username,
      deletedAt: null,
    });
    if (isDuplicatedName) {
      UserErrors(StatusCodes.USER_ALREADY_EXISTS,"User đã tồn tại");
    }
    if (data.phoneNumber) {
      await this.userService.checkPhone(
        { phoneNumber: data.phoneNumber },
        Type.STAFF,
      );
    }

    const user = new this.userModel(data);
    return await user.save();
  }
  async detailStaff(data: DetailStaffInput): Promise<User> {
    validateDetailStaff(data);
    const user = await this.userModel
      .findOne({
        _id: data.id,
        deletedAt: null,
      })
      .exec();
    if (!user) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS);
    }
    return user;
  }
  async updateStaff(
    data: UpdateStaffInput,
    cmsPermisionGateway: any,
  ): Promise<User> {
    validateUpdateStaff(data);
    const checkExist = await this.userModel.findOne({
      _id: data._id,
      deletedAt: null,
    });
    if (!checkExist) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS,"Người dùng không tồn tại");
    }
    if (!data.phoneNumber) {
      data.phoneNumber = '';
    } else {
      const isDuplicatedTel = await this.userModel.exists({
        _id: { $ne: data._id },
        phoneNumber: data.phoneNumber,
        deletedAt: null,
      });
      if (isDuplicatedTel) {
        UserErrors(StatusCodes.TEL_ALREADY_EXISTS ,"số điện thoại đã tồn tại");
      }
    }


    checkExist.set(omit(data, '_id'));
    return await checkExist.save();
  }

  async listStaff(data: ListStaffInput): Promise<ListStaffResponse> {
    validateListStaff(data);
    const skip = (Number(data.page) - 1) * Number(data.limit);
    const conditions: any = {
      type: Type.STAFF,
      deletedAt: null,
    };
    if (data.search) {
      conditions.$or = [
        { username: { $regex: data.search, $options: 'i' } },
        { email: { $regex: data.search, $options: 'i' } },
        { name: { $regex: data.search, $options: 'i' } },
        { phoneNumber: { $regex: data.search, $options: 'i' } },
      ];
    }
    if (data.status) {
      conditions.status = data.status === 'true';
    }

    const users = await this.userModel
      .find(conditions)
      .skip(skip)
      .limit(Number(data.limit))
      .sort({ _id: -1 })
      .exec();
    const userAll = await this.userModel.countDocuments(conditions).exec();
    return {
      data: users,
      total: userAll,
    };
  }

  async deleteStaff(data: DetailStaffInput): Promise<boolean> {
    validateDeleteStaff(data);
    const checkExist = await this.userModel.findOne({
      _id: data.id,
      deletedAt: null,
    });

    if (!checkExist) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS,"User không tồn tại");
    }
    const mainQuery: any = {
      deletedAt: Date.now(),
    };
    await this.exerciseRegineService.deleteExerciseRegineByUser(data.id)

    await this.userModel.findByIdAndUpdate(data.id, mainQuery).exec();
    return true;
  }
}
