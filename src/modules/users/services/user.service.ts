import mongoose, { Model, Mongoose, Types } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  User,
  DeliveryAddress,
  UserRecord,
} from '../interfaces/user.interface';
import {
  UserInput,
  UpdateUserInput,
  RegisterUserInput,
  CheckPhoneInput,
  DetailUserInput,
  UpdateUserAppInput,
  Type,
  ListUserInput,
  ListUserResponse,
  UpdateInfoUserInput,
  ChangePassword,
  UpdateDeliveryAddressInput,
  DeliveryAddressResponse,
  ListDeliveryAddressResponse,
  BaseResponseUser,
  ListDeliveryAddress,
  EmailVerifiedParam,
} from '../interfaces/user.entity';
import {
  UpdatedResponse,
  StatusCodes,
  BaseInput,
  BaseStatus,
  BaseResponse,
} from '@modules/base.interface';
import { UserErrors } from '@helper/error.helper';
import {
  LoginInput,
  IDTokenPayload,
  RefreshTokenInput,
  ForgotPasswordInput,
  EmailForgotPasswordInput,
} from '../interfaces/auth.entity';
import {
  validateLoginInput,
  userValidations,
  phoneNumberValidations,
  detailUserValidation,
  tokenValidation,
  updateUserValidation,
  uploadAvatarValidation,
  listUserValidation,
  validateUpdateInfoUserInput,
  changePasswordValidation,
  validateDeliveryAddress,
  ForgotValidations,
} from '../utils/validations';
import userUtil, { updatePointRanking } from '../utils/user.util';
import { generateUserToken, decodeToken } from '@utils/jwt';
import { JWTAuthTokenType } from '@utils/jwt.entity';
import { Token } from '../interfaces/token.interface';
import ms from 'ms';
import { omit } from 'lodash';
import { remove } from 'fs-extra';
import { Notification } from '@modules/notifications/interfaces/notification.interface';
import { PayloadData } from '@modules/notifications/interfaces/notification.entity';
import { createNotificationToTag } from '@modules/notifications/utils/onesignal';
import { getContentNotification } from '@utils/helper';
import { GMailSupporting } from '@utils/mail.util';
import { randomString } from '@utils/string';
import { Media } from '@modules/media/interfaces/media.interface';
import { uploadImageWithoutResizing } from '@utils/photo';
import { constants } from 'buffer';
@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    @InjectModel('Token') private readonly tokenModel: Model<Token>,
    @InjectModel('DeliveryAddress')
    private readonly deliveryAddressModel: Model<DeliveryAddress>,
    @InjectModel('Media') private readonly mediaModel: Model<Media>,
  ) {}

  async create(createUserInput: UserInput): Promise<User> {
    const createdData = new this.userModel(createUserInput);
    return await createdData.save();
  }

  async register(
    params: RegisterUserInput,
    host: string,
  ): Promise<IDTokenPayload> {
    userValidations(params);
    if (params.email) {
      const isExistEmail = await this.userModel
        .findOne({
          deletedAt: null,
          email: params.email,
        })
        .lean()
        .exec();
      if (isExistEmail)
        throw UserErrors(StatusCodes.EMAIL_ALREADY_EXISTS, 'Email đã tồn tại');
    }
    await this.checkPhone({
      phoneNumber: params.phoneNumber,
    });
    const verifyToken = randomString(3, 'number');
    params.verifyToken = verifyToken;
    params.isVerified = false;
    const userModel = new this.userModel(params);
    const user = await userModel.save();
    if (user) {
      // TODO: Define email template
      const mailOptions = {
        from: 'OnlineCare',
        to: params.email,
        subject: 'Xác thực tài khoản',
        html: `<h2>Bạn đã đăng ký thành công vào hệ thống OnlineCare</h2>
              <p>
                Vui lòng click vào link sau để kích hoạt tài khoản 
                <a href="${host}/user/verify?token=${verifyToken}" target="_blank">${host}/user/verify?token=${verifyToken}</a>
              </p>
              `,
      };
      await GMailSupporting(mailOptions);
    }

    return {
      token: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.ID_TOKEN,
      }),
      refreshToken: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.REFRESH_TOKEN,
      }),
      user,
      expiresAt: new Date(
        new Date().getTime() + ms(process.env.JWT_ID_TOKEN_EXPIRES),
      ).toISOString(),
    };
  }
  async emailVerify(params: EmailVerifiedParam): Promise<BaseResponse> {
    const { token } = params;
    const user = await this.userModel.findOne({
      verifyToken: token,
      deletedAt: null,
    });

    if (!user) {
      throw UserErrors(StatusCodes.TOKEN_NOT_EXIST);
    }
    await user.update({
      verifyToken: null,
      emailVerifiedAt: new Date(),
      isVerified: true,
      status: BaseStatus.ACTIVE,
    });
    return {
      status: StatusCodes.SUCCESS,
    };
  }
  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async findByUsername(username: string): Promise<User> {
    return await this.userModel.findOne({ username }).exec();
  }

  async checkPhone(
    params: CheckPhoneInput,
    type: Type = Type.CUSTOMERS,
  ): Promise<string> {
    phoneNumberValidations(params);
    const isDuplicated = await this.userModel.exists({
      phoneNumber: params.phoneNumber,
      type: type,
      deletedAt: null,
    });

    if (isDuplicated) {
      UserErrors(StatusCodes.TEL_ALREADY_EXISTS);
    }

    return params.phoneNumber;
  }

  async findById(_id: string): Promise<any> {
    return await this.userModel
      .findOne({ _id })
      .lean()
      .exec();
  }

  async findUserById(_id: string): Promise<any> {
    return await this.userModel
      .findOne({
        deletedAt: null,
        _id,
      })
      .lean()
      .exec();
  }

  async findByIds(ids: string[]): Promise<User[]> {
    return await this.userModel.find({ _id: { $in: ids } }).exec();
  }

  async update(
    _id: string,
    updateUserInput: UpdateUserInput,
  ): Promise<UpdatedResponse> {
    const $set: any = updateUserInput;
    return await this.userModel.updateOne({ _id }, { $set }, { new: true });
  }

  async findUserChat(userId: string): Promise<User[]> {
    return await this.userModel
      .find({
        _id: { $nin: [userId] },
      })
      .sort({ lastOnlineTime: -1 })
      .limit(1)
      .exec();
  }
  async sendemailforgotpassword(
    param: EmailForgotPasswordInput,
  ): Promise<User> {
    const user = await this.userModel.findOne({
      email: param.email,
      deletedAt: null,
      status: BaseStatus.ACTIVE,
    });
    if (!user) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS, 'User không tồn tại');
    }
    if (user) {
      const verifyToken = randomString(3, 'number');
      user.verifyToken = verifyToken;
      await user.save();
      console.log(verifyToken);
      const mailOptions = {
        from: process.env.APP_NAME,
        to: user.email,
        subject: 'Mã thay đổi mật khẩu',
        html: `
    <!DOCTYPE html>
    <html>
    <head>
    
      <title>Rating Reminder</title>
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
      <meta content="width=device-width" name="viewport">
      <style type="text/css">
        @font-face {
          font-family: &#x27;
          Postmates Std&#x27;
          ;
          font-weight: 600;
          font-style: normal;
          src: local(&#x27;
          Postmates Std Bold&#x27;
          ),
          url(https://s3-us-west-1.amazonaws.com/buyer-static.postmates.com/assets/email/postmates-std-bold.woff) format(&#x27;
          woff&#x27;
          );
        }
    
        @font-face {
          font-family: &#x27;
          Postmates Std&#x27;
          ;
          font-weight: 500;
          font-style: normal;
          src: local(&#x27;
          Postmates Std Medium&#x27;
          ),
          url(https://s3-us-west-1.amazonaws.com/buyer-static.postmates.com/assets/email/postmates-std-medium.woff) format(&#x27;
          woff&#x27;
          );
        }
    
        @font-face {
          font-family: &#x27;
          Postmates Std&#x27;
          ;
          font-weight: 400;
          font-style: normal;
          src: local(&#x27;
          Postmates Std Regular&#x27;
          ),
          url(https://s3-us-west-1.amazonaws.com/buyer-static.postmates.com/assets/email/postmates-std-regular.woff) format(&#x27;
          woff&#x27;
          );
        }
      </style>
      <style media="screen and (max-width: 680px)">
        @media screen and (max-width: 680px) {
          .page-center {
            padding-left: 0 !important;
            padding-right: 0 !important;
          }
    
          .footer-center {
            padding-left: 20px !important;
            padding-right: 20px !important;
          }
        }
      </style>
    
    
      </style>
    <style>
     input[type=text] { 
   
     }
    </style>
    </head>
    
    <body style="background-color: #f4f4f5;">
      <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; background-color: #f4f4f5; text-align: center;">
        <tbody>
          <tr>
            <td style="text-align: center;">
              <table align="center" cellpadding="0" cellspacing="0" id="body" style="background-color: #fff; width: 100%;  height: 100%;">
                <tbody>
                  <tr>
                    <td>
                      <table align="center" cellpadding="0" cellspacing="0" class="page-center" style="text-align: left; padding-bottom: 88px; width: 100%; padding-left: 120px; padding-right: 120px;">
                        <tbody>
                          <tr>
                            <td style="padding-top: 24px;">
                              <img src="https://d1pgqke3goo8l6.cloudfront.net/wRMe5oiRRqYamUFBvXEw_logo.png" style="width: 56px;">
    </td>
    </tr>
    <tr>
    <td colspan="2" style="padding-top: 72px; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #000000; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 48px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: -2.6px; line-height: 52px; mso-line-height-rule: exactly; text-decoration: none;">Xác thực tài khoản</td>
    </tr>
    <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 16px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
    "Xin chào ${user.name}, 
        Chúng tôi đã nhận được thông báo quên mật khẩu của bạn .
        Nhập mã xác nhận sau đây để thay đổi mật khẩu: ${user.verifyToken}"
  </td>
    <tr>
    <td style="padding-top: 48px; padding-bottom: 48px;">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
    <tbody><tr>
    <td style="width: 100%; height: 1px; max-height: 1px; background-color: #d9dbe0; opacity: 0.81"></td>
    </tr>
    </tbody>
      <div class="centered">
    <form  id="search-theme-form" method='get' target="_blank" >


    
                                      
    </input>
    </form>
      
    </div>
     </div>
       </body>
    
    </table>
    </td>
    </tr>
    <tr>
    <td style="-ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 16px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
                                          You're receiving this e-mail because you requested a password reset for your Postmates account.
                                        </td>
    </tr>
    <tr>
    <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 16px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
                                          Please tap the button below to choose a new password.
                                        </td>
    </tr>
    <tr>
    <td>
    
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    <table align="center" cellpadding="0" cellspacing="0" id="footer" style="background-color: black; width: 100%; height: 100%;">
    <tbody><tr>
    <td>
    <table align="center" cellpadding="0" cellspacing="0" class="footer-center" style="text-align: left; width: 100%; padding-left: 120px; padding-right: 120px;">
    <tbody><tr>
    <td colspan="2" style="padding-top: 72px; padding-bottom: 24px; width: 100%;">
    <img src="https://d1pgqke3goo8l6.cloudfront.net/DFcmHWqyT2CXk2cfz1QB_wordmark.png" style="width: 124px; height: 20px">
    </td>
    </tr>
    <tr>
    <td colspan="2" style="padding-top: 24px; padding-bottom: 48px;">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
    <tbody><tr>
    <td style="width: 100%; height: 1px; max-height: 1px; background-color: #EAECF2; opacity: 0.19"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr>
    <td style="-ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095A2; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 15px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: 0; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
                                              If you have any questions or concerns, we're here to help. Contact us via our <a data-click-track-id="1053" href="https://support.postmates.com/buyer" style="font-weight: 500; color: #ffffff" target="_blank">Help Center</a>.
                                            </td>
    </tr>
    <tr>
    <td style="height: 72px;"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </body>  
    </html>
  `,
      };
      await GMailSupporting(mailOptions);
    }
    return user;
  }
  async loginApp(data: LoginInput): Promise<IDTokenPayload> {
    validateLoginInput(data);
    const { email, password } = data;
    const user = await this.userModel.findOne({
      email: email,
      deletedAt: null,
      status: BaseStatus.ACTIVE,
    });
    if (!user) {
      UserErrors(
        StatusCodes.USER_DO_NOT_EXISTS,
        'User không tồn tại hoặc chưa được kích hoạt',
      );
    }

    if (user.status === BaseStatus.INACTIVE) {
      UserErrors(StatusCodes.USER_HAS_BEEN_BLOCKED);
    }

    const checkPassword = await userUtil.compare(password, user.password);
    if (!checkPassword) {
      UserErrors(StatusCodes.PASSWORD_INCORRECT);
    }

    return {
      token: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.ID_TOKEN,
      }),
      refreshToken: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.REFRESH_TOKEN,
      }),
      user,
      expiresAt: new Date(
        new Date().getTime() + ms(process.env.JWT_ID_TOKEN_EXPIRES),
      ).toISOString(),
    };
  }

  async refreshToken(data: RefreshTokenInput): Promise<IDTokenPayload> {
    try {
      tokenValidation(data);
      const token = await this.tokenModel.exists({ token: data.refreshToken });
      if (token) {
        UserErrors(StatusCodes.TOKEN_ALREADY_EXISTS);
      }
      const decodedToken = await decodeToken(data.refreshToken);
      const user = await this.userModel
        .findOne({
          _id: decodedToken.userId,
          deletedAt: null,
        })
        .exec();
      if (!user) {
        UserErrors(StatusCodes.USER_DO_NOT_EXISTS);
      }
      return {
        token: generateUserToken({
          userId: decodedToken.userId,
          type: JWTAuthTokenType.ID_TOKEN,
        }),
        refreshToken: generateUserToken({
          userId: decodedToken.userId,
          type: JWTAuthTokenType.REFRESH_TOKEN,
        }),
        user,
        expiresAt: new Date(
          new Date().getTime() + ms(process.env.JWT_ID_TOKEN_EXPIRES),
        ).toISOString(),
      };
    } catch (error) {
      UserErrors(StatusCodes.TOKEN_NOT_RIGHT);
    }
  }

  async logout(header: any): Promise<Token> {
    if (header.authorization) {
      return await this.tokenModel.create({ token: header.authorization });
    }
  }

  async detailUser(params: DetailUserInput): Promise<UserRecord> {
    detailUserValidation(params);
    const user = await this.userModel
      .findOne({ _id: params.id, deletedAt: null })
      .select({ password: 0 })
      .lean()
      .exec();

    // const usesr = await this.userModel.aggregate([
    //   { $match: { _id: mongoose.Types.ObjectId(params.id) } },
    //   {
    //     $lookup: {
    //       from: 'media',
    //       localField: 'avatar',
    //       foreignField: '_id',
    //       as: 'avatarSource',
    //     }
    //   },
    //   {
    //     $project: {
    //       _id: 1,
    //       avatar: { $arrayElemAt: ['$avatarSource', 0] },
    //       name: 1,
    //       online: 1
    //     }
    //   }
    // ]);

    if (!user) {
      return UserErrors(StatusCodes.USER_DO_NOT_EXISTS);
    }
    return user;
  }

  async updateUser(data: UpdateUserAppInput): Promise<User> {
    updateUserValidation(data);
    const checkUser = await this.userModel.findOne({
      _id: data._id,
      deletedAt: null,
    });
    if (!checkUser) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS);
    }
    checkUser.set(omit(data, '_id'));
    return await checkUser.save();
  }

  async uploadAvatar(user: User, file): Promise<User> {
    const checkUser = await this.userModel.findOne({
      _id: user._id,
      deletedAt: null,
    });
    if (!checkUser) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS);
    }
    const path = `uploads/avatar`;
    const _id = mongoose.Types.ObjectId().toString();

    const photo = await uploadImageWithoutResizing(file, path, _id);
    const media = new this.mediaModel(photo);

    await media.save(), checkUser.set({ avatar: media._id });
    return await checkUser.save();
  }

  async listUser(params: ListUserInput): Promise<ListUserResponse> {
    listUserValidation(params);
    const { search, limit, page, rank, status } = params;

    let conditions: object = {
       $or : [
        {type:Type.CUSTOMERS},
        {type:Type.STAFF},
    ],
      deletedAt: null,
    };
    if (search) {
      conditions = {
        ...conditions,
        $or: [
          { name: new RegExp(search, 'i') },
          { phoneNumber: new RegExp(search, 'i') },
        ],
      };
    }
    if (status) {
      conditions = {
        ...conditions,
        status,
      };
    }
    if (rank) {
      conditions = {
        ...conditions,
        pointRankingId: rank,
      };
    }
    const lim = limit ? parseInt(limit) : 10;
    const pag = page ? parseInt(page) : 1;

    const [data, total] = await Promise.all([
      this.userModel
        .find(conditions)
        .skip(lim * (pag - 1))
        .sort({ createdAt: -1, name: 1 })
        .limit(lim)
        .lean()
        .exec(),
      this.userModel.countDocuments(conditions),
    ]);
    return { data, total };
  }

  async updateInfoUser(params: UpdateInfoUserInput): Promise<User> {
    validateUpdateInfoUserInput(params);
    const infoUser = await this.userModel.findOne({
      _id: params._id,
      deletedAt: null,
    });
    if (!infoUser) {
      UserErrors(StatusCodes.RECORD_NOT_FOUND);
    }
    infoUser.set(omit(params, '_id'));
    await infoUser.save();

    return infoUser;
  }

  async forgotPassword(param: ForgotPasswordInput) {
    ForgotValidations(param);
    const { email, password, verifyToken } = param;
    const user = await this.userModel.findOne({
      email: email,
      deletedAt: null,
      verifyToken: verifyToken,
    });
    7;
    if (!user) {
      UserErrors(
        StatusCodes.USER_DO_NOT_EXISTS,
        'User không tồn tại hoặc mã xác nhận không đúng',
      );
    }

    user.set({ password: password, verifyToken: null });
    return await user.save();
  }

  /**
   * Đổi mật khẩu
   *
   * @param {ChangePassword} data
   * @param {string} _id
   * @param {string} oldPassword
   * @returns {Promise<UpdatedResponse>}
   * @memberof UserService
   */
  async changePassword(
    data: ChangePassword,
    _id: string,
    oldPassword: string,
  ): Promise<UpdatedResponse> {
    await changePasswordValidation(data, oldPassword);
    return await this.userModel.updateOne(
      { _id },
      { $set: { password: data.newPassword } },
    );
  }

  /**
   *
   *
   * @param {UpdateDeliveryAddressInput} body
   * @param {User} user
   * @returns {Promise<DeliveryAddressResponse>}
   * @memberof UserService
   */
  async addDeliveryAddress(
    body: UpdateDeliveryAddressInput,
    user: User,
  ): Promise<DeliveryAddressResponse> {
    if (body.isDefault && body.isDefault == true) {
      await this.deliveryAddressModel.findOneAndUpdate(
        { createdBy: user._id, isDefault: true },
        { $set: { isDefault: false } },
      );
    }
    const address = {
      ...body,
      createdBy: user._id.toString(),
      createdAt: new Date(),
    };

    const deliveryAddress = await this.deliveryAddressModel.create(address);
    return {
      status: StatusCodes.SUCCESS,
      data: deliveryAddress,
    };
  }

  async updateDeliveryAddress(
    params: BaseInput,
    body: UpdateDeliveryAddressInput,
    user: User,
  ): Promise<DeliveryAddressResponse> {
    const deliveryAddress = await this.deliveryAddressModel.findOne({
      _id: params.id,
      createdBy: user._id,
      deletedAt: null,
    });

    if (!deliveryAddress) {
      UserErrors(StatusCodes.RECORD_NOT_FOUND);
    }
    if (body.isDefault == true && deliveryAddress.isDefault != true) {
      await this.deliveryAddressModel.findOneAndUpdate(
        { createdBy: user._id, isDefault: true },
        { $set: { isDefault: false } },
      );
    }

    return {
      status: StatusCodes.SUCCESS,
      data: await this.deliveryAddressModel.findByIdAndUpdate(
        params.id,
        { $set: body },
        { new: true },
      ),
    };
  }

  async listDeliveryAddress(
    user: User,
    query: ListDeliveryAddress,
  ): Promise<ListDeliveryAddressResponse> {
    const { isDefault, limit, page } = query;

    const conditions: any = {
      createdBy: user._id,
      deletedAt: null,
    };
    if (isDefault) {
      conditions.isDefault = isDefault;
    }

    const lim = limit ? +limit : 10;
    const pag = page ? +page : 1;

    const deliveryAddress = (await this.deliveryAddressModel
      .find(conditions)
      .sort({ createdAt: -1 })
      .populate('city', 'name')
      .populate('district', 'name')
      .populate('ward', 'name')
      .skip(lim * (pag - 1))
      .limit(lim)
      .lean()
      .exec()) as DeliveryAddress[];

    return {
      status: StatusCodes.SUCCESS,
      data: deliveryAddress,
    };
  }

  async detailDeliveryAddress(
    param: BaseInput,
    user: User,
  ): Promise<DeliveryAddressResponse> {
    validateDeliveryAddress(param);
    const data = (await this.deliveryAddressModel
      .findOne({
        deletedAt: null,
        createdBy: user._id,
        _id: param.id,
      })
      .populate('city', 'name')
      .populate('district', 'name')
      .populate('ward', 'name')
      .lean()
      .exec()) as DeliveryAddress;
    if (!data) {
      UserErrors(StatusCodes.USER_DO_NOT_EXISTS);
    }
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }

  async deleteDeliveryAddress(
    params: BaseInput,
    user: User,
  ): Promise<BaseResponseUser> {
    validateDeliveryAddress(params);
    const deliveryAddress = await this.deliveryAddressModel.findOne({
      _id: params.id,
      createdBy: user._id,
      deletedAt: null,
    });

    if (!deliveryAddress) {
      UserErrors(StatusCodes.DELIVERY_ADDRESS_DO_NOT_EXISTS);
    }

    await this.deliveryAddressModel.findByIdAndUpdate(params.id, {
      $set: { deletedAt: new Date(), updatedAt: new Date() },
    });

    return {
      status: StatusCodes.SUCCESS,
    };
  }

  async updateConversationId(id, conversationId): Promise<User> {
    const dataUpdate = await this.userModel.findOneAndUpdate(
      {
        deletedAt: null,
        _id: id,
      },
      {
        conversationId: conversationId,
      },
    );

    return dataUpdate;
  }
}
