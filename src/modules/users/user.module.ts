import { Module, Global } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema, DeliveryAddressSchema } from './schemas/user.schema';
import { UserService } from './services/user.service';
import { UsersController } from './controllers/user.controller';
import { TokenSchema } from './schemas/token.schema';
import { MulterModule } from '@nestjs/platform-express';
import { StaffService } from './services/staff.service';
import { StaffController } from './controllers/staff.controller';
import { CMSPermisionGateway } from '@modules/permissions/gateways/cmsPermission.gateway';
import { PermissionModule } from '@modules/permissions/permission.module';
import { RequireAuth } from './utils/user.guard';
import { MediaService } from '@modules/media/services/media.service';
import { MediaSchema } from '@modules/media/schemas/media.schema';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'User', schema: UserSchema },
      { name: 'Media', schema: MediaSchema },
      { name: 'Token', schema: TokenSchema },
      { name: 'DeliveryAddress', schema: DeliveryAddressSchema },
  
    ]),
    PermissionModule,

  ],
  providers: [UserService, StaffService,MediaService , CMSPermisionGateway, RequireAuth],
  exports: [UserService, StaffService],
  controllers: [UsersController, StaffController],
})
export class UserModule {}
