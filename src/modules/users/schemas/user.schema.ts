import { DeliveryAddress } from './../interfaces/user.interface';
import mongoose from 'mongoose';
import { clone } from 'lodash';
import { GenderType, OnlineStatus, Type } from '../interfaces/user.entity';
import { User } from '../interfaces/user.interface';
import Hash from '../utils/user.util';
import { BaseStatus } from '@modules/base.interface';
import { string } from 'yup';

const ObjectId = mongoose.Types.ObjectId;

export const UserSchema = new mongoose.Schema({
  name: String,
  phoneNumber: String,
  username: String,
  email: String,
  password: String,
  age: Number,
  verifyToken:{
    type:String,
    default:null
  },
  emailVerifiedAt: {
    type: Date,
    default: null,
  },
  isVerified: {
    type:Boolean,
    default:false
  },
  gender: {
    type: GenderType,
    default: GenderType.MALE,
  },
  status: { type: String, default: BaseStatus.INACTIVE },
  online: {
    type: OnlineStatus,
    default: OnlineStatus.OFFLINE,
  },
  type: {
    type: Type,
  },
  avatar: {
    type: mongoose.Types.ObjectId,
    default: null,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  location: {
    type: String,
    default: null,
  },
  deviceId: String,
  dateOfBirth: {
    type: Date,
    default: null,
  },
  lastOnlineTime: {
    type: Date,
    default: new Date(),
  },
  unreadNotification: Number,
  conversationId: {
    type: mongoose.Types.ObjectId,
    default: null,
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: Date,
  deletedAt: {
    type: Date,
    default: null,
  },
});

/**
 * Password hash middleware.
 */
UserSchema.pre<User>('updateOne', async function save(next) {
  try {
    const user = clone(this);
    if (user._update.$set.password) {
      user._update.$set.password = await Hash.hash(
        user._update.$set.password,
      ).then((pass: string) => pass); // hash password
    }

    return next();
  } catch (error) {
    return next(error);
  }
});

UserSchema.pre('save', async function save(next) {
  try {
    const user = clone(this);
    if (!user.isModified('password')) return next();

    user.password = await Hash.hash(user.password); // hash password

    return next();
  } catch (error) {
    return next(error);
  }
});

export const UserModel =
  mongoose.models['Users'] ||
  mongoose.model<User>('Users', UserSchema, 'Users');

export const DeliveryAddressSchema = new mongoose.Schema(
  {
    receiverName: String,
    receiverPhone: String,
    cityId: ObjectId,
    districtId: ObjectId,
    wardId: ObjectId,
    lat: Number,
    lon: Number,
    createdBy: ObjectId,
    detail: String,
    isDefault: Boolean,
    createdAt: {
      type: Date,
      default: new Date(),
    },
    updatedAt: {
      type: Date,
      default: new Date(),
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { strict: false, timestamps: true },
);

DeliveryAddressSchema.virtual('city', {
  ref: 'City',
  localField: 'cityId',
  foreignField: '_id',
  justOne: true,
  match: { deletedAt: null },
});

DeliveryAddressSchema.virtual('district', {
  ref: 'District',
  localField: 'districtId',
  foreignField: '_id',
  justOne: true,
  match: { deletedAt: null },
});

DeliveryAddressSchema.virtual('ward', {
  ref: 'Ward',
  localField: 'wardId',
  foreignField: '_id',
  justOne: true,
  match: { deletedAt: null },
});

export const DeliveryAddressModel =
  mongoose.models['DeliveryAddress'] ||
  mongoose.model<DeliveryAddress>(
    'DeliveryAddress',
    DeliveryAddressSchema,
    'DeliveryAddress',
  );
