import mongoose from 'mongoose';
export const TokenSchema = new mongoose.Schema({
    token: String,
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: Date
})