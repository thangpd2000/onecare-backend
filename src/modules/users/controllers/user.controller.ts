import { addDeliveryAddressValidation } from './../utils/validations';
import {
  Controller,
  Get,
  Req,
  Post,
  Body,
  UseGuards,
  Put,
  Param,
  Headers,
  UseInterceptors,
  UploadedFile,
  Query,
  Delete,
  Render,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
import { Request } from 'express';
import {
  UserInput,
  // UpdateUserInput,
  // UpdateUserResponse,
  OnlineStatus,
  CheckUserNameInput,
  CheckUserNameResponse,
  FetchCountryLanguageResponse,
  MatchingUserResponse,
  RegisterUserInput,
  // RegisterUserResponse,
  CheckPhoneInput,
  CheckPhoneResponse,
  // LogOutInput,
  LogOutResponse,
  DetailUserInput,
  DetailUserResponse,
  UpdateUserAppInput,
  // UpdateUserAppInputId,
  UpdateUserAppResponse,
  UploadAvatarAppInput,
  ListUserResponse,
  ListUserInput,
  UpdateInfoUserInput,
  UpdateInfoUserInputId,
  UpdateInfoUserResponse,
  ChangePassword,
  UpdateDeliveryAddressInput,
  DeliveryAddressResponse,
  ListDeliveryAddressResponse,
  BaseResponseUser,
  Type,
  RankUserResponse,
  ListDeliveryAddress,
  Roles,
  EmailVerifiedParam,
  EmailVerifiedResponse,
} from '../interfaces/user.entity';
import {
  LoginInput,
  AuthResponse,
  RefreshTokenInput,
  ForgotPasswordInput,
  EmailForgotPasswordInput,
} from '../interfaces/auth.entity';
import { UserService } from '../services/user.service';
import { generateUserToken } from '@utils/jwt';
import {
  StatusCodes,
  BooleanResponse,
  MyRequest,
  BaseResponse,
  BaseInput,
} from '@modules/base.interface';
// import UserUtil from '../utils/user.util';
import { User } from '../interfaces/user.interface';
import { RequireAuth } from '../utils/user.guard';
import { ListCountry } from '@utils/countries.util';
import { ListLanguages } from '@utils/languages.util';
import { JWTAuthTokenType } from '@utils/jwt.entity';
import {
  editFileName,
  imageFileFilter,
  destination,
  destinationAdmin,
} from '../utils/user.upload';
import { AnyFilesInterceptor, FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { UserErrors } from '@helper/error.helper';
import Any = jasmine.Any;
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { uploadImage } from '@utils/photo';
import { MediaService } from '@modules/media/services/media.service';
import { MAX_UPLOADED_FILE } from '@helper/constant.helper';
import { responseSuccess } from '@helper/response.helper';
@Controller('user')
export class UsersController {
  constructor(
    private readonly userService: UserService,
    private readonly mediaService: MediaService
    ) {}
  // tạo tài khoản
  @Post()
  async createUser(
    @Body() body: UserInput,
    ): Promise<AuthResponse> {
    const user = await this.userService.create(body );
    return {
      status: StatusCodes.SUCCESS,
      token: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.ID_TOKEN,
      }),
      refreshToken: generateUserToken({
        userId: user._id,
        type: JWTAuthTokenType.REFRESH_TOKEN,
      }),
      user,
    };
  }
  @Post('/register')
  async registerUser(@Body() body: RegisterUserInput,@Req() req: Request): Promise<AuthResponse> {
    const payload = await this.userService.register(body,req.headers.host);

    return {
      status: StatusCodes.SUCCESS,
      ...payload,
    };
  }

   /**
   * Xác thực qua email
   *
   * @param {EmailVerifiedParam} params
   * @returns {Promise<EmailVerifiedResponse>}
   * @memberof UsersController
   */
  @Get('verify')

  async emailVerify(
    @Query() params: EmailVerifiedParam,
    @Res() res: Response
  ) {
    const emailsuccess=await this.userService.emailVerify(params);
    if(emailsuccess.status==StatusCodes.SUCCESS){
     return res.send(`<html>
     <head>
       <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
     </head>
       <style>
         body {
           text-align: center;
           padding: 40px 0;
           background: #EBF0F5;
         }
           h1 {
             color: #88B04B;
             font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
             font-weight: 900;
             font-size: 40px;
             margin-bottom: 10px;
           }
           p {
             color: #404F5E;
             font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
             font-size:20px;
             margin: 0;
           }
         i {
           color: #9ABC66;
           font-size: 100px;
           line-height: 200px;
           margin-left:-15px;
         }
         .card {
           background: white;
           padding: 60px;
           border-radius: 4px;
           box-shadow: 0 2px 3px #C8D0D8;
           display: inline-block;
           margin: 0 auto;
         }
       </style>
       <body>
         <div class="card">
         <div style="border-radius:200px; height:200px; width:200px; background: #F8FAF5; margin:0 auto;">
           <i class="checkmark">✓</i>
         </div>
           <h1>${emailsuccess.status}</h1> 
           <p>We received your purchase request;<br/> we'll be in touch shortly!</p>
         </div>
       </body>
   </html>`)
    }
   
  }

  // check username
  @Post('check-username')
  async checkUsername(
    @Body() body: CheckUserNameInput,
  ): Promise<CheckUserNameResponse> {
    const user = await this.userService.findByUsername(body.username);
    return { user, status: StatusCodes.SUCCESS };
  }

  //check phone number
  @Post('/check-phone')
  async checkPhone(@Body() body: CheckPhoneInput): Promise<CheckPhoneResponse> {
    const phoneNumber = await this.userService.checkPhone(body);
    return {
      phoneNumber: phoneNumber,
      status: StatusCodes.SUCCESS,
    };
  }

  // đăng nhập
  @Post('login')
  async login(@Body() body: LoginInput): Promise<AuthResponse> {
    const payload = await this.userService.loginApp(body);

    return {
      status: StatusCodes.SUCCESS,
      ...payload,
    };
  }
  @Post('refresh-token')
  async refreshToken(@Body() body: RefreshTokenInput): Promise<AuthResponse> {
    const token = await this.userService.refreshToken(body);
    return {
      status: StatusCodes.SUCCESS,
      ...token,
    };
  }

  // đăng xuất
  @Post('logout')
  async logout(@Headers() header: any): Promise<LogOutResponse> {
    const token = await this.userService.logout(header);
    return {
      status: StatusCodes.SUCCESS,
      token: token,
    };
  }
  @Post('emailforgotpassword')
  async sendemailforgotpassword(@Body() param: EmailForgotPasswordInput): Promise<any> {
    const token = await this.userService.sendemailforgotpassword(param);
    return {
      status: StatusCodes.SUCCESS,

    };
  }

  // cập nhật thông tin user
  @Put()
  @Roles('CUSTOMERS')
  async updateUser(
    @Body() body: UpdateUserAppInput,
    @Req() req: any,
  ): Promise<UpdateUserAppResponse> {
    body._id = req.user._id;
    const user = await this.userService.updateUser(body);
    return {
      status: StatusCodes.SUCCESS,
      user: user,
    };
  }

  //upload avatar
  @Post('/avatar')
  @Roles('CUSTOMERS')
  @UseInterceptors(FileInterceptor('file', {
    limits: {
      fileSize: 5242880
    }
  }))
  async uploadAvatar(
    @UploadedFile() file,
    @Req() req: Request & { user: User },
  ): Promise<UpdateUserAppResponse> {
    if (file && file.length > MAX_UPLOADED_FILE) {
      UserErrors(StatusCodes.CAN_NOT_GREATER_THAN_MAX_UPLOADED_FILE);
    }
    const userUpload = await this.userService.uploadAvatar(req.user,file);
    return {
      status: StatusCodes.SUCCESS,
      user: userUpload,
    };
  }

  @Post('heart-beat')
  @UseGuards(RequireAuth)
  async heartBeat(
    @Req() req: Request & { user: User },
  ): Promise<BooleanResponse> {
    const { _id } = req.user;

    const updated = await this.userService.update(_id, {
      online: OnlineStatus.ONLINE,
    });
    let result = false;
    if (updated && updated.ok) {
      result = true;
    }

    return {
      status: StatusCodes.SUCCESS,
      data: result,
    };
  }

  // lấy danh sách countries/languages
  @Get('list-country-language')
  listCountryAndLanguages(): FetchCountryLanguageResponse {
    return {
      status: StatusCodes.SUCCESS,
      countries: ListCountry,
      languages: ListLanguages,
    };
  }

  // tìm kiếm người chat phù hợp
  @Get('matching-user')
  @UseGuards(RequireAuth)
  async findUserChat(@Req() req: MyRequest): Promise<MatchingUserResponse> {
    const users = await this.userService.findUserChat(req.user._id);
    return {
      status: StatusCodes.SUCCESS,
      data: users,
    };
  }

  @Get('list')
  // @Roles('STAFF-CUSTOMERS-read')
  async listUser(@Query() params: ListUserInput): Promise<ListUserResponse> {
    const { data, total } = await this.userService.listUser(params);
    return { status: StatusCodes.SUCCESS, data, total };
  }

  @Put('/update/:id')
  @Roles('CUSTOMERS')
  async updateInfoUser(
    @Body() params: UpdateInfoUserInput,
    @Param() { id }: UpdateInfoUserInputId,
  ): Promise<UpdateInfoUserResponse> {
    params._id = id;
    const infoUserUpdate = await this.userService.updateInfoUser(params);
    return {
      status: StatusCodes.SUCCESS,
      data: infoUserUpdate,
    };
  }
  //Thông tin cá nhân
  @Get('detail/:id')
  @Roles('CUSTOMERS','STAFF','ADMIN')
  async detailUser(
    @Param() params: DetailUserInput,
  ): Promise<DetailUserResponse> {
    const user = await this.userService.detailUser(params);
    return {
      status: StatusCodes.SUCCESS,
      user: user,
    };
  }
  /**
   * Đổi mật khẩu
   *
   * @param {ChangePassword} body
   * @param {(Request & { user: User })} req
   * @returns {Promise<BooleanResponse>}
   * @memberof UsersController
   */
  @Post('/change-password')
  @Roles('CUSTOMERS')
  async changePassword(
    @Body() body: ChangePassword,
    @Req() req: Request & { user: User },
  ): Promise<BooleanResponse> {
    const { _id, password } = req.user;
    const updated = await this.userService.changePassword(body, _id, password);
    let result = false;
    if (updated && updated.ok) {
      result = true;
    }
    return {
      status: StatusCodes.SUCCESS,
      data: result,
    };
  }
  //quên mật khẩu
  @Put('forgot-password')
  async forgotPassword(@Body() body: ForgotPasswordInput): Promise<BaseResponse> {
    const payload = await this.userService.forgotPassword(body);

    return {
      status: StatusCodes.SUCCESS,
    };
  }

  // //admin upload avatar
  // @Post('/admin-upload/avatar/:id')
  // @UseGuards(RequireAuth)
  // @UseInterceptors(
  //   FileInterceptor('avatar', {
  //     storage: diskStorage({
  //       destination: destinationAdmin,
  //       filename: editFileName,
  //     }),
  //     fileFilter: imageFileFilter,
  //   }),
  // )
  // async AdminUploadAvatar(
  //   @Body() body: UploadAvatarAppInput,
  //   @Req() req: any,
  //   @UploadedFile() file,
  // ): Promise<UpdateUserAppResponse> {
  //   if (req.user.type === Type.CUSTOMERS) {
  //     return UserErrors(StatusCodes.INVALID_TOKEN);
  //   }
  //   if (file) {
  //     body.avatar = `/assets/uploads/avatars/${req.body._id}/${file.filename}`;
  //   }
  //   body._id = req.body._id;
  //   const user = await this.userService.uploadAvatar(body);
  //   return {
  //     status: StatusCodes.SUCCESS,
  //     user: user,
  //   };
  // }

  /**
   *
   *
   * @param {UpdateDeliveryAddressInput} body
   * @param {(Request & { user: User })} req
   * @returns {Promise<DeliveryAddressResponse>}
   * @memberof UsersController
   */
  @Post('addDeliveryAddress')
  @Roles('CUSTOMERS')
  async addDeliveryAddress(
    @Body() body: UpdateDeliveryAddressInput,
    @Req() req: Request & { user: User },
  ): Promise<DeliveryAddressResponse> {
    await addDeliveryAddressValidation(body);
    return this.userService.addDeliveryAddress(body, req.user);
  }

  @Get('delivery-address')
  @Roles('CUSTOMERS')
  async listDeliveryAddress(
    @Query() query: ListDeliveryAddress,
    @Req() req: Request & { user: User },
  ): Promise<ListDeliveryAddressResponse> {
    return this.userService.listDeliveryAddress(req.user, query);
  }
  @Get('detail-address/:id')
  @Roles('CUSTOMERS')
  async detailDeliveryAddress(
    @Param() param: BaseInput,
    @Req() req: Request & { user: User },
  ): Promise<DeliveryAddressResponse> {
    return this.userService.detailDeliveryAddress(param, req.user);
  }

  @Put('delivery-address/:id')
  @Roles('CUSTOMERS')
  async updateDeliveryAddress(
    @Param() params: BaseInput,
    @Body() body: UpdateDeliveryAddressInput,
    @Req() req: Request & { user: User },
  ): Promise<DeliveryAddressResponse> {
    await addDeliveryAddressValidation(body);
    return this.userService.updateDeliveryAddress(params, body, req.user);
  }

  @Delete('delivery-address/:id')
  @Roles('CUSTOMERS')
  async deleteDeliveryAddress(
    @Param() params: BaseInput,
    @Req() req: Request & { user: User },
  ): Promise<BaseResponseUser> {
    return this.userService.deleteDeliveryAddress(params, req.user);
  }

  //get ranking detail
}
