import { StatusCodes } from '@modules/base.interface';
import { CMSPermisionGateway } from '@modules/permissions/gateways/cmsPermission.gateway';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  Headers,
  Delete,
} from '@nestjs/common';
import { AuthResponse, LoginCmsInput } from '../interfaces/auth.entity';
import {
  CreateStaffInput,
  DeleteStaffResponse,
  DetailStaffInput,
  ListStaffInput,
  ListStaffResponse,
  StaffResponse,
  UpdateStaffInput,
  UpdateStaffInputId,
} from '../interfaces/staff.entity';
import { LogOutResponse, Roles } from '../interfaces/user.entity';
import { StaffService } from '../services/staff.service';
import { RequireAuth } from '../utils/user.guard';

@Controller('staff')
export class StaffController {
  constructor(
    private readonly staffService: StaffService,
    private cmsPermisionGateway: CMSPermisionGateway,
  ) {}

  //đăng nhập
  @Post('login')
  async login(@Body() body: LoginCmsInput): Promise<AuthResponse> {
    const payload = await this.staffService.loginStaff(body);

    return {
      status: StatusCodes.SUCCESS,
      ...payload,
    };
  }
  //đăng xuất
  @Post('logout')
  // @UseGuards(RequireAuth)
  async logout(@Headers() header: any): Promise<LogOutResponse> {
    const token = await this.staffService.logout(header);
    return {
      status: StatusCodes.SUCCESS,
      token: token,
    };
  }
  @Post()
  async createStaff(@Body() body: CreateStaffInput): Promise<StaffResponse> {
    const user = await this.staffService.createStaff(body);
    return {
      status: StatusCodes.SUCCESS,
      data: user,
    };
  }
  @Put('/:id')
  async updateStaff(
    @Body() body: UpdateStaffInput,
    @Param() { id }: UpdateStaffInputId,
  ): Promise<StaffResponse> {
    body._id = id;
    const user = await this.staffService.updateStaff(
      body,
      this.cmsPermisionGateway,
    );
    return {
      status: StatusCodes.SUCCESS,
      data: user,
    };
  }
  @Get('/detail/:id')
  async detailStaff(@Param() param: DetailStaffInput): Promise<StaffResponse> {
    const user = await this.staffService.detailStaff(param);
    return {
      status: StatusCodes.SUCCESS,
      data: user,
    };
  }

  @Get('/list')
  async listStaff(@Query() query: ListStaffInput): Promise<ListStaffResponse> {
    const users = await this.staffService.listStaff(query);
    return {
      status: StatusCodes.SUCCESS,
      ...users,
    };
  }

  @Delete(':id')
  async DeleteStaff(
    @Param() params: DetailStaffInput,
  ): Promise<DeleteStaffResponse> {
    await this.staffService.deleteStaff(params);
    return {
      status: StatusCodes.SUCCESS,
      data: true,
    };
  }
}
