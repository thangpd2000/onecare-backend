import { MongooseModule } from '@nestjs/mongoose';
import { Global, Module } from '@nestjs/common';
import { WardSchema } from './schemas/ward.schema';
import { WardService } from './services/ward.service';
import { WardController } from './controllers/ward.controller';

@Global()
@Module({
  imports: [MongooseModule.forFeature([{ name: 'Ward', schema: WardSchema }])],
  providers: [WardService],
  exports: [WardService],
  controllers: [WardController],
})
export class WardModule {}
