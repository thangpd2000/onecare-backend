import { Document } from 'mongoose'
export interface District extends Document {
    readonly _id: string;
    readonly name: string;
    readonly createdAt: Date;
    readonly updatedAt: Date;
    readonly deletedAt: Date;
}