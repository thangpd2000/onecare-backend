import { Document } from 'mongoose';
export interface Ward extends Document {
  readonly _id: string;
  readonly name: string;
  readonly cityId: string;
  readonly districtId: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
}
