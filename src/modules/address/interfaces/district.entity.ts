import { StatusCodes } from '@modules/base.interface';
import { District } from './district.interface';
//input
export class CreateDistrictInput {
  name: string;
  cityId: string;
}

export class ListDistrictInput {
  limit: string;
  page: string;
  cityId: string;
}
//response

export class ListDistrictResponse {
  status?: StatusCodes;
  data: District[];
  total: number;
}
