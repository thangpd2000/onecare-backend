import { StatusCodes } from '@modules/base.interface';
import { City } from './city.interface';
//input
export class CreateCityInput {
  name: string;
}

export class ListCityInput {
  limit: string;
  page: string;
}
//response

export class ListCityResponse {
  status?: StatusCodes;
  data: City[];
  total: number;
}
