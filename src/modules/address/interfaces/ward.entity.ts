import { StatusCodes } from '@modules/base.interface';
import { Ward } from './ward.interface';
//input
export class CreateWardInput {
  name: string;
  districtId: string;
}

export class ListWardInput {
  limit: string;
  page: string;
  districtId: string;
}
//response

export class ListWardResponse {
  status?: StatusCodes;
  data: Ward[];
  total: number;
}
