import { MongooseModule } from "@nestjs/mongoose";
import { Global, Module } from "@nestjs/common";
import { CitySchema } from "./schemas/city.schema";
import { CityService } from "./services/city.service";
import { CityController } from "./controllers/city.controller";

@Global()
@Module({
    imports: [MongooseModule.forFeature([{ name: 'City', schema: CitySchema }])],
    providers: [CityService],
    exports: [CityService],
    controllers: [CityController]
})
export class CityModule { }
