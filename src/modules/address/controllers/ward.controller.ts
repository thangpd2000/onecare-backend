import { Controller, Get, Query } from "@nestjs/common";
import { StatusCodes } from "@modules/base.interface";
import { ListWardInput, ListWardResponse } from "../interfaces/ward.entity";
import { WardService } from "../services/ward.service";

@Controller('ward')
export class WardController {
    constructor(
        private readonly wardService: WardService,
    ) { }
    @Get()
    async listWard(@Query() params: ListWardInput): Promise<ListWardResponse> {
        const result = await this.wardService.listWard(params)
        return {
            ...result,
            status: StatusCodes.SUCCESS
        }
    }
}

