import { StatusCodes } from "@modules/base.interface";
import { CityService } from "../services/city.service";
import { Controller, Get, Query } from "@nestjs/common";
import { ListCityInput, ListCityResponse } from "../interfaces/city.entity";

@Controller('city')
export class CityController {
    constructor(
        private readonly cityService: CityService,
    ) { }
    @Get()
    async listCity(@Query() params: ListCityInput): Promise<ListCityResponse> {
        const result = await this.cityService.listCity(params)
        return {
            ...result,
            status: StatusCodes.SUCCESS
        }
    }
}

