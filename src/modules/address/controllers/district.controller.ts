import { StatusCodes } from "@modules/base.interface";
import { Controller, Get, Query } from "@nestjs/common";
import { DistrictService } from "../services/district.service";
import { ListDistrictInput, ListDistrictResponse } from "../interfaces/district.entity";

@Controller('district')
export class DistrictController {
    constructor(
        private readonly districtService: DistrictService,
    ) { }
    @Get()
    async listDistrict(@Query() params: ListDistrictInput): Promise<ListDistrictResponse> {
        const result = await this.districtService.listDistrict(params)
        return {
            ...result,
            status: StatusCodes.SUCCESS
        }
    }
}

