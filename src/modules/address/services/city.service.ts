import { Injectable } from "@nestjs/common";
import { listCityValidation, createCityValidation } from "../utils/city.validation";
import { listCityQuery } from "../utils/city.queries";
import { ListCityInput, ListCityResponse, CreateCityInput } from "../interfaces/city.entity";
import { City } from "../interfaces/city.interface";
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

@Injectable()
export class CityService {
    constructor(@InjectModel('City') private readonly cityModel: Model<City>) { }
    
    async create(params: CreateCityInput): Promise<City> {
        createCityValidation(params)
        const city = await this.cityModel.create(params);
        return city;
    }
    
    async listCity(params: ListCityInput): Promise<ListCityResponse> {
        listCityValidation(params)
        const { mainQuery, mainQueryAll } = listCityQuery(params)
        const [listCity, total] = await Promise.all([
            this.cityModel.aggregate(mainQuery).exec(),
            this.cityModel.aggregate(mainQueryAll).exec()
        ])
        return {
            data: listCity,
            total: total.length
        }
    }
}