import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {
  CreateDistrictInput,
  ListDistrictInput,
  ListDistrictResponse,
} from '../interfaces/district.entity';
import {
  listDistrictValidation,
  createManyDistrictValidation,
  createDistrictValidation,
} from '../utils/district.validation';
import { District } from '../interfaces/district.interface';
import { listDistrictQuery } from '../utils/district.queries';

@Injectable()
export class DistrictService {
  constructor(
    @InjectModel('District') private readonly districtModel: Model<District>,
  ) {}

  async create(params: CreateDistrictInput): Promise<District> {
    createDistrictValidation(params);
    const district = await this.districtModel.create(params);
    return district;
  }

  async createMany(params: CreateDistrictInput[]): Promise<District[]> {
    createManyDistrictValidation(params);
    const districts = await this.districtModel.insertMany(params);
    return districts;
  }

  async listDistrict(params: ListDistrictInput): Promise<ListDistrictResponse> {
    listDistrictValidation(params);
    const { mainQuery, mainQueryAll } = listDistrictQuery(params);
    const [listDistrict, total] = await Promise.all([
      this.districtModel.aggregate(mainQuery).exec(),
      this.districtModel.aggregate(mainQueryAll).exec(),
    ]);
    return {
      data: listDistrict,
      total: total.length,
    };
  }
}
