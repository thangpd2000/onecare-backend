import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {
  CreateWardInput,
  ListWardInput,
  ListWardResponse,
} from '../interfaces/ward.entity';
import {
  listWardValidation,
  createManyWardValidation,
  createWardValidation,
} from '../utils/ward.validation';
import { Ward } from '../interfaces/ward.interface';
import { listWardQuery } from '../utils/ward.queries';

@Injectable()
export class WardService {
  constructor(
    @InjectModel('Ward') private readonly wardModel: Model<Ward>,
  ) {}

  async create(params: CreateWardInput): Promise<Ward> {
    createWardValidation(params);
    const ward = await this.wardModel.create(params);
    return ward;
  }

  async createMany(params: CreateWardInput[]): Promise<Ward[]> {
    createManyWardValidation(params);
    const wards = await this.wardModel.insertMany(params);
    return wards;
  }

  async listWard(params: ListWardInput): Promise<ListWardResponse> {
    listWardValidation(params);
    const { mainQuery, mainQueryAll } = listWardQuery(params);
    const [listWard, total] = await Promise.all([
      this.wardModel.aggregate(mainQuery).exec(),
      this.wardModel.aggregate(mainQueryAll).exec(),
    ]);
    return {
      data: listWard,
      total: total.length,
    };
  }
}
