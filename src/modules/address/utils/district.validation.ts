import * as yup from 'yup';
import {
  ListDistrictInput,
  CreateDistrictInput,
} from '../interfaces/district.entity';
export const createDistrictValidation = (data: CreateDistrictInput) => {
  const basedRules: any = {
    name: yup.string(),
    cityId: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const createManyDistrictValidation = (data: CreateDistrictInput[]) => {
  const basedRules: any = {
    name: yup.string(),
    cityId: yup.string(),
  };
  const element = yup.object().shape(basedRules);
  const schema = yup.array().of(element);
  schema.validateSync(data);
};

export const listDistrictValidation = (data: ListDistrictInput) => {
  const basedRules: any = {
    limit: yup.string(),
    after: yup.string(),
    cityId: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
