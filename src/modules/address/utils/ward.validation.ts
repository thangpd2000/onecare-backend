import * as yup from 'yup';
import {
  ListWardInput,
  CreateWardInput,
} from '../interfaces/ward.entity';
export const createWardValidation = (data: CreateWardInput) => {
  const basedRules: any = {
    name: yup.string(),
    districtId: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const createManyWardValidation = (data: CreateWardInput[]) => {
  const basedRules: any = {
    name: yup.string(),
    districtId: yup.string(),
  };
  const element = yup.object().shape(basedRules);
  const schema = yup.array().of(element);
  schema.validateSync(data);
};

export const listWardValidation = (data: ListWardInput) => {
  const basedRules: any = {
    limit: yup.string(),
    after: yup.string(),
    districtId: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
