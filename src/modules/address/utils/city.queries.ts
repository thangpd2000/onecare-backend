import { ListCityInput } from "../interfaces/city.entity";

export const listCityQuery = (params: ListCityInput): any => {
    const { limit, page } = params;
    const $match: any = {
        deletedAt: null
    }

    const mainQuery: any = [
        { $match }
    ]
    if (limit) {
        mainQuery.push({ $limit: parseInt(limit) })
    }
    if (page) {
        const skip = (Number(page) - 1) * Number(limit)
        mainQuery.push({ $skip: skip })
    }

    const topCities = [ 'Hồ Chí Minh', 'Hà Nội' ]; // actual order is reversed
    mainQuery.push({ $addFields: { customOrder: { $indexOfArray: [topCities, '$name'] } } });
    
    mainQuery.push({ $sort: { customOrder: -1, name: 1 } });

    const mainQueryAll = [
        { $match }
    ]
    return { mainQuery, mainQueryAll }

}


