import { ListDistrictInput } from "../interfaces/district.entity";
import { Types } from "mongoose";

export const listDistrictQuery = (params: ListDistrictInput): any => {
    const { limit, page, cityId } = params;
    const $match: any = {
        deletedAt: null
    }
    if (cityId) {
        $match.cityId = new Types.ObjectId(cityId)
    }
    const mainQuery: any = [
        { $match }
    ]
    if (limit) {
        mainQuery.push({ $limit: parseInt(limit) })
    }
    if (page) {
        const skip = (Number(page) - 1) * Number(limit)
        mainQuery.push({ $skip: skip })
    }

    mainQuery.push({ $sort: { name: 1 } });

    const mainQueryAll = [
        { $match }
    ]
    return { mainQuery, mainQueryAll }

}


