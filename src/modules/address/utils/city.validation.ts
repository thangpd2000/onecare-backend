import * as yup from 'yup';
import { ListCityInput, CreateCityInput } from '../interfaces/city.entity';

export const createCityValidation = (data: CreateCityInput) => {
  const basedRules: any = {
    name: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const listCityValidation = (data: ListCityInput) => {
  const basedRules: any = {
    limit: yup.string(),
    page: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
