import { ListWardInput } from "../interfaces/ward.entity";
import { Types } from "mongoose";

export const listWardQuery = (params: ListWardInput): any => {
    const { limit, page, districtId } = params;
    const $match: any = {
        deletedAt: null
    }
    if (districtId) {
        $match.districtId = new Types.ObjectId(districtId)
    }
    const mainQuery: any = [
        { $match }
    ]
    if (limit) {
        mainQuery.push({ $limit: parseInt(limit) })
    }
    if (page) {
        const skip = (Number(page) - 1) * Number(limit)
        mainQuery.push({ $skip: skip })
    }

    mainQuery.push({ $sort: { name: 1 } });

    const mainQueryAll = [
        { $match }
    ]
    return { mainQuery, mainQueryAll }

}


