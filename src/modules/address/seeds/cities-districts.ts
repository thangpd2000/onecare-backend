import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { asyncForEach } from '@helper/function.helper';
import data from './data.json';
import { CityService } from '../services/city.service';
import { DistrictService } from '../services/district.service';

type City = {
  name: string;
  slug: string;
  type: string;
  name_with_type: string;
  code: string;
};

type District = {
  name: string;
  type: string;
  slug: string;
  name_with_type: string;
  path: string;
  path_with_type: string;
  code: string;
  parent_code: string;
};

@Injectable()
export class CityDistrictSeed {
  constructor(private readonly cityService: CityService, private readonly districtService: DistrictService) { }

  @Command({
    command: 'seed:cities',
    describe: 'seed cities',
    autoExit: true,
  })
  async create() {
    const cities = Object.entries(data);

    await asyncForEach(cities, async ([, city]: [string, City]) => {
      const cityDoc = await this.cityService.create({ name: city.name });
      const districts = Object.entries(city['quan-huyen']);

      const districtQuery = districts.map(
        ([, district]: [string, District]) => ({
          name: district.name,
          cityId: cityDoc._id,
        }),
      );

      await this.districtService.createMany(districtQuery);
    });
  }
}
