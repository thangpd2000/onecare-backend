import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { asyncForEach } from '@helper/function.helper';
import data from './data.json';
import { CityService } from '../services/city.service';
import { DistrictService } from '../services/district.service';
import { WardService } from '../services/ward.service';
import { City as ICity } from '../interfaces/city.interface';
import { District as IDistrict } from '../interfaces/district.interface';

type City = {
  name: string;
  slug: string;
  type: string;
  name_with_type: string;
  code: string;
};

type District = {
  name: string;
  type: string;
  slug: string;
  name_with_type: string;
  path: string;
  path_with_type: string;
  code: string;
  parent_code: string;
};

type Ward = {
  name: string;
  type: string;
  slug: string;
  name_with_type: string;
  path: string;
  path_with_type: string;
  code: string;
  parent_code: string;
};

@Injectable()
export class WardSeed {
  constructor(
    private readonly cityService: CityService,
    private readonly districtService: DistrictService,
    private readonly wardService: WardService,
  ) {}

  @Command({
    command: 'seed:wards',
    describe: 'seed wards',
    autoExit: true,
  })
  async create() {
    const cities = Object.entries(data);

    const cityObjects = await this.cityService.listCity({
      limit: '100',
      page: '1',
    });
    await asyncForEach(cities, async ([, city]: [string, City]) => {
      const cityDoc = cityObjects.data.find((c: ICity) => c.name === city.name);

      const districtObjects = await this.districtService.listDistrict({
        limit: '1000',
        page: '1',
        cityId: cityDoc._id.toString(),
      });

      const districts = Object.entries(city['quan-huyen']);

      await asyncForEach(
        districts,
        async ([, district]: [string, District]) => {
          const districtDoc = districtObjects.data.find(
            (d: IDistrict) => d.name === district.name,
          );

          const wards = Object.entries(district['xa-phuong']);

          const wardQuery = wards.map(([, ward]: [string, Ward]) => ({
            name: ward.name,
            districtId: districtDoc._id,
          }));

          await this.wardService.createMany(wardQuery);
        },
      );
    });
  }
}
