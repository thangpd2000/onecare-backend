import { MongooseModule } from "@nestjs/mongoose";
import { Global, Module } from "@nestjs/common";
import { DistrictSchema } from "./schemas/district.schema";
import { DistrictService } from "./services/district.service";
import { DistrictController } from "./controllers/district.controller";

@Global()
@Module({
    imports: [MongooseModule.forFeature([{ name: 'District', schema: DistrictSchema }])],
    providers: [DistrictService],
    exports: [DistrictService],
    controllers: [DistrictController]
})
export class DistrictModule { }
