import mongoose from 'mongoose';
export const CitySchema = new mongoose.Schema({
    name: String,
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: Date,
    deletedAt: {
        type: Date,
        default: null
    }
})