import mongoose from 'mongoose';
export const WardSchema = new mongoose.Schema({
  name: String,
  cityId: mongoose.Types.ObjectId,
  districtId: mongoose.Types.ObjectId,
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: Date,
  deletedAt: {
    type: Date,
    default: null,
  },
});
