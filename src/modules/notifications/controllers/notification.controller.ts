import { RequireAuth } from '@modules/users/utils/user.guard';
import {
  destination,
  editFileName,
  imageFileFilter,
} from '@modules/notifications/utils/notification.upload';
import {
  Post,
  Controller,
  UseGuards,
  UseInterceptors,
  Body,
  UploadedFile,
  Get,
  Query,
  Put,
  Param,
  Delete,
  Req,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { NotificationService } from '../services/notification.service';
import {
  CountNotificationResponse,
  DeleteNotificationResponse,
  ListNotificationInput,
  ListNotificationResponse,
  NotificationCreateInput,
  NotificationCreateResponse,
  NotificationUpdateInput,
  NotificationUpdateInputId,
} from '../interfaces/notification.entity';
import { StatusCodes } from '@modules/base.interface';
import { User } from '@modules/users/interfaces/user.interface';
import { Roles } from '@modules/users/interfaces/user.entity';
@Controller('notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}
  @Post()
  @Roles('STAFF-NOTIFICATION-create')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: destination,
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async createNotification(
    @Body() body: NotificationCreateInput & { fromUser: string },
    @UploadedFile() file,
  ): Promise<NotificationCreateResponse> {
    if (file) {
      body.image = `/assets/uploads/notifications/${file.filename}`;
    }
    body.fromUser = body.fromUser ? JSON.parse(body.fromUser) : [];
    const data = await this.notificationService.createNotifications(body);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }

  @Put('/:id')
  @Roles('STAFF-NOTIFICATION-edit')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: destination,
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async updateNotification(
    @Body() body: NotificationUpdateInput & { fromUser: string },
    @Param() params: NotificationUpdateInputId,
    @UploadedFile() file,
  ): Promise<NotificationCreateResponse> {
    body._id = params.id;
    if (file) {
      body.image = `/assets/uploads/notifications/${file.filename}`;
    }
    body.fromUser = body.fromUser ? JSON.parse(body.fromUser) : [];
    const data = await this.notificationService.updateNotification(body);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
  @Delete('/:id')
  @Roles('STAFF-NOTIFICATION-delete')
  async deleteNotification(
    @Param() params: NotificationUpdateInputId,
  ): Promise<DeleteNotificationResponse> {
    const data = await this.notificationService.deleteNotification(params);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
  @Get('/list')
  @Roles('STAFF-NOTIFICATION-read')
  async listNotification(
    @Query() query: ListNotificationInput,
  ): Promise<ListNotificationResponse> {
    const data = await this.notificationService.listNotification(query);
    return {
      status: StatusCodes.SUCCESS,
      ...data,
    };
  }
  @Get('/listApp')
  @Roles('CUSTOMERS')
  async listNotificationApp(
    @Query() query: ListNotificationInput,
    @Req() req: any,
  ): Promise<ListNotificationResponse> {
    const data = await this.notificationService.listNotificationApp(
      query,
      req.user,
    );
    return {
      status: StatusCodes.SUCCESS,
      ...data,
    };
  }
  @Get('/count')
  @Roles('CUSTOMERS')
  async countNotification(
    @Req() req: Request & { user: User },
  ): Promise<CountNotificationResponse> {
    const data = await this.notificationService.countNotification(req.user);

    return {
      status: StatusCodes.SUCCESS,
      ...data,
    };
  }
  @Get('/markOneAsSeen')
  @Roles('CUSTOMERS')
  async markAsSeen(
    @Req() req: Request & { user: User },
  ): Promise<CountNotificationResponse> {
    const data = await this.notificationService.markNotificationAsSeen(
      req.user,
    );

    return {
      status: StatusCodes.SUCCESS,
      ...data,
    };
  }
  @Get('/markAllAsSeen')
  @Roles('CUSTOMERS')
  async resetCountNotification(
    @Req() req: Request & { user: User },
  ): Promise<CountNotificationResponse> {
    const data = await this.notificationService.resetCountNotification(
      req.user,
    );

    return {
      status: StatusCodes.SUCCESS,
      ...data,
    };
  }
  @Get('/detail/:id')
  @Roles('STAFF-NOTIFICATION-read', 'CUSTOMERS')
  async detailNotification(
    @Param() params: NotificationUpdateInputId,
  ): Promise<NotificationCreateResponse> {
    const data = await this.notificationService.detailNotification(params);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
}
