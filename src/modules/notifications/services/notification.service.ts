import { UserErrors } from '@helper/error.helper';
import { StatusCodes, TypeNoti, Actor } from '@modules/base.interface';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  CountNotificationResponse,
  ListNotificationInput,
  ListNotificationResponse,
  NotificationCreateInput,
  NotificationUpdateInput,
  NotificationUpdateInputId,
} from '../interfaces/notification.entity';
import { Notification } from '../interfaces/notification.interface';
import {
  PayloadData,
  NotificationMsgCreateInput,
} from '@modules/notifications/interfaces/notification.entity';
import {
  validateDeleteNotification,
  validateListNotification,
  validateNotification,
  validateUpdateNotification,
} from '../utils/notification.validation';
import { omit } from 'lodash';
import { remove } from 'fs-extra';
import {
  createNotificationToAll,
  createNotificationToTag,
} from '../utils/onesignal';
import { User } from '@modules/users/interfaces/user.interface';
import console from 'console';

@Injectable()
export class NotificationService {
  constructor(
    @InjectModel('Notification')
    private readonly notificationModel: Model<Notification>,
    @InjectModel('User')
    private readonly userModel: Model<User>,
  ) {}

  async createNotifications(
    body: NotificationCreateInput,
  ): Promise<Notification> {
    validateNotification(body);
    const fromUserOld = body.fromUser;
    if (body.fromUser.includes('ALL') === true) {
      createNotificationToAll(body.title, body.content, {
        actor: Actor.SYSTEM,
        type: TypeNoti.GENERAL,
        title: body.title,
        content: body.content,
      });
    } else {
      const users = await this.userModel
        .find({
          pointRankingId: { $in: body.fromUser },
        })
        .select({ _id: 1 });
      const listUserId = [];
      for (const user of users) {
        listUserId.push(user._id);
      }
      createNotificationToTag(body.title, body.content, listUserId, {
        actor: Actor.SYSTEM,
        type: TypeNoti.GENERAL,
        title: body.title,
        content: body.content,
      });
      body.fromUser = listUserId;
    }

    const [data] = await Promise.all([
      new this.notificationModel({
        ...body,
        payload: {
          actor: Actor.SYSTEM,
          type: TypeNoti.GENERAL,
        },
        toUser: fromUserOld,
      }).save(),
      this.userModel.updateMany(
        {},
        {
          $inc: { unreadNotification: 1 },
        },
      ),
    ]);

    return data;
  }

  async updateNotification(
    body: NotificationUpdateInput,
  ): Promise<Notification> {
    validateUpdateNotification(body);
    const fromUserOld = body.fromUser;
    const checkExist = await this.notificationModel.findOne({
      _id: body._id,
      deletedAt: null,
    });
    if (!checkExist) {
      UserErrors(StatusCodes.NOTIFICATION_DO_NOT_EXISTS);
    }
    body.toUser = fromUserOld;
    const imageOld = checkExist.image;
    checkExist.set(omit(body, '_id'));
    const promises = [checkExist.save(), remove(`.${imageOld}`)];
    const [data] = await Promise.all(promises);
    return data;
  }

  async deleteNotification(
    params: NotificationUpdateInputId,
  ): Promise<boolean> {
    validateDeleteNotification(params);
    const checkExist = await this.notificationModel.findOne({
      _id: params.id,
      deletedAt: null,
    });
    if (!checkExist) {
      UserErrors(StatusCodes.NOTIFICATION_DO_NOT_EXISTS);
    }
    await this.notificationModel
      .findByIdAndUpdate(params.id, {
        deletedAt: new Date(),
      })
      .exec();
    return true;
  }

  async listNotification(
    query: ListNotificationInput,
  ): Promise<ListNotificationResponse> {
    validateListNotification(query);
    const { limit, page, title } = query;
    const skip = (Number(page) - 1) * Number(limit);
    const conditions: any = {
      deletedAt: null,
      'payload.actor': Actor.SYSTEM,
    };
    if (title) {
      conditions.title = new RegExp(
        title.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'),
        'i',
      );
    }
    const [data, total] = await Promise.all([
      this.notificationModel
        .find(conditions)
        .limit(Number(limit))
        .skip(skip)
        .sort({ _id: -1 })
        .exec(),
      this.notificationModel.countDocuments(conditions),
    ]);
    return { data, total };
  }

  async listNotificationApp(
    query: ListNotificationInput,
    user,
  ): Promise<ListNotificationResponse> {
    validateListNotification(query);
    const { limit, page } = query;
    const skip = (Number(page) - 1) * Number(limit);
    const listCon = ['ALL', user._id];
    const conditions: any = {
      deletedAt: null,
      fromUser: { $in: listCon },
    };
    const [data, total] = await Promise.all([
      this.notificationModel
        .find(conditions)
        .limit(Number(limit))
        .skip(skip)
        .sort({ _id: -1 })
        .exec(),
      this.notificationModel.countDocuments(conditions),
    ]);
    return { data, total };
  }

  async countNotification(user: User): Promise<CountNotificationResponse> {
    return { data: user.unreadNotification || 0 };
  }

  async markNotificationAsSeen(user: User): Promise<CountNotificationResponse> {
    const unreadNotification = user.unreadNotification;

    const newUnreadNotification =
      unreadNotification && unreadNotification > 0 ? unreadNotification - 1 : 0;

    const updatedUser = await this.userModel.findByIdAndUpdate(
      user._id,
      {
        $inc: { unreadNotification: newUnreadNotification },
      },
      { new: true },
    );

    return { data: updatedUser.unreadNotification };
  }

  async resetCountNotification(user: User): Promise<CountNotificationResponse> {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      user._id,
      {
        $set: { unreadNotification: 0 },
      },
      { new: true },
    );

    return { data: updatedUser.unreadNotification };
  }

  async detailNotification(
    params: NotificationUpdateInputId,
  ): Promise<Notification> {
    validateDeleteNotification(params);
    const checkExist = await this.notificationModel.findOne({
      _id: params.id,
      deletedAt: null,
    });
    if (!checkExist) {
      UserErrors(StatusCodes.NOTIFICATION_DO_NOT_EXISTS);
    }
    return checkExist;
  }

  async createNotification(
    data: NotificationMsgCreateInput,
  ): Promise<Notification> {
    return await this.notificationModel.create(data);
  }
}
