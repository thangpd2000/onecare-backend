import mongoose from 'mongoose';
export const NotificationSchema = new mongoose.Schema({
  title: String,
  content: String,
  payload: Object,
  image: String,
  fromUser: [String],
  toUser: [String],
  status: Boolean,
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: Date,
  deletedAt: {
    type: Date,
    default: null,
  },
});
