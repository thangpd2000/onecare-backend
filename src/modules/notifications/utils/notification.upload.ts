import { extname } from 'path';
import fs from 'fs-extra';
export const imageFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|JPG|JPEG|jpeg|PNG|png|gif)$/)) {
    return callback(new Error('Only image files are allowed!'), false);
  }
  callback(null, true);
};

export const editFileName = (req, file, callback) => {
  const randomName = Array(32)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  const fileExtName = extname(file.originalname);
  callback(null, `${randomName}${fileExtName}`);
};

export const destination = (req, file, callback) => {
  const dir = `./assets/uploads/notifications`;
  fs.ensureDirSync(dir);
  callback(null, dir);
};
