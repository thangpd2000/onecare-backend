import { envConfig } from '@helper/env.helper';
import * as OneSignal from 'onesignal-node';
const appId = envConfig.appId;
const apiKey = envConfig.apiKey;
const apiRoot = envConfig.apiRoot;
const client = new OneSignal.Client(appId, apiKey, { apiRoot });
import { LengthContentNoti } from '@modules/base.interface';

export const createNotificationToPlayerId = async (
  title,
  content,
  listPlayerId,
  payload = null
) => {
  const validContent = content && content.trim() ? content : title;
  if (payload) {
    if(payload?.title) {
      payload.title = payload.title.slice(LengthContentNoti.MIN, LengthContentNoti.MAX);
    }
    if (payload?.content) {
      payload.content = payload.content.slice(LengthContentNoti.MIN, LengthContentNoti.MAX)
    }
  }
  const notification = {
    contents: {
      en: validContent.slice(LengthContentNoti.MIN, LengthContentNoti.MAX),
      tr: title,
    },
    data: { payload },
    // eslint-disable-next-line @typescript-eslint/camelcase
    include_player_ids: listPlayerId,
  };
  return await client.createNotification(notification);
};

export const createNotificationToAll = async (title: string, content, payload = null) => {
  try {
    
    if (payload) {
      if(payload?.title) {
        payload.title = payload.title.slice(LengthContentNoti.MIN, LengthContentNoti.MAX);
      }
      if (payload?.content) {
        payload.content = payload.content.slice(LengthContentNoti.MIN, LengthContentNoti.MAX)
      }
    }
    const validContent = content && content.trim() ? content : title;
    const notification = {
      contents: {
        en: validContent.slice(LengthContentNoti.MIN, LengthContentNoti.MAX),
        tr: title,
      },
      data: { payload },
    
      // eslint-disable-next-line @typescript-eslint/camelcase
      included_segments: ['Engaged Users', 'Active Users', 'Subscribed Users'],
    };
    const msgNoti = await client.createNotification(notification);
    return msgNoti;
  } catch (error) {
    console.log('Create notification all err',JSON.stringify(error));
  }
};
export const createNotificationToTag = async (title: string, content, tags: string[], payload = null) => {
  try {
    const validContent = content && content.trim() ? content : title;
    if (payload) {
      if(payload?.title) {
        payload.title = payload.title.slice(0, 500);
      }
      if (payload?.content) {
        payload.content = payload.content.slice(0, 500)
      }
    }
    const prmCreateNoti = [];
    for (const id of tags) {
      const filters: any = [
        { field: 'tag', key: 'user_id', relation: '=', value: id },
      ];
      const notification = {
        contents: {
          en: validContent.slice(0, 500),
          tr: title,
        },
        data: { payload },
        filters: filters,
      };
      prmCreateNoti.push(client.createNotification(notification));
    }
    let responseNoti = null;
    if (prmCreateNoti.length) {
      responseNoti = await Promise.all(prmCreateNoti);
    }
    return responseNoti;
    
  } catch (error) {
    console.log('CreateNotificationToTag err',JSON.stringify(error));
  }
};

export const getDevices = async () => {
  const allDevices = await client.viewDevices();
  return allDevices;
}