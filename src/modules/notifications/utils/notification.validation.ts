import * as yup from 'yup';
import {
  ListNotificationInput,
  NotificationCreateInput,
  NotificationUpdateInputId,
} from '../interfaces/notification.entity';
export const validateNotification = (data: NotificationCreateInput) => {
  const basedRules: any = {
    title: yup
      .string()
      .min(2)
      .max(2000)
      .required(),
    content: yup
      .string()
      .min(2)
      .max(5000)
      .required(),
    image: yup.string().required(),
    fromUser: yup
      .array()
      .of(yup.string())
      .required(),
    status: yup.boolean().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateListNotification = (data: ListNotificationInput) => {
  const basedRules: any = {
    limit: yup.string(),
    page: yup.string(),
    title: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateUpdateNotification = (data: NotificationCreateInput) => {
  const basedRules: any = {
    _id: yup.string().required(),
    title: yup
      .string()
      .min(2)
      .max(2000)
      .required(),
    content: yup
      .string()
      .min(2)
      .max(5000)
      .required(),
    image: yup.string().required(),
    fromUser: yup
      .array()
      .of(yup.string())
      .required(),
    status: yup.boolean().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateDeleteNotification = (data: NotificationUpdateInputId) => {
  const basedRules: any = {
    id: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
