import { Document } from 'mongoose';
import { PayloadData } from '@modules/notifications/interfaces/notification.entity';
export interface Notification extends Document {
  readonly _id: string;
  readonly title: string;
  readonly content: string;
  readonly image: string;
  readonly fromUser: [string];
  readonly toUser: [string];
  readonly status: boolean;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
  readonly payload: PayloadData;
}
