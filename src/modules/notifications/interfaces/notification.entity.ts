import { StatusCodes, TypeNoti, Actor } from '@modules/base.interface';
import { Notification } from './notification.interface';
export class NotificationCreateInput {
  title: string;
  content: string;
  image: string;
  fromUser: string[];
  status: boolean;
}

export class NotificationCreateResponse {
  status?: StatusCodes;
  data: Notification;
}

export class ListNotificationInput {
  limit: string;
  page: string;
  title?: string;
}

export class ListNotificationResponse {
  status?: StatusCodes;
  data: Notification[];
  total: number;
}

export class CountNotificationResponse {
  status?: StatusCodes;
  data: number;
}

export class NotificationUpdateInput {
  _id: string;
  title: string;
  content: string;
  image: string;
  fromUser: string[];
  toUser?: string[];
  status: boolean;
}

export class NotificationUpdateInputId {
  id: string;
}
export class DeleteNotificationResponse {
  status: StatusCodes;
  data: boolean;
}

export enum TitleNotification {
  HAPPY_BIRTHDAY = 'Chúc mừng sinh nhật',
  PROMOTION = 'Khuyến Mại',
  ORDER = 'Đơn hàng',
}

export class NotificationCommonInput {
  title: string;
  content: string;
  image: string;
  fromUser: string[];
  status: boolean;
}

export interface PayloadData {
  codeOrder?: string;
  hotline?: number;
  point?: number;
  userId?: string;
  actor?: Actor;
  type?: TypeNoti;
  storeIds?: string[];
  orderId?: string;
  code?: string;
}

export interface NotificationMsgCreateInput extends NotificationCreateInput {
  payload: PayloadData;
  createdAt: Date;
}
