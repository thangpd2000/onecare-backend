import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { StatusCodes } from "@modules/base.interface";
import { UserErrors } from "@helper/error.helper";

@Injectable()
export class ValidateUploadInput implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean {



    const request = context.switchToHttp().getRequest();
    const { width, height } = request.body;
    
    if (!width) {
      UserErrors(StatusCodes.WIDTH_CAN_NOT_BE_BLANK);
    }
    if (!height) {
      UserErrors(StatusCodes.HEIGHT_CAN_NOT_BE_BLANK);
    }

    return true;
  }
}