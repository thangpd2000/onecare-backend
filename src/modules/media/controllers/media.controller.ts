import { uploadImage } from '@utils/photo';
import { UserErrors } from '@helper/error.helper';
import { StatusCodes } from '@modules/base.interface';
import { RequireAuth } from '@modules/users/utils/user.guard';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor, MulterModule  } from '@nestjs/platform-express';
import { MediaService } from '../services/media.service';
import {
  FetchMediaParams,
  FileDimension,
  FileDimensionMulti,
  MediaResponse,
  MultiMediaResponse
} from '../interfaces/media.entity';

@Controller('media')
export class MediaController {
  constructor(private readonly mediaService: MediaService) {}

  @Post()
  @UseGuards(RequireAuth)
  @UseInterceptors(FileInterceptor('file', {
    limits: {
      fileSize: 5242880
    }
  }))
  async upload(
    @UploadedFile() file,
    @Body() body: FileDimension,
  ): Promise<MediaResponse> {
    if (!file) {
      UserErrors(StatusCodes.FILE_CAN_NOT_BE_BLANK);
    }
    const uploadedFile = uploadImage(file, 'uploads');
    const media = await this.mediaService.create({
      ...uploadedFile,
      ...body,
    });
    return {
      data: media,
      status: StatusCodes.SUCCESS,
    };
  }

  @Post('multi')
  @UseGuards(RequireAuth)
  @UseInterceptors(
    FilesInterceptor('files', 5, {
      limits: {
        fileSize: 5242880
      }
    }),
  )
  async uploadMultiFile(
    @UploadedFiles() files,
    @Body() body: FileDimensionMulti
  ): Promise<MultiMediaResponse> {
    if(!files || files.length === 0) {
      UserErrors(StatusCodes.FILE_CAN_NOT_BE_BLANK);
    }
    // when app upload if 1 image : with and height not array
    const arrWidth = (body.width && Array.isArray(body.width)) ? body.width : [body.width];
    const arrHeight = (body.height && Array.isArray(body.height)) ? body.height : [body.height];
    const prmMedia = [];
    for(let i = 0; i < files.length; i++ ) {
      const file = files[i];
      const width = Number(arrWidth[i]) || 0;
      const height = Number(arrHeight[i]) || 0;
      const uploadedFile = uploadImage(file, 'uploads', width, height);
      let dimensions: any = undefined;
      if (body.width && body.height) {
        dimensions = {
          width,
          height
        }
      }
      prmMedia.push(this.mediaService.create({
        ...uploadedFile,
        width,
        height,
        dimensions
      }))
    }
    const dataMultiMedia = await Promise.all(prmMedia);
    return {
      data: dataMultiMedia,
      status: StatusCodes.SUCCESS
    }

  }

  @Get(':id')
  async fetchMedia(@Param() params: FetchMediaParams): Promise<MediaResponse> {
    const media = await this.mediaService.findById(params.id);
    return {
      data: media,
      status: StatusCodes.SUCCESS,
    };
  }
}
