import mongoose from 'mongoose';
import { Media } from '../interfaces/media.interface';
import { MediaType } from '../interfaces/media.entity';

export const MediaSchema = new mongoose.Schema(
  {
    url: String,
    width: Number,
    height: Number,
    dimensions: {
      width: Number,
      height: Number
    },
    type: {
      type: MediaType,
      default: MediaType.PHOTO,
    },
    createdAt: {
      type: Date,
      default: new Date(),
    },
    updatedAt: Date,
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  { strict: false },
);

/**
 * Password hash middleware.
 */

export const MediaModel =
  mongoose.models['Medias'] ||
  mongoose.model<Media>('Medias', MediaSchema, 'Medias');
