import { Media } from "./media.interface";
import { StatusCodes } from "@modules/base.interface";

export enum MediaType {
  PHOTO = "PHOTO",
  VIDEO = "VIDEO",
  FILE = "FILE",
}

export class FileDimension {
  width: number
  height: number
}

export class UploadFileInput extends FileDimension {
  url: string
  type?: MediaType
}

export class UploadFileInputMulti extends UploadFileInput {
  dimensions?: FileDimensionMulti
}

export class FetchMediaParams {
  id: string
}

export class MediaResponse {
  status: StatusCodes
  data: Media
}

export class MultiMediaResponse {
  status: StatusCodes
  data: Media[]
}

export class FileDimensionMulti {
  width?: number | number[]
  height?: number | number[]
}