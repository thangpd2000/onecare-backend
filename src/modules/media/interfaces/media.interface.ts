import { Document } from 'mongoose';
import { MediaType } from './media.entity';
import { FileDimension } from '@modules/media/interfaces/media.entity'
export interface Media extends Document {
  readonly _id: string;
  readonly url: string;
  readonly thumbnail: string;
  readonly thumbnail2x: string;
  readonly type: MediaType;
  readonly width: number;
  readonly height: number;
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
  readonly dimensions?: FileDimension;
}
