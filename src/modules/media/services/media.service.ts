import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Media } from '../interfaces/media.interface';
import { UploadFileInput, UploadFileInputMulti } from '../interfaces/media.entity';

@Injectable()
export class MediaService {
  constructor(@InjectModel('Media') private readonly mediaModel: Model<Media>) { }

  async create(body: UploadFileInputMulti): Promise<Media> {
    const createdData = new this.mediaModel(body);
    return await createdData.save();
  }

  async findById(_id: string): Promise<Media> {
    return await this.mediaModel.findOne({ _id }).exec();
  }

}
