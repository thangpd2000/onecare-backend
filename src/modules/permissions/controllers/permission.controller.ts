import {
  Get,
  Put,
  Body,
  Post,
  Query,
  Param,
  Delete,
  Controller,
} from '@nestjs/common';
import {
  PermissionInput,
  DetailPermission,
  PermissionResponse,
  ListGroupsResponse,
  ListPermissionInput,
  UpdatePermissionInput,
  DeletePermissionInput,
  ListPermissionResponse,
  UpdatePermissionInputId,
  DeletePermissionResponse,
} from '../interfaces/permission.entity';
import { StatusCodes } from '@modules/base.interface';
import { Roles } from '@modules/users/interfaces/user.entity';
import { PermissonService } from '../services/permission.service';

@Controller('permission')
export class PermissionController {
  constructor(private readonly permissionService: PermissonService) {}

  @Post()
  @Roles('ADMIN')
  async createPermission(
    @Body() body: PermissionInput & { permission: string },
  ): Promise<PermissionResponse> {
    body.permission = body.permission ? JSON.parse(body.permission) : [];
    const permission = await this.permissionService.create(body);
    return {
      status: StatusCodes.SUCCESS,
      data: permission,
    };
  }

  @Put('/:id')
  @Roles('ADMIN')
  async updatePermission(
    @Body() body: UpdatePermissionInput & { permission: string },
    @Param() { id }: UpdatePermissionInputId,
  ): Promise<PermissionResponse> {
    body.permission = body.permission ? JSON.parse(body.permission) : [];
    body._id = id;
    const permission = await this.permissionService.update(body);
    return {
      status: StatusCodes.SUCCESS,
      data: permission,
    };
  }
  @Delete('/:id')
  @Roles('ADMIN')
  async deletePermission(
    @Param() param: DeletePermissionInput,
  ): Promise<DeletePermissionResponse> {
    const permission = await this.permissionService.delete(param);
    return {
      status: StatusCodes.SUCCESS,
      data: permission,
    };
  }

  @Get('/detail/:id')
  @Roles('ADMIN')
  async detailPermission(
    @Param() param: DetailPermission,
  ): Promise<PermissionResponse> {
    const permission = await this.permissionService.detail(param);
    return {
      status: StatusCodes.SUCCESS,
      data: permission,
    };
  }

  @Get('/list')
  async listGroups(
    @Query() query: ListPermissionInput,
  ): Promise<ListGroupsResponse> {
    const permissions = await this.permissionService.listGroups(query);
    return {
      status: StatusCodes.SUCCESS,
      ...permissions,
    };
  }

  @Get('/listCms')
  @Roles('ADMIN')
  async listGroupsCms(
    @Query() query: ListPermissionInput,
  ): Promise<ListGroupsResponse> {
    const permissions = await this.permissionService.listGroups(query);
    return {
      status: StatusCodes.SUCCESS,
      ...permissions,
    };
  }

  @Get('/listPermisson')
  async listPermission(): Promise<ListPermissionResponse> {
    const permissions = await this.permissionService.listPermission();
    return {
      status: StatusCodes.SUCCESS,
      ...permissions,
    };
  }
}
