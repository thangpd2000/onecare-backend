import {
  WebSocketServer,
  WebSocketGateway,
  SubscribeMessage,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Injectable } from '@nestjs/common';
import { EventSocket } from '@modules/base.interface';
import { RequireAuth } from '@modules/users/utils/user.guard';

// @WebSocketGateway()
@WebSocketGateway({
  namespace: 'cms-permission',
  origins: '*:*',
})
@Injectable()
export class CMSPermisionGateway
  implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(private readonly auth: RequireAuth) {}
  @WebSocketServer() server: Server;
  async handleConnection(client: Socket) {
    try {
      if (client.handshake.query.authorization && client.handshake.query.authorization !== 'null') {
        const decodedToken = await this.auth.validateToken(
          client.handshake.query.authorization,
        );
        console.log('Client connected to socket in room', decodedToken.userId);
        client.join(decodedToken.userId);
      }
      console.log('cliient connected to socket', client.id)
    } catch (err) {
      this.server.emit(EventSocket.ERROR, err);
      client.disconnect(true);
    }
  }

  @SubscribeMessage(EventSocket.CLIENT_JOIN_ROOM_WHEN_LOGIN)
  async handleMessage(client: Socket, payload: any) {
    try {
      const authorization = payload.authorization;
      if (authorization) {
        // decode token to user id
        const decodedToken = await this.auth.validateToken(authorization);
        console.log('Client connected when login to socket in room', decodedToken.userId);
        client.join(decodedToken.userId);
      }      
    } catch (error) {
      this.server.emit(EventSocket.ERROR, error);
    }
  }

  async handleDisconnect(client: Socket) {
    try {
      const decodedToken = await this.auth.validateToken(
        client.handshake.query.authorization,
      );
      console.log(
        'Client disconnected from socket in room',
        decodedToken.userId,
      );
      client.leave(decodedToken.userId);
    } catch (err) {
      this.server.emit(EventSocket.ERROR, err);
    }
  }
  async notifyPermissionChanged(userIds: any[], data: any) {
    try {
      if (userIds.length > 0) {
        for (const userId of userIds) {
          this.server.to(userId._id).emit('PERMISSION_CHANGED', {
            data: data,
            userId
          });
        }
      }
    } catch (e) {
      console.error(e);
    }
  }
}
