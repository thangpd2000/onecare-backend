import {
  PermissionInput,
  DetailPermission,
  ListPermissionInput,
  UpdatePermissionInput,
  DeletePermissionInput,
} from '../interfaces/permission.entity';
import * as yup from 'yup';
import { UserErrors } from '@helper/error.helper';
import { StatusCodes } from '@modules/base.interface';
import { Permission } from '../interfaces/permission.interface';

export const validatePermission = (data: PermissionInput) => {
  const basedRules: any = {
    name: yup
      .string()
      .min(2)
      .max(100)
      .required(),
    description: yup.string().max(1000),
    status: yup.boolean().required(),
    permission: yup.array().of(
      yup.object().shape({
        _id: yup.string().required(),
        read: yup.number().required(),
        create: yup.number().required(),
        edit: yup.number().required(),
        delete: yup.number().required(),
      }),
    ),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateUpdatePermission = (data: UpdatePermissionInput) => {
  const basedRules: any = {
    _id: yup.string().required(),
    name: yup
      .string()
      .min(2)
      .max(100)
      .required(),
    description: yup.string().max(1000),
    status: yup.boolean().required(),
    permission: yup.array().of(
      yup.object().shape({
        _id: yup.string().required(),
        type: yup.string(),
        read: yup.number().required(),
        create: yup.number().required(),
        edit: yup.number().required(),
        delete: yup.number().required(),
      }),
    ),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const checkTypePermission = (
  permissions: Permission[],
  permissionInput: any[],
) => {
  const userPermissions = [];

  for (const permission of permissionInput) {
    const inputPermission = permissions.find(
      (p: Permission) => p._id.toString() === permission._id.toString(),
    );
    if (
      (inputPermission.read == false && permission.read !== -1) ||
      (inputPermission.create == false && permission.create !== -1) ||
      (inputPermission.edit == false && permission.edit !== -1) ||
      (inputPermission.delete == false && permission.delete !== -1)
    ) {
      UserErrors(StatusCodes.PERMISSION_MALFORMED);
    }

    userPermissions.push({ ...permission, type: inputPermission.type });
  }
  //assign array and remove duplicate
  const permissionType = userPermissions.reduce(
    (a, c) => a.add(c._id),
    new Set(),
  );
  const permissionNews = permissions.filter(
    v => !permissionType.has(v._id.toString()),
  );
  if (permissionNews.length > 0) {
    for (const permissionNew of permissionNews) {
      const obj = {
        _id: permissionNew._id,
        type: permissionNew.type,
        read: 0,
        create: 0,
        edit: 0,
        delete: 0,
      };
      if (permissionNew.read == false) {
        obj.read = -1;
      }
      if (permissionNew.create == false) {
        obj.create = -1;
      }
      if (permissionNew.edit == false) {
        obj.edit = -1;
      }
      if (permissionNew.delete == false) {
        obj.delete = -1;
      }
      userPermissions.push(obj);
    }
  }
  return userPermissions;
};

export const validateDeletePermission = (data: DeletePermissionInput) => {
  const basedRules: any = {
    id: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateDetailPermission = (data: DetailPermission) => {
  const basedRules: any = {
    id: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateListPermission = (data: ListPermissionInput) => {
  const basedRules: any = {
    name: yup.string(),
    status: yup.string(),
    limit: yup.string(),
    page: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
