import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupSchema } from './schemas/group.schema';
import { RequireAuth } from '@modules/users/utils/user.guard';
import { PermissionSchema } from './schemas/permission.schema';
import { UserSchema } from '@modules/users/schemas/user.schema';
import { PermissonService } from './services/permission.service';
import { CMSPermisionGateway } from './gateways/cmsPermission.gateway';
import { PermissionController } from './controllers/permission.controller';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Group', schema: GroupSchema },
      { name: 'Permission', schema: PermissionSchema },
      { name: 'User', schema: UserSchema },
    ]),
  ],
  providers: [PermissonService, CMSPermisionGateway, RequireAuth],
  exports: [PermissonService, CMSPermisionGateway],
  controllers: [PermissionController],
})
export class PermissionModule {}
