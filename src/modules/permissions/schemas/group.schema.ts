import mongoose from 'mongoose';

export const GroupSchema = new mongoose.Schema({
  name: String,
  description: String,
  status: Boolean,
  permission: Array,
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: Date,
  deletedAt: {
    type: Date,
    default: null,
  },
});
