import mongoose from 'mongoose';

export const PermissionSchema = new mongoose.Schema({
    type: String,
    read: Boolean,
    create: Boolean,
    edit: Boolean,
    delete: Boolean
})