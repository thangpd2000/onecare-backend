import { StatusCodes } from '@modules/base.interface';
import { Group } from './group.interface';
import { Permission } from './permission.interface';

export enum PermissionType {
  PRODUCT = 'PRODUCT',
  ORDER = 'ORDER',
  PROMOTION = 'PROMOTION',
  NEWS = 'NEWS',
  TERMS_OF_DELIVERY = 'TERMS_OF_DELIVERY',
  CONTACT = 'CONTACT',
  BANK_ACCOUNT = 'BANK_ACCOUNT',
  FAQ = 'FAQ',
  MESSAGE = 'MESSAGE',
  NOTIFICATION = 'NOTIFICATION',
  CUSTOMERS = 'CUSTOMERS',
  STORE='STORE',
  CATEGORYPRODUCT='CATEGORYPRODUCT',
  CONFIGURATIONS='CONFIGURATIONS',
  RANK='RANK',
  CATEGOTYSYSTEM='CATEGOTYSYSTEM',
  STATISTICS='STATISTIC'
}
export class PermissionGroup {
  _id: string;
  read: number;
  create: number;
  edit: number;
  delete: number;
}
export class PermissionInput {
  name: string;
  description: string;
  status: boolean;
  permission: PermissionGroup[];
}
export class PermissionResponse {
  status: StatusCodes;
  data: Group;
}

export class UpdatePermissionInput {
  _id: string;
  name: string;
  description: string;
  status: boolean;
  permission: PermissionGroup[];
}
export class UpdatePermissionInputId {
  id: string;
}

export class DeletePermissionInput {
  id: string;
}

export class DetailPermission {
  id: string;
}

export class DeletePermissionResponse {
  status: StatusCodes;
  data: boolean;
}

export class ListPermissionInput {
  status: string;
  name: string;
  limit: string;
  page: string;
}
export class ListGroupsResponse {
  status?: StatusCodes;
  data: Group[];
  total: number;
}
export class ListPermissionResponse {
  status?: StatusCodes;
  data: Permission[];
}
