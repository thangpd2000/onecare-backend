import { Document } from 'mongoose';
export interface Group extends Document {
  readonly _id: string;
  readonly name: string;
  readonly description: string;
  readonly status: boolean;
  readonly permission: any[];
  readonly createdAt: Date;
  readonly updatedAt: Date;
  readonly deletedAt: Date;
}
