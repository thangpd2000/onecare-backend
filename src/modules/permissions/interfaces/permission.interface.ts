import { Document } from 'mongoose';

export interface Permission extends Document {
    readonly _id: string;
    readonly type: string;
    readonly read: boolean;
    readonly create: boolean;
    readonly edit: boolean;
    readonly delete: boolean;
}