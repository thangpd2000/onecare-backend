import {
  PermissionInput,
  DetailPermission,
  ListGroupsResponse,
  ListPermissionInput,
  DeletePermissionInput,
  UpdatePermissionInput,
  ListPermissionResponse,
} from '../interfaces/permission.entity';
import {
  validatePermission,
  checkTypePermission,
  validateListPermission,
  validateDeletePermission,
  validateDetailPermission,
  validateUpdatePermission,
} from '../utils/permission.validation';
import { Model } from 'mongoose';
import { omit, isEqual } from 'lodash';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserErrors } from '@helper/error.helper';
import { Group } from '../interfaces/group.interface';
import { StatusCodes } from '@modules/base.interface';
import { Type } from '@modules/users/interfaces/user.entity';
import { Permission } from '../interfaces/permission.interface';
import { User } from '@modules/users/interfaces/user.interface';
import { CMSPermisionGateway } from '../gateways/cmsPermission.gateway';

@Injectable()
export class PermissonService {
  constructor(
    @InjectModel('Group') private readonly groupModel: Model<Group>,
    @InjectModel('User') private readonly userModel: Model<User>,
    private readonly cmsPermisionGateway: CMSPermisionGateway,
    @InjectModel('Permission')
    private readonly permissionModel: Model<Permission>,
  ) {}
  async create(data: PermissionInput): Promise<Group> {
    validatePermission(data);
    const permissions = await this.permissionModel.find().exec();
    //check format permission
    const permissionInput = checkTypePermission(permissions, data.permission);
    data.permission = permissionInput;
    const createdData = new this.groupModel(data);
    return await createdData.save();
  }

  async update(data: UpdatePermissionInput): Promise<Group> {
    validateUpdatePermission(data);
    const permission = await this.groupModel
      .findOne({
        _id: data._id,
        deletedAt: null,
      })
      .select({ createdAt: 0, deletedAt: 0, __v: 0, _id: 0 })
      .exec();
    if (!permission) {
      UserErrors(StatusCodes.PERMISSION_DO_NOT_EXISTS);
    }
    const isDuplicated = await this.groupModel.exists({
      _id: { $ne: data._id },
      name: data.name,
      deletedAt: null,
    });
    if (isDuplicated) {
      UserErrors(StatusCodes.PERMISSION_ALREADY_EXISTS);
    }
    const permissions = await this.permissionModel.find().exec();
    //check format permission
    const permissionInput = checkTypePermission(permissions, data.permission);
    data.permission = permissionInput;
    const checkChange = isEqual(omit(data, '_id'), permission.toJSON());

    if (checkChange === false) {
      const userIds = await this.userModel
        .find({
          groups: data._id,
          deletedAt: null,
          type: Type.STAFF
        })
        .select({ _id: 1 });
      if (userIds.length > 0) {
        this.cmsPermisionGateway.notifyPermissionChanged(
          userIds,
          'PERMISSION_CHANGED',
        );
      }
    }
    permission.set(data);
    return await permission.save();
  }

  async delete(data: DeletePermissionInput): Promise<boolean> {
    validateDeletePermission(data);
    const permission = await this.groupModel.findOne({
      _id: data.id,
      deletedAt: null,
    });
    if (!permission) {
      UserErrors(StatusCodes.PERMISSION_DO_NOT_EXISTS);
    }
    const user = await this.userModel.exists({
      groups: data.id,
      deletedAt: null,
    });
    if (user) {
      UserErrors(StatusCodes.USER_ALREADY_EXISTS_IN_GROUP);
    }
    await this.groupModel
      .findByIdAndUpdate(data.id, { deletedAt: new Date() })
      .exec();
    return true;
  }

  async detail(data: DetailPermission): Promise<Group> {
    validateDetailPermission(data);
    const permission = await this.groupModel.findOne({
      _id: data.id,
      deletedAt: null,
    });
    if (!permission) {
      UserErrors(StatusCodes.PERMISSION_DO_NOT_EXISTS);
    }
    return permission;
  }
  async listGroups(data: ListPermissionInput): Promise<ListGroupsResponse> {
    validateListPermission(data);
    const skip = (Number(data.page) - 1) * Number(data.limit);
    const conditions: any = {
      deletedAt: null,
    };
    if (data.name) {
      conditions.name = { $regex: data.name, $options: 'i' };
    }
    if (data.status) {
      conditions.status = data.status === 'true';
    }
    const permissions = await this.groupModel
      .find(conditions)
      .limit(Number(data.limit))
      .skip(skip)
      .exec();
    const permissionsAll = await this.groupModel
      .countDocuments(conditions)
      .exec();
    return {
      data: permissions,
      total: permissionsAll,
    };
  }
  async listPermission(): Promise<ListPermissionResponse> {
    const permissions = await this.permissionModel.find();
    return { data: permissions };
  }
}
