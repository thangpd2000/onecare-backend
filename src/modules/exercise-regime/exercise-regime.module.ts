import { Module, Global } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { exerciseRegimeShema} from './schemas/exercise-regime.shemas';
import { ExerciseRegineService } from './services/exercise-regime.sevices';
import { ExerciseRegineController } from './controllers/exercise-regime.controllers';
import { MediaSchema } from '@modules/media/schemas/media.schema';

@Global()
@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Exercise-regine', schema: exerciseRegimeShema },
  { name: 'Media', schema: MediaSchema },])],
  providers: [ExerciseRegineService],
  exports: [ExerciseRegineService],
  controllers: [ExerciseRegineController],
})
export class ExerciseRegineModule {}
