import { Types } from 'mongoose';
import {
  LookupUsers,
  LookupUser,
  LookupMedias,
} from '@helper/queries.helper';
import { ExerciseRegineListInput} from '../interfaces/exercise-regime.entity';

export const historiesExcerciseRegimeQuery = (params: ExerciseRegineListInput,user): any[] => {

  let $match: any = {
    deletedAt: null,
    userId: {$ne: user._id}
  };

  if (params.search) {
    $match = {
      ...$match,
      $or: [
        { address: new RegExp(params.search, 'i') },
        { method: new RegExp(params.search, 'i') },
      ],
    };
  }
  const lookupUser=LookupUser('$userId','user')
  const lookupMeidas = LookupMedias('$mediaIds','medias');
  const mainQuery = [
    { $match },
    { ...lookupUser },
    { ...lookupMeidas },
  
  ];
  return mainQuery;
};

