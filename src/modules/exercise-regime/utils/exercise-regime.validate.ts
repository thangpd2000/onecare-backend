import * as yup from 'yup';
import {
  ExerciseRegineCreateInput,
  ExerciseRegineId,
  ExerciseRegineListInput,
} from '../interfaces/exercise-regime.entity';
export const validateCreateExerciseRegine = (
  data: ExerciseRegineCreateInput,
) => {
  const basedRules: any = {
    address: yup.string().required(),
    degree: yup.string().required(),
    introduce: yup.string().required(),
    decription: yup
      .string()
      .min(2)
      .max(5000),
    method: yup.string().required(),
    mediaIds: yup.array(),
    link: yup.string(),
    content: yup
      .string()
      .min(2)
      .max(5000),
    status: yup.boolean().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
export const validateUpdateExerciseRegine = (
  data: ExerciseRegineCreateInput,
) => {
  const basedRules: any = {
    id: yup.string().required(),
    address: yup.string().required(),
    degree: yup.string().required(),
    introduce: yup.string().required(),
    decription: yup
      .string()
      .min(2)
      .max(5000),
    method: yup.string().required(),
    link: yup.string(),
    media: yup.string().required(),
    status: yup.boolean().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
export const validateExerciseRegineId = (data: ExerciseRegineId) => {
  const basedRules: any = {
    id: yup.string().required(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};

export const validateExerciseRegineList = (data: ExerciseRegineListInput) => {
  const basedRules: any = {
    limit: yup.string(),
    page: yup.string(),
    name: yup.string(),
    status: yup.string(),
  };
  const schema = yup.object().shape(basedRules);
  schema.validateSync(data);
};
