import { UserErrors } from '@helper/error.helper';
import { StatusCodes } from '@modules/base.interface';
import { Media } from '@modules/media/interfaces/media.interface';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { uploadImageWithoutResizing } from '@utils/photo';
import { files } from 'jszip';
import { Model } from 'mongoose';
import { asapScheduler } from 'rxjs';
import path from 'path';
import sharp from 'sharp';
import mongoose from 'mongoose';
import {
  ExerciseRegineCreateInput,
  ExerciseRegineResponse,
  ExerciseRegineId,
  ExerciseRegineListInput,
  ExerciseRegineDeleteResponse,
  ExerciseRegineListResponse,
  ExerciseRegineUpdateInput,
} from '../interfaces/exercise-regime.entity';
import {
  ExerciseRegine,
  ExerciseRegineRecord,
} from '../interfaces/exercise-regime.interface';
import {
  validateCreateExerciseRegine,
  validateExerciseRegineId,
  validateExerciseRegineList,
} from '../utils/exercise-regime.validate';
import { User } from '@modules/users/interfaces/user.interface';
import { check } from 'prettier';
import { historiesExcerciseRegimeQuery } from '../utils/excercise-regime.queries';
@Injectable()
export class ExerciseRegineService {
  constructor(
    @InjectModel('Exercise-regine')
    private readonly ExerciseRegineModel: Model<ExerciseRegine>,
  ) {}
  @InjectModel('Media') private readonly mediaModel: Model<Media>;
  /**
   *
   *
   * @param {Input} body
   * @param {*} files
   * @returns {Promise<Product>}
   * @memberof ProductService
   */
  async createExerciseRegine(
    body: ExerciseRegineCreateInput,
    user: User,
  ): Promise<ExerciseRegine> {
    const check = await this.ExerciseRegineModel.findOne({
      userId: user._id,
      deleteAt: null,
    });
    let dataResponse = null;
    if (check) {
      check.set({
        userId: user._id,
        address: body.address,
        method: body.method,
        description: body.decription,
        degree: body.degree,
        introduce: body.introduce,
        status: body.status,
        link: body.link,
        mediaIds: body.media,
        updatedAt: new Date(),
      });
      dataResponse = await check.save();
    }

    if (!check) {
      const ExerciseRegine = {
        _id: mongoose.Types.ObjectId().toString(),
        userId: user._id,
        address: body.address,
        method: body.method,
        description: body.decription,
        degree: body.degree,
        introduce: body.introduce,
        status: body.status,
        link: body.link,
        mediaIds: body.media,
        createdAt: new Date(),
      };
      const createData = new this.ExerciseRegineModel(ExerciseRegine);
      dataResponse = await createData.save();
      return dataResponse;
    }
  }
  async detailExerciseRegine(
    param: ExerciseRegineId,
  ): Promise<ExerciseRegineRecord> {
    validateExerciseRegineId(param);
    const data = await this.ExerciseRegineModel.findOne({
      _id: param.id,
      deletedAt: null,
    })
      .populate('users')
      .populate('medias')
      .lean();
    if (!data) {
      UserErrors(StatusCodes.EXERCISEREGIME_DO_NOT_EXISTS);
    }

    return data;
  }
  async deleteExerciseRegine(param: ExerciseRegineId): Promise<boolean> {
    validateExerciseRegineId(param);
    const ExerciseRegine = await this.ExerciseRegineModel.findOneAndDelete({
      _id: param.id,
      deletedAt: null,
    });
    if (!ExerciseRegine) {
      UserErrors(StatusCodes.EXERCISEREGIME_DO_NOT_EXISTS);
    }
    return true;
  }

  async deleteExerciseRegineByUser(userId): Promise<boolean> {
    const ExerciseRegine = await this.ExerciseRegineModel.findOneAndDelete({
      userId: userId,
      deletedAt: null,
    });
    console.log(ExerciseRegine);
    if (!ExerciseRegine) {
      UserErrors(StatusCodes.EXERCISEREGIME_DO_NOT_EXISTS);
    }
    return true;
  }

  async updateExerciseRegine(
    body: ExerciseRegineUpdateInput,
  ): Promise<ExerciseRegine> {
    validateCreateExerciseRegine(body);
    const news = await this.ExerciseRegineModel.findOne({
      _id: body.id,
      deletedAt: null,
    });
    if (!news) UserErrors(StatusCodes.NEWS_DO_NOT_EXISTS);

    const newsUpdate = {
      address: body.address,
      method: body.method,
      description: body.decription,
      degree: body.degree,
      introduce: body.introduce,
      status: body.status,
      mediaIds: body.media,
      link: body.link,
      createdAt: new Date(),
    };
    news.set(newsUpdate);
    return await news.save();
  }
  async listExerciseRegine(
    query: ExerciseRegineListInput,
    user: User,
  ): Promise<ExerciseRegineListResponse> {
    validateExerciseRegineList(query);
    const { limit, page, title, status } = query;
    const skip = (Number(page) - 1) * Number(limit);

    let conditions: any = {
      deletedAt: null,
      userId: { $ne: user._id },
    };

    if (query.search) {
      conditions = {
        ...conditions,
        $or: [
          { address: new RegExp(query.search, 'i') },
          { method: new RegExp(query.search, 'i') },
        ],
      };
    }
    const mainQuery = historiesExcerciseRegimeQuery(query, user);

    const total = await this.ExerciseRegineModel.countDocuments(conditions);
    mainQuery.push({ $sample: { size: total } });

    const data = await this.ExerciseRegineModel.aggregate(mainQuery).exec();

    return { data, total };
  }
}
