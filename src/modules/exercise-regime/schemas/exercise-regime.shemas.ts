import mongoose from 'mongoose';
import { string } from 'yup';
const ObjectId = mongoose.Types.ObjectId;
export const exerciseRegimeShema = new mongoose.Schema({
  address: String,
  degree: String,
  introduce: String,
  description: String,
  method: String,
  mediaIds: [{type: ObjectId,default:[]}],
  link: String,
  userId: { type: ObjectId },
  status: { type: Boolean, default: true },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: Date,
  deletedAt: {
    type: Date,
    default: null,
  },
  avatar:String
});
exerciseRegimeShema.index({ introduce: 'text' });
exerciseRegimeShema.index({ address: 'text' });
exerciseRegimeShema.index(
  { title: 1 },
  {
    collation: {
      locale: 'en',
      strength: 2,
    },
  },
);
exerciseRegimeShema.virtual('medias', {
  ref: 'Media',
  localField: 'mediaIds',
  foreignField: '_id',
  match: { deletedAt: null },
});
exerciseRegimeShema.virtual('users', {
  ref: 'User',
  localField: 'userId',
  foreignField: '_id',
  match: { deletedAt: null },
  justOne: true,
});
