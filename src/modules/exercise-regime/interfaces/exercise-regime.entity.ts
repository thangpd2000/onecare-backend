import { from } from 'rxjs';
import { StatusCodes } from '@modules/base.interface';
import {
  ExerciseRegine,
  ExerciseRegineRecord,
} from './exercise-regime.interface';
import { Tracing } from 'trace_events';

export class ExerciseRegineCreateInput {
  address: string;
  degree: string;
  introduce: string;
  decription: string;
  method: string;
  media: string[];
  link: string;
  status: boolean;
}
export class ExerciseRegineUpdateInput {
  id: string;
  address: string;
  degree: string;
  introduce: string;
  decription: string;
  method: string;
  media: string[];
  link: string;
  status: boolean;
}

export class ExerciseRegineResponse {
  status: StatusCodes;
  data: ExerciseRegineRecord;
}

export class ExerciseRegineId {
  id: string;
}
export class ExerciseRegineDeleteResponse {
  status: StatusCodes;
  data: boolean;
}
export class ExerciseRegineListInput {
  limit: string;
  search:string;
  page: string;
  title?: string;
  status?: ExerciseRegineStatus;
}
enum ExerciseRegineStatus {
  true = 'true',
  false = 'false',
}
export class ExerciseRegineListResponse {
  status?: StatusCodes;
  data: ExerciseRegineRecord[];
  total: number;
}
