import { from } from "rxjs";
import {Document} from 'mongoose'
import { Tracing } from "trace_events";
import { Media } from "@modules/media/interfaces/media.interface";

export interface ExerciseRegine extends Document{
readonly _id:string;
readonly userId:string;
readonly address: string,
readonly degree: string
readonly introduce: string,
readonly description: string,
readonly method: string,
readonly image:string;
readonly status:boolean;
readonly link:string;
readonly createAt:Date;
readonly updatedAt: Date;
readonly deleteAt:Date;
readonly mediaIds: string;
readonly medias?: Media[];
}
export interface ExerciseRegineRecord {
    readonly _id:string;
readonly userId:string;
readonly address: string,
readonly degree: string
readonly introduce: string,
readonly description: string,
readonly method: string,
readonly image:string;
readonly status:boolean;
readonly link:string;
readonly createAt:Date;
readonly updatedAt: Date;
readonly deleteAt:Date;
readonly mediaIds: string;
readonly medias?: Media[];
    }