import { StatusCodes } from '@modules/base.interface';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import {
  ExerciseRegineCreateInput,
  ExerciseRegineDeleteResponse,
  ExerciseRegineId,
  ExerciseRegineListInput,
  ExerciseRegineListResponse,
  ExerciseRegineResponse,
  ExerciseRegineUpdateInput,
} from '../interfaces/exercise-regime.entity';
import { ExerciseRegineService } from '../services/exercise-regime.sevices';
import { validateCreateExerciseRegine } from '../utils/exercise-regime.validate';
import { Roles } from '@modules/users/interfaces/user.entity';
import { User } from '@modules/users/interfaces/user.interface';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('exercise-regine')
@Controller('exercise-regine')
export class ExerciseRegineController {
  constructor(private readonly ExerciseRegineService: ExerciseRegineService) {}
  @Post()
  @Roles('STAFF')
  async create(
    @Body() body: ExerciseRegineCreateInput,
    @Req() req: Request & { user: User },
  ): Promise<ExerciseRegineResponse> {
    await validateCreateExerciseRegine(body);
    const data = await this.ExerciseRegineService.createExerciseRegine(
      body,
      req.user,
    );
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
  @Get('detail/:id')
  @Roles('STAFF', 'CUSTOMERS')
  async detailExerciseRegine(
    @Param() param: ExerciseRegineId,
  ): Promise<ExerciseRegineResponse> {
    const data = await this.ExerciseRegineService.detailExerciseRegine(param);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
  @Put()
  @Roles('STAFF')
  async updateNews(
    @Body() body: ExerciseRegineUpdateInput,
  ): Promise<ExerciseRegineResponse> {
    const data = await this.ExerciseRegineService.updateExerciseRegine(body);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
  @Delete('delete/:id')
  @Roles('STAFF')
  async deleteExerciseRegine(
    @Param() param: ExerciseRegineId,
  ): Promise<ExerciseRegineDeleteResponse> {
    const data = await this.ExerciseRegineService.deleteExerciseRegine(param);
    return {
      status: StatusCodes.SUCCESS,
      data,
    };
  }
  @Get('/list')
  @Roles('CUSTOMERS')
  async listExerciseRegine(
    @Query() query: ExerciseRegineListInput,
    @Req() req: Request & { user: User },
  ): Promise<ExerciseRegineListResponse> {
    const { data, total } = await this.ExerciseRegineService.listExerciseRegine(
      query,
      req.user,
    );
    return {
      status: StatusCodes.SUCCESS,
      data,
      total,
    };
  }
}
