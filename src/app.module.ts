import {
  LanguageModule,
  UploadModule,
  DatabaseModule,
  StaticFilesModule,
} from './root-module';
import { Module } from '@nestjs/common';
import { CommandModule } from 'nestjs-command';

import { UserModule } from '@modules/users/user.module';
import { CityModule } from '@modules/address/city.module';
import { WardModule } from '@modules/address/ward.module';
import { SeedModule } from '@commands/seeds/seeds.module';
import { MediaModule } from './modules/media/media.module';
import { ChatModule } from '@modules/chat/chat.module';
import { CrontabModule } from '@modules/crontab/crontab.module';
import { DistrictModule } from '@modules/address/district.module';
import { MigrationModule } from '@commands/migrations/migrations.module';
import { PermissionModule } from '@modules/permissions/permission.module';
import { APP_GUARD } from '@nestjs/core';
import { RequireAuth } from '@modules/users/utils/user.guard';
import { NotificationModule } from '@modules/notifications/notification.module';
import { ExerciseRegineModule } from '@modules/exercise-regime/exercise-regime.module';


@Module({
  imports: [
    StaticFilesModule,
    LanguageModule,
    DatabaseModule,
    UploadModule,
    WardModule,
    UserModule,
    CityModule,
    SeedModule,
    MediaModule,
    CommandModule,
    ChatModule,
    CrontabModule,
    DistrictModule,
    MigrationModule,
    PermissionModule,
    NotificationModule,
    ExerciseRegineModule
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: RequireAuth,
    },
  ],
})
export class AppModule {
  // configure(consumer: MiddlewareConsumer) {
  //   consumer
  //     .apply(UpdateLastOnlineTime)
  //     .forRoutes('message');
  // }
}
