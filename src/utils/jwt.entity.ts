export enum JWTAuthTokenType {
  ID_TOKEN = 'ID_TOKEN',
  REFRESH_TOKEN = 'REFRESH_TOKEN',
}

export type JWTAuthTokenPayload = {
  userId: string;
  type: JWTAuthTokenType;
};
