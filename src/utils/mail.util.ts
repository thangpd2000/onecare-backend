import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';

import { envConfig } from '@helper/env.helper';

const options = {
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: envConfig.MAIL_USERNAME || 'thangpdph09030@fpt.edu.vn',
    pass: envConfig.MAIL_SECRET_KEY || 'T0986716795',
  },
  port: 465,
};
const transporter = nodemailer.createTransport(smtpTransport(options));

export const GMailSupporting = mailOptions => {
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
};

// MailJet
const mailjet = require('node-mailjet').connect(
  envConfig.MJ_APIKEY_PUBLIC,
  envConfig.MJ_APIKEY_PRIVATE,
);

export const MailJetSupporting = (
  to: string,
  subject: string,
  html: string,
) => {
  const request = mailjet.post('send', { version: 'v3.1' }).request({
    Messages: [
      {
        From: {
          Email: envConfig.APP_EMAIL,
          Name: envConfig.APP_NAME,
        },
        To: [
          {
            Email: to,
            Name: 'You',
          },
        ],
        Subject: subject,
        HTMLPart: html,
      },
    ],
  });
  request
    .then(result => {
      console.log(result.body);
    })
    .catch(err => {
      console.log(err.statusCode);
    });
};
