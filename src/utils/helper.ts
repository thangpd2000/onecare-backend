
import { TemplateTitleNotification, PayloadNoti, TelegramConfig } from '@modules/base.interface';
import TelegramBot  from 'node-telegram-bot-api';
import mongoose from 'mongoose';

const BotTelegram = new TelegramBot(TelegramConfig.TOKEN, { polling: false });

export const paginate = (array, page_number, page_size ) => {
  return array.slice((page_number - 1) * page_size, page_number * page_size);
}

export const getContentNotification = (code: TemplateTitleNotification, payload: PayloadNoti): string => {
  switch(code) {
    case TemplateTitleNotification.ORDER_CONFRIRMED:
      return `Đơn hàng ${payload.codeOrder} đã được hệ thống xác nhận.`
    case TemplateTitleNotification.ORDER_PAID:
      return `Bạn vừa thanh toán thành công cho đơn hàng ${payload.codeOrder}`
    case TemplateTitleNotification.ORDER_DELIVED:
      return `Đơn hàng ${payload.codeOrder} đã được giao hàng thành công tới địa điểm nhận.`
    case TemplateTitleNotification.ORDER_CANCELED_BY_CMS:
      return `Đơn hàng ${payload.codeOrder} của bạn đã bị từ chối. Vui lòng liên hệ Dailymart theo số hotline cửa hàng ${payload.hotline} hoặc chat trực tiếp trên App để được hỗ trợ.`
    case TemplateTitleNotification.HAPPY_BIRTHDAY:
      return `Đội ngũ Dailymart gửi lời chúc mừng sinh nhật tốt đẹp nhất tới quý khách. Hy vọng quý khách sẽ tiếp tục đồng hành và có những trải nghiệm thú vị cùng Dailymart trong thời gian tới!`
    case TemplateTitleNotification.CHANGE_POIN_WHEN_PAY_IN_STORE:
      return `Bạn vừa sử dụng ${payload.point} điểm vào đơn hàng mua trực tiếp tại cửa hàng.`
    default:
      return 'Default'
  }
}

export const sendMessageTelegram = (message, json) => {
  try {
    BotTelegram.sendMessage(TelegramConfig.CHAT_ID, message + '\n\n<pre>' + JSON.stringify(json, null, 2) + '</pre>', {
      parse_mode: 'HTML'
    });
  } catch (err) {
    console.log('Something went wrong when trying to send a Telegram notification', err);
  }
}

export const convertStringToObjectId = (arr) => {
  return arr.map(e => mongoose.Types.ObjectId(e));
}

export const isArraysHasSameElement = (array: string[], otherArray: string[]) => {
  return array.some(element => otherArray.includes(element));
}