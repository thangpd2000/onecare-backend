import { Injectable, NestMiddleware } from '@nestjs/common';
import { Response } from 'express';
import { MyRequest } from '@modules/base.interface';

@Injectable()
export class UpdateLastOnlineTime implements NestMiddleware {
  use(req: MyRequest, res: Response, next: Function) {
    console.log('Request...', req.user);
    next();
  }
}
