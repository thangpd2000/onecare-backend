import sharp from 'sharp';
import { MediaType } from '@modules/media/interfaces/media.entity';
import mongoose from 'mongoose';
import fs from 'fs';
import { defaultImageExtension } from '@helper/constant.helper';
import { HttpException, HttpStatus  } from '@nestjs/common'
import { UserErrors } from '@helper/error.helper';
import { StatusCodes } from '@modules/base.interface'

export const mkdirSync = dirPath => {
  try {
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath, { recursive: true });
    }
  } catch (err) {
    if (err.code !== 'EXIST') throw err;
  }
};

function resize(file, width, height, filename, path) {
  let pathName = '';
  if (width && height) {
    pathName = `assets/${path}/${width}-${height}`;
  } else {
    pathName = `assets/${path}`;
  }
  mkdirSync(pathName);
  return sharp(file.buffer)
    .flatten({ background: { r: 255, g: 255, b: 255 } })
    .jpeg({ quality: 100 })
    .resize(width, height)
    .withMetadata()
    .toFile(pathName + '/' + filename, function(err) {
      if (err) {
        console.error(err);
        UserErrors(StatusCodes.FAILURE ,err ,err);
      }
      console.log('Photo uploaded');
    });
}

export const uploadImage = (file, path, width = 0, height = 0) => {
  const _id = mongoose.Types.ObjectId().toString();
  const ext = defaultImageExtension;
  let url = `statics/${path}/1200-1200/${_id}.${ext}`;
  if (width && height) {
    resize(file, width, height, `${_id}.${ext}`, path);
    url = `statics/${path}/${width}-${height}/${_id}.${ext}`;
  }
  resize(file, 300, 300, `${_id}.${ext}`, path);
  resize(file, 600, 600, `${_id}.${ext}`, path);
  resize(file, 1200, 1200, `${_id}.${ext}`, path);
  return {
    _id,
    url,
    thumbnail: `statics/${path}/300-300/${_id}.${ext}`,
    thumbnail2x: `statics/${path}/600-600/${_id}.${ext}`,
    type: MediaType.PHOTO,
    createdAt: new Date(),
  };
};

export const uploadImageWithoutResizing = async (file, path, _id) => {
  const ext = defaultImageExtension;
  const photo = resize(file, null, null, `${_id}.${ext}`, path);
  const metatdata = await photo.metadata();

  return {
    _id,
    url: `statics/${path}/${_id}.${ext}`,
    type: MediaType.PHOTO,
    width: metatdata.width,
    height: metatdata.height,
    createdAt: new Date(),
  };
};
