import jwt from 'jsonwebtoken';
import { readFileSync } from 'fs';
import { JWTAuthTokenPayload } from './jwt.entity';

const pathToKeys =
  process.env.STAGE === 'production'
    ? 'configs/jwtRS256.prod.key'
    : 'configs/jwtRS256.staging.key';
const privateKey = readFileSync(pathToKeys, 'utf8');
const publicKey = readFileSync(pathToKeys + '.pub', 'utf8');

export const generateUserToken = (payload: JWTAuthTokenPayload) => {
  return jwt.sign(payload, privateKey, {
    expiresIn: process.env.JWT_ID_TOKEN_EXPIRES,
    algorithm: 'RS256',
  });
};

export const generateRefreshToken = (payload: JWTAuthTokenPayload) => {
  return jwt.sign(payload, privateKey, {
    expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRES,
    algorithm: 'RS256',
  });
};

export const decodeToken = async (token: string) => {
  return jwt.verify(
    token,
    publicKey,
    { algorithms: ['RS256'] },
    (error, decoded) => {
      if (error) {
        throw error;
      }
      return decoded;
    },
  );
};
