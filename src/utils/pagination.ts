export const paginationDto = query => {
  const page = Number(query.page);
  const limit = Number(query.limit);
  const skip = page ? (page - 1) * limit : 0;

  return {
    limit,
    skip,
  };
};
