import {
  I18nModule,
  I18nJsonParser,
  HeaderResolver,
  CookieResolver,
  QueryResolver,
  AcceptLanguageResolver,
} from 'nestjs-i18n';
import { MongooseModule } from '@nestjs/mongoose';
import * as path from 'path';
import { envConfig } from '@helper/env.helper';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { MulterModule } from '@nestjs/platform-express';

export const LanguageModule = I18nModule.forRoot({
  fallbackLanguage: 'en',
  parser: I18nJsonParser,
  parserOptions: {
    path: path.join(__dirname, '/i18n/'),
  },
  resolvers: [
    { use: QueryResolver, options: ['lang', 'locale', 'l'] },
    new HeaderResolver(['x-custom-lang']),
    AcceptLanguageResolver,
    new CookieResolver(['lang', 'locale', 'l']),
  ],
});

export const UploadModule = MulterModule.register({
  dest: './assets/uploads',
});

export const DatabaseModule = MongooseModule.forRoot(
  envConfig.CLOUD_DB ||
    `${envConfig.MONGO_URI || 'mongodb://localhost:27017'}/${
      envConfig.DATABASE_NAME
    }${envConfig.MONGO_AUTH === 'true' ? '?authSource=admin' : ''}`,
  {
    // use function findOneAndUpdate()
    useFindAndModify: false,
    useCreateIndex: true,
  },
);

export const StaticFilesModule = ServeStaticModule.forRoot({
  rootPath: join(__dirname, '..', 'assets'),
});
