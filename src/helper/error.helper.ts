import {
  HttpException,
  HttpStatus,
  ExceptionFilter,
  Catch,
  ArgumentsHost,
} from '@nestjs/common';
import { Response } from 'express';
import { StatusCodes } from '@modules/base.interface';

export const UserErrors = (
  status: StatusCodes,
  message?: any,
  code?: HttpStatus,
  data?: any,
) => {
  throw new HttpException({ status, message, data   }, code || HttpStatus.BAD_REQUEST);
};

export const UserErrorsPromo = (status: StatusCodes, data: any) => {
  throw new HttpException({ status, data }, HttpStatus.BAD_REQUEST);
};

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException | Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    if (exception instanceof HttpException) {
      return response.json(exception.getResponse());
    }

    return response.json({
      status: this.mapStatusCode(exception),
      message: exception.message,
      stacktrace: exception.stack,
    });
  }

  mapStatusCode(exception: Error): StatusCodes {
    if (exception.name === 'ValidationError') {
      return StatusCodes.VALIDATION_ERROR;
    }

    return StatusCodes.FAILURE;
  }
}
export class UserError<Status> extends HttpException {
  constructor(
    status: Status | StatusCodes,
    message?: any,
    code?: HttpStatus,
    
  ) {
    super({ status, message }, code || HttpStatus.BAD_REQUEST);
  }
}
