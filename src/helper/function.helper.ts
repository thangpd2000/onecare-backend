export async function asyncForEach(array: Array<any>, callback: any) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
export const isEmpty = (text: string): boolean => {
  if (!text) return true;

  if (text.trim() === '') return true;

  return false;
};
