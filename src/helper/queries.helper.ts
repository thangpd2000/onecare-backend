export const LookupUsers: any = (key = '$userIds', as = 'users') => {
  return {
    $lookup: {
      from: 'users',
      let: {
        userIds: key,
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ['$_id', '$$userIds'],
            },
          }
        },
        {
          $lookup: {
            from: 'media',
            let: {
              avatarId: '$avatar',
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ['$_id', '$$avatarId'],
                  },
                }
              },
            ],
            as: 'avatarSource'
          }
        },
        { $sort: { name: 1 } },
        {
          $project: {
            _id: 1,
            avatar: { $arrayElemAt: ['$avatarSource', 0] },
            name: 1
          }
        }
      ],
      as
    }
  }
};

export const LookupUser: any = (key = '$createdBy', as = 'creator') => {
  return {
    $lookup: {
      from: 'users',
      let: {
        userId: key,
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ['$_id', '$$userId'],
            },
          }
        },
        {
          $lookup: {
            from: 'media',
            let: {
              avatarId: '$avatar',
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $eq: ['$_id', '$$avatarId'],
                  },
                }
              },
            ],
            as: 'avatarSource'
          }
        },
        { $sort: { name: 1 } },
        {
          $project: {
            _id: 1,
            avatar: { $arrayElemAt: ['$avatarSource', 0] },
            name: 1,
            online: 1
          }
        }
      ],
      as
    }
  }
};

export const LookupMessage: any = (key = '$lastMessageId', as = 'message') => {
  return {
    $lookup: {
      from: 'messages',
      let: {
        messageId: key,
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ['$_id', '$$messageId'],
            },
          }
        },
      ],
      as
    }
  }
};

export const LookupMedias: any = (key = '$mediaIds', as = 'medias') => {
  return {
    $lookup: {
      from: 'media',
      let: {
        mediaIds: key,
      },
      pipeline: [
        {
          $match: {
            $expr: {
              $in: ['$_id', '$$mediaIds'],
            },
          }
        },
      ],
      as
    }
  }
};