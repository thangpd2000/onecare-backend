/* eslint-disable @typescript-eslint/camelcase */
import { HttpService } from '@nestjs/common';
import { envConfig } from './env.helper';

interface Tag {
    key: string;
    value: string;
}
interface NotificationParams {
    title: string;
    content: string;
    data?: any;
    tag?: Tag;
}

class Onesignal {
  private httpService: HttpService = new HttpService;
  private appId: string;
  private appKey: string;
  private apiName: string;
  private headers: any;

  constructor(appId: string, appKey: string) {
    this.appId = appId;
    this.appKey = appKey;
    this.apiName = 'https://onesignal.com/api/v1';
    this.headers = {
      'Content-Type': 'application/json;',
      Authorization: `Basic ${this.appKey}`,
    };
  }

  // gửi thông báo cho tất cả người dùng
  sendToAlls(params: NotificationParams) {
    try {
      this.httpService.post(
        this.apiName,
        {
          app_id: this.appId,
          contents: {
            en: params.title,
          },
          headings: {
            en: params.content,
          },
          data: params.data,
          included_segments: ['All'],
        },
        {
          headers: this.headers,
        },
      ).subscribe(res => {
        console.log('send notification success: ', res);
      });
    } catch (error) {
      console.log('send to all error: ', error);
    }
  }

  // gửi thông báo theo filter tag
  sendToTags(params: NotificationParams) {
    try {
      this.httpService.post(
        this.apiName,
        {
          app_id: this.appId,
          contents: {
            en: params.title,
          },
          headings: {
            en: params.content,
          },
          data: params.data,
          filters: [{ field: 'tag', key: params.tag.key, relation: '=', value: params.tag.value }]
        },
        {
          headers: this.headers,
        },
      ).subscribe(res => {
        console.log('send notification success: ', res);
      })
      
    } catch (error) {
      console.log('send to all error: ', error);
    }
  }

 
}

const OnesignalHelper = new Onesignal(
  envConfig.appId,
  envConfig.apiKey,
);

export default OnesignalHelper;
