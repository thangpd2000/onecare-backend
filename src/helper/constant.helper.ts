export const RESPONSE_STATUS = {
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE',
};

export const JWT_EXPIRED = '30d';

export const SESSION_SECRET =
  'SESSION_SECRET_________sksaswinwoadnjsndlsnwajnwnewjdansnflsklsnd3e303p;l.sdds';

export const PASSWORD_LENGTH = {
  MIN: 6,
  MAX: 32,
};

export const MAX_UPLOADED_FILE = 5;

export const MIN_DELIVERY_ADDRESS = 1;

export const defaultImageExtension = 'jpg';