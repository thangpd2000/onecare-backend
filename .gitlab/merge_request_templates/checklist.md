**MR cho ticket:**
* http://redmine.vinsofts.net/issues/#

---

**Checklist:**
- [ ] Không console.log
- [ ] Hạn chế tối đa dùng kiểu `any`
- [ ] Không duplicate code
- [ ] Kiểm tra xem các function code có bị rủi ro không?
- [ ] Xóa biến không sử dụng
- [ ] Tách nhỏ các function, để mỗi function xử lý 1 chức năng duy nhất
- [ ] Không hard code

**Note:**
- [ ] Cần deploy cùng MR khác (link: ___)